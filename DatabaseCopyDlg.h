#pragma once

#include "Resource.h"

#include "DBHandler.h"
#include "afxwin.h"
// CDatabaseCopyDlg dialog

class CDatabaseCopyDlg : public CDialog
{
	DECLARE_DYNAMIC(CDatabaseCopyDlg)

	BOOL m_bInitialized;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;

	CButton m_wndBtnDoCopy;
	CButton m_wndCancelBtn;

	CXTListCtrl m_wndFromLB;
	CXTHeaderCtrl m_HeaderFrom;

	CXTListCtrl m_wndToLB;
	CXTHeaderCtrl m_HeaderTo;

	TCHAR m_szUserDB[128];
	CString m_sSelectedFromDB;
	CString m_sSelectedToDB;
	CString m_sActiveDBLbl;
	CString m_sActiveDBMsg;
	CString m_sCopyDoneDBMsg;
protected:
	CHmsUserDBRec m_recHmsDBs;
	vecHmsUserDBs m_vecHmsUserDBs;

	void populateFromLB(void);
	void populateToLB(void);
	

	void doCopyDB(void);
public:
	CDatabaseCopyDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDatabaseCopyDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG4 };

	void setHMSDBs(vecHmsUserDBs &vec)	{	m_vecHmsUserDBs = vec;	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMClickList1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickList2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButton1();
};
