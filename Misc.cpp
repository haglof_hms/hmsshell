
#include "StdAfx.h"

#include "MainFrm.h"

#include "windowsx.h"  // required for GET_X_LPARAM,  GET_Y_LPARAM)

#include <fstream>

#pragma managed(push, off)

BOOL getGlobLangsInfo(vecLANGUAGE_INFO &vec)
{
	vec.clear();

	for (int i = 0; i < _countof(languages); ++i)
	{
			DWORD lcid = MAKELCID(languages[i].wLangID, SORT_DEFAULT);
			TCHAR szBuf1[64];
			TCHAR szBuf2[64];
			GetLocaleInfo(lcid,LOCALE_SLANGUAGE,szBuf1,64);
			GetLocaleInfo(lcid,LOCALE_SABBREVLANGNAME,szBuf2,64);
			vec.push_back(CLanguageInfo(lcid,szBuf1,szBuf2));
	}

	return (vec.size() > 0);
}

BOOL getGlobLangInfo(LPCTSTR lng_abrev,CLanguageInfo &lng)
{
	for (int i = 0; i < _countof(languages); ++i)
	{
			DWORD lcid = MAKELCID(languages[i].wLangID, SORT_DEFAULT);
			TCHAR szBuf1[64];
			TCHAR szBuf2[64];
			GetLocaleInfo(lcid,LOCALE_SLANGUAGE,szBuf1,64);
			GetLocaleInfo(lcid,LOCALE_SABBREVLANGNAME,szBuf2,64);
			if (_tcscmp(lng_abrev,szBuf2) == 0)
			{
				lng = CLanguageInfo(lcid,szBuf1,szBuf2);
				return TRUE;
			}
	}

	return FALSE;
}

void setLangSet(LPCTSTR lang_name)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR sValue[127];
	TCHAR szRoot[127];

	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),REG_ROOT,REG_LANG_KEY);
	RegCreateKeyEx(HKEY_CURRENT_USER,
		szRoot,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_WRITE,
		NULL,
		&hk,
		&dwDisp);
	// Set value into a NULL-terminated string
	_stprintf(sValue,_T("%s"),lang_name);
	// ... and store it in a key ...
	USES_CONVERSION;
	RegSetValueEx(hk,
		REG_LANG_ITEM,
		0,
		REG_SZ,
		(LPBYTE)&sValue,
		(DWORD) (lstrlen(sValue)+1)*sizeof(TCHAR));

	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);
}

CString getLanguageFileName(void)
{
	CString sLangFN;

	sLangFN.Format(_T("%s%s%s"),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	return sLangFN;
}

void getLanguageFiles(CStringArray &str_list)
{
	CDiskObject cdo;
	CString sPath;

	sPath = getLanguageDir();

	str_list.RemoveAll();	// Clear list
	cdo.EnumAllFilesWithFilter(LANGUAGE_FN_WC,sPath,str_list);
}

CString getResStr(UINT id)
{
	CString sText;
	XTPResourceManager()->LoadString(&sText, id);

	return sText;
}

CString getShellDataDir(void)
{
	CString sPath;
	TCHAR szPath[MAX_PATH];
	if(SUCCEEDED(SHGetFolderPath(NULL, 
                             CSIDL_APPDATA|CSIDL_FLAG_CREATE, 
                             NULL, 
                             0, 
                             szPath))) 
	{
		sPath.Format(_T("%s\\%s"),szPath,SUBDIR_SHELL_DATA);
	}
	return sPath;
}

BOOL getShellDataFiles(CStringArray &str_list)
{
	CString sSuitePath;
	CString sFile;
	BOOL bFileExists,bItemsInList;

	bFileExists = bItemsInList = FALSE;
	str_list.RemoveAll();	// Clear list

	sSuitePath.Format(_T("%s\\%s"),getShellDataDir(),SHELLDATA_ShortcutsFN);
	// Get suites to be added to Navigation bar, from Shortcuts.xml; 060612 p�d
	XMLHandler *xml = new XMLHandler();
	if (fileExists(sSuitePath))
	{
		if (xml)
		{
			if (xml->load(sSuitePath))
			{
				// Get all ShellData files in ShellData subdirectory; 051115 p�d
				xml->getSuitesInShellTree(str_list);
			}	// if (xml->load(sSuitePath))
		}	// if (xml)
	}	// if (fileExists(sSuitePath))
	delete xml;

	if (str_list.GetCount() > 0)
	{
		return TRUE;
	}

	return FALSE;

}

BOOL getShellDataFilesOnDisk(CStringArray &str_list)
{
	BOOL bReturn = FALSE;
	CStringArray tmpFiles;
	CDiskObject cdo;
	CString sSuitesPath,sFile;

	// Read Suites in subdirectories; 051117 p�d
	str_list.RemoveAll();	// Clear list
	sSuitesPath.Format(_T("%s"),getShellDataDir());
	cdo.EnumAllFiles(sSuitesPath,tmpFiles);
	//cdo.EnumFilesInDirectory(sSuitesPath,tmpFiles,EF_FULLY_QUALIFIED);

	if (tmpFiles.GetCount() > 0)
	{
		for (int i = 0;i < tmpFiles.GetCount();i++)
		{
			sFile = tmpFiles.GetAt(i);
			if (sFile.Right(4) == _T(".xml"))
				str_list.Add(sFile);
		}
		bReturn = (str_list.GetCount() > 0);
	}

	tmpFiles.RemoveAll();
	return bReturn;

	return FALSE;
}

BOOL checkShellDataFiles(CStringArray &str_list_disk,		// ... on disk
												 CStringArray &str_list_shortcuts)		// In Shortcuts.xml
{
	CFileFind find;
	BOOL bFound,bIsDirty;
	CString sStr1;
	CString sStr2;
	CString sFileName;
	CString sSuitePath;
	bIsDirty = FALSE;
	// Check if there's file on disk, not in Shortcuts. If found
	// add to shortcuts; 060807 p�d
	for (int i1 = 0;i1 < str_list_disk.GetCount();i1++)
	{
		sStr1 = str_list_disk[i1]; // On disk
		bFound = FALSE;
		for (int i2 = 0;i2 < str_list_shortcuts.GetCount();i2++)
		{
			sStr2 = str_list_shortcuts[i2];	// In Shortcuts

			if (sStr2.CompareNoCase(sStr1) == 0)
			{
				bFound = TRUE;
				break;
			}
		}	// for (int i2;i2 < str_list2.GetCount();i2++)

		// File on disk, NOT in Shortcuts
		if (!bFound)
		{
			if (find.FindFile(sStr1))
			{
				sFileName = sStr1.Right((int)_tcslen(sStr1) - (int)_tcslen(find.GetFilePath()));
			}
			find.Close();

			sSuitePath.Format(_T("%s\\%s"),getShellDataDir(),SHELLDATA_ShortcutsFN);
			// Get suites to be added to Navigation bar, from Shortcuts.xml; 060612 p�d
			XMLHandler *xml = new XMLHandler();
			if (xml->load(sSuitePath))
			{
				xml->addShellDataFileToShortcuts(sFileName);
				bIsDirty = TRUE;
			}
			xml->save(sSuitePath);
			delete xml;

		}	// if (!bFound)

	}	// for (int i1;i1 < str_list1.GetCount();i1++)

	bIsDirty = FALSE;
	// Check if there's items in shortcuts, but not on disk. 
	// If so, remove items from shortcuts; 060814 p�d
	for (int i1 = 0;i1 < str_list_shortcuts.GetCount();i1++)
	{
		sStr1 = str_list_shortcuts[i1]; // On disk
		bFound = FALSE;
		for (int i2 = 0;i2 < str_list_disk.GetCount();i2++)
		{
			sStr2 = str_list_disk[i2];	// In Shortcuts

			if (sStr2.CompareNoCase(sStr1) == 0)
			{
				bFound = TRUE;
				break;
			}
		}	// for (int i2;i2 < str_list2.GetCount();i2++)

		// File in Shortcuts, not on disk; 060814 p�d
		if (!bFound)
		{
			sSuitePath.Format(_T("%s\\%s"),getShellDataDir(),SHELLDATA_ShortcutsFN);
			// Get suites to be added to Navigation bar, from Shortcuts.xml; 060612 p�d
			XMLHandler *xml = new XMLHandler();
			if (xml->load(sSuitePath))
			{
				xml->delShellDataFileFromShortcuts(sStr1);
				bIsDirty = TRUE;
			}
			xml->save(sSuitePath);
			delete xml;

		}	// if (!bFound)

	}	// for (int i1;i1 < str_list1.GetCount();i1++)
	return bIsDirty;
}

BOOL getShellTreeDataItems(CStringArray &shellfiles_list,	// Array of files in Shortcuts.xml
													 vecTreeList &shelltree_list)		// ShellTree data
{
	BOOL bIsOK = FALSE;
	XMLHandler *xml = new XMLHandler();
	// Check if there's any ShellData files; 051115 p�d
	if (shellfiles_list.GetCount() > 0)
	{

		for (int i = 0;i < shellfiles_list.GetCount();i++)
		{
			if (xml->load(shellfiles_list.GetAt(i)))
			{
				xml->getShellTreeData(shelltree_list);
			}	// if (xml->load(m_arrShellDataFileList.GetAt(i)))
		}	// for (int i = 0;i < m_arrShellDataFileList.GetCount();i++)
		
		bIsOK = TRUE;
	}	// if (m_arrShellDataFileList.GetCount() > 0)
	delete xml;

	return bIsOK;
}

BOOL getSuites(CStringArray &str_list,BOOL clear)
{
	BOOL bReturn = FALSE;
	CDiskObject cdo;
	CString sSuitesPath,sFile;
	CStringArray tmpFiles;

	// Read Suites in subdirectories; 051117 p�d
	if (clear) str_list.RemoveAll();	// Clear list
	sSuitesPath.Format(_T("%s"),getSuitesDir());
	cdo.EnumFilesInDirectory(sSuitesPath,tmpFiles,EF_FULLY_QUALIFIED);

	if (tmpFiles.GetCount() > 0)
	{
		for (int i = 0;i < tmpFiles.GetCount();i++)
		{
			sFile = tmpFiles.GetAt(i);
			if (sFile.Right(4) == _T(".dll"))
				str_list.Add(sFile);
		}
		bReturn = (str_list.GetCount() > 0);
	}

	tmpFiles.RemoveAll();
	return bReturn;

}

BOOL getModules(CStringArray &str_list,BOOL clear)
{
	BOOL bReturn = FALSE;
	CDiskObject cdo;
	CString sModulesPath,sFile;
	CStringArray tmpFiles;

	if (clear) str_list.RemoveAll();	// Clear list
	sModulesPath.Format(_T("%s"),getModulesDir()); //,SUBDIR_MODULES);
	cdo.EnumFilesInDirectory(sModulesPath,tmpFiles); //,EF_FULLY_QUALIFIED);

	if (tmpFiles.GetCount() > 0)
	{
		for (int i = 0;i < tmpFiles.GetCount();i++)
		{
			sFile = tmpFiles.GetAt(i);
			if (sFile.Right(4) == _T(".dll"))
				str_list.Add(sFile);
		}
		bReturn = (str_list.GetCount() > 0);
	}

	tmpFiles.RemoveAll();
	return bReturn;
}

CString getHelpDir(void)
{
	CString sPath;

	sPath.Format(_T("%s%s"),getProgDir(),SUBDIR_HELP);

	return sPath;
}

CString getSetupShellData(void)
{
	CString sPath;

	sPath.Format(_T("%s%s\\%s"),getProgDir(),SETUP_DIR,SETUP_SHELLDATA_DIR);

	return sPath;
}


HTREEITEM GetNextTreeItem(const CTreeCtrl& treeCtrl, HTREEITEM hItem)
{
	// we return the next HTEEITEM for a tree such as:
	// Root (1)
	//		Child1 (2)
	//			xxxx (3)
	//			yyyy (4)
	//		Chiled2 (5)
	// Item (6)
	// has this item got any children
	if (treeCtrl.ItemHasChildren(hItem))
	{
		return treeCtrl.GetNextItem(hItem, TVGN_CHILD);
	}
	else if (treeCtrl.GetNextItem(hItem, TVGN_NEXT) != NULL)
	{
		// the next item at this level
		return treeCtrl.GetNextItem(hItem, TVGN_NEXT);
	}
	else
	{
		// return the next item after our parent
		hItem = treeCtrl.GetParentItem(hItem);
		if (hItem == NULL)
		{
			// no parent
			return NULL;
		}

		while (treeCtrl.GetNextItem(hItem, TVGN_NEXT) == NULL)
		{
			hItem = treeCtrl.GetParentItem(hItem);
			if (hItem == NULL)
				return NULL;
		}

		// next item that follows our parent
		return treeCtrl.GetNextItem(hItem, TVGN_NEXT);
	}
}


int getLevel(const CTreeCtrl& tree,HTREEITEM hItem)
{
	int nLevel = 0;

	while ((hItem = tree.GetParentItem( hItem )) != NULL)
	{
		nLevel++;
	}
	return nLevel;
}

BOOL getCheckStatus(const CTreeCtrl& tree,HTREEITEM hItem)
{
	return tree.GetCheck(hItem);
}

void setTreeParents(CTreeCtrl& tree,HTREEITEM hItem,BOOL check)
{
	while ((hItem = tree.GetParentItem( hItem )) != NULL) 
	{
		if (!tree.GetCheck(hItem))
		{
			tree.SetCheck(hItem,check);	
		}	// if (!GetCheck(hItem))
	}	// while ((hItem = GetParentItem( hItem )) != NULL) 
}


/////////////////////////////////////////////////////////////////////////////
// class CPADTreeCtrl

IMPLEMENT_DYNAMIC( CPADTreeCtrl, CTreeCtrl )

BEGIN_MESSAGE_MAP( CPADTreeCtrl, CTreeCtrl )
	//{{AFX_MSG_MAP(CPADTreeCtrl)
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_DESTROY()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND_RANGE(ID_POPUP_HIDE,ID_POPUP_ADD_TO_SHORTCUTS, OnPopupMenuItem)
	ON_COMMAND_RANGE(ID_POPUP_EXPAND_TREE,ID_POPUP_COLLAPSE_TREE, OnPopupMenuItemExpandCollapse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPADTreeCtrl::CPADTreeCtrl()
{
}

CPADTreeCtrl::~CPADTreeCtrl()
{
}

void CPADTreeCtrl::OnDestroy()
{
	HTREEITEM hItem;
	CTreeList *p;
	// Clear data allocated for CTreeList
	// in treecontrol, set by SetItemData; 070307 p�d
	hItem = GetFirstVisibleItem();
	if (hItem != NULL)
	{
		while (hItem != NULL)
		{
			p = (CTreeList*)GetItemData(hItem);
			if (p != NULL)
			{
				delete p;
			}
			hItem = GetNextItem(hItem,TVGN_NEXTVISIBLE);
		}
	}
}

BOOL CPADTreeCtrl::OnEraseBkgnd(CDC * /*pDC*/)
{
	return FALSE;
}

BOOL CPADTreeCtrl::PreTranslateMessage(MSG *pMsg)
{
	if (pMsg->message == WM_KEYUP)
	{
		if (pMsg->wParam == VK_RETURN)
		{
			HTREEITEM hNode = GetSelectedItem();
			if (hNode != NULL)
			{
				Expand(hNode,TVE_TOGGLE);
				CXTTreeCtrl *data = (CXTTreeCtrl*)GetItemData(hNode);
				if (data != NULL)
				{
					CMainFrame *p = DYNAMIC_DOWNCAST(CMainFrame,AfxGetMainWnd());
					p->SendMessage(WM_USER_MSG_TREELIST, WPARAM(0),(LPARAM)(CXTTreeCtrl*)data);
				}	// if (data != NULL)

				SelectItem(hNode);
			}	// if (hNode != NULL)
		}
	}
	return CTreeCtrl::PreTranslateMessage(pMsg);
}

void CPADTreeCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CTreeCtrl::OnLButtonDown(nFlags,point);

	UINT uFlag;
	HTREEITEM hNode = HitTest(point,&uFlag);
	if ((hNode != NULL) && (TVHT_ONITEM & uFlag))
	{
		Expand(hNode,TVE_TOGGLE);
		CXTTreeCtrl *data = (CXTTreeCtrl*)GetItemData(hNode);
		if (data != NULL)
		{
			CMainFrame *p = DYNAMIC_DOWNCAST(CMainFrame,AfxGetMainWnd());
			p->SendMessage(WM_USER_MSG_TREELIST, WPARAM(0),(LPARAM)(CXTTreeCtrl*)data);
		}	// if (data != NULL)

		SelectItem(hNode);
	}	// if (hNode != NULL)
}

void CPADTreeCtrl::OnRButtonDown(UINT nFlags, CPoint point) 
{
	CMenu menu;
	menu.LoadMenu(IDR_NAVBAR_POPUP);
	CMenu* pPopup = menu.GetSubMenu(0);

	HTREEITEM hMyItem = HitTest(CPoint(55,6), &nFlags);
	if ((hMyItem != NULL) && (TVHT_ONITEM & nFlags))
	{
		Select(hMyItem, TVGN_CARET);
	}

	// Only show these menuitems when user clicks outside an item in the
	// Tree control; 060425 p�d
	pPopup->InsertMenu(0,MF_BYPOSITION | MF_STRING,ID_POPUP_EXPAND_TREE,(getResStr(IDS_STRING151)));
	pPopup->InsertMenu(1,MF_BYPOSITION | MF_STRING,ID_POPUP_COLLAPSE_TREE,(getResStr(IDS_STRING152)));
	pPopup->InsertMenu(2,MF_BYPOSITION | MF_SEPARATOR);
	pPopup->ModifyMenu(3,MF_BYPOSITION | MF_STRING,ID_POPUP_SHOW,(getResStr(IDS_STRING146)));

	ClientToScreen (&point);
	UINT cmd = TrackPopupMenu(pPopup->GetSafeHmenu(),
					TPM_LEFTALIGN | TPM_TOPALIGN | TPM_RIGHTBUTTON,
						point.x, point.y, 0, GetSafeHwnd(), 0);
}

void CPADTreeCtrl::OnPopupMenuItem(UINT nMenuID)
{
	HTREEITEM hItem = GetSelectedItem();
	if (hItem != NULL)
	{
		CTreeList *data = (CTreeList*)GetItemData(hItem);
		if (data != NULL)
		{
			CMainFrame *p = DYNAMIC_DOWNCAST(CMainFrame,AfxGetMainWnd());
			p->SendMessage(WM_USER_MSG_TREELIST, WPARAM(nMenuID),(LPARAM)(CXTTreeCtrl*)data);
		}	// if (data != NULL)
	}	// if (hItem != NULL)
	else
	{
		CMainFrame *p = DYNAMIC_DOWNCAST(CMainFrame,AfxGetMainWnd());
		p->SendMessage(WM_USER_MSG_TREELIST, WPARAM(nMenuID),(LPARAM)(CXTTreeCtrl*)NULL);
	}	// if (hItem != NULL)
}

void CPADTreeCtrl::OnPopupMenuItemExpandCollapse(UINT nMenuID)
{
	HTREEITEM hItem = NULL;
	switch (nMenuID)
	{
		case ID_POPUP_EXPAND_TREE :
		{
			hItem = GetFirstVisibleItem();
			if (hItem != NULL)
			{
				while (hItem != NULL)
				{
					Expand(hItem,TVE_EXPAND);
					
					hItem = GetNextItem(hItem,TVGN_NEXTVISIBLE);
				}
			}	// if (hItem != NULL)
			break;
		}	// case ID_POPUP_EXPAND_TREE :
		case ID_POPUP_COLLAPSE_TREE :
		{
			hItem = GetFirstVisibleItem();
			if (hItem != NULL)
			{
				while (hItem != NULL)
				{
					Expand(hItem,TVE_COLLAPSE);
					
					hItem = GetNextItem(hItem,TVGN_NEXTVISIBLE);
				}
			}	// if (hItem != NULL)
			break;
		}	// case ID_POPUP_EXPAND_TREE :
	};
}


/////////////////////////////////////////////////////////////////////////////
// class CPADExtTreeCtrl

IMPLEMENT_DYNAMIC( CPADExtTreeCtrl, CTreeCtrl )

BEGIN_MESSAGE_MAP( CPADExtTreeCtrl, CTreeCtrl )
	//{{AFX_MSG_MAP(CPADExtTreeCtrl)
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND_RANGE(ID_POPUP_EXPAND_TREE,ID_POPUP_CHANGE_ITEM_TYPE, OnPopupMenuItem)

	ON_NOTIFY_REFLECT(TVN_BEGINLABELEDIT, OnBeginlabeledit)
	ON_NOTIFY_REFLECT(TVN_ENDLABELEDIT, OnEndlabeledit)
	ON_NOTIFY_REFLECT(TVN_BEGINDRAG, OnBeginDrag)

	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPADExtTreeCtrl::CPADExtTreeCtrl()
{
	m_menuItemSelected = -1;
	m_bLDragging = FALSE;
	m_pTreeEdit = NULL;
	m_nSelectedLevel = -1;
	m_bIsTopLevelItem = FALSE;
	m_bIsDropTarget = FALSE;
}

CPADExtTreeCtrl::~CPADExtTreeCtrl()
{
}

void CPADExtTreeCtrl::setSuitePathAndFN(LPCTSTR fn)
{
	m_sSuitePathAndFN = fn;

}


void CPADExtTreeCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CString S;
	CTreeList *pRec = NULL;
	int nSelectedLevel;
	int nNextLevel;
	BOOL bCheckStatus;
	HTREEITEM hTreeItem = HitTest(point, &nFlags);
	HTREEITEM hHitItem = hTreeItem;
	// Get selected item and find out which items to change, based on selection.
  if ((hTreeItem != NULL))
  {
		if (TVHT_ONITEMSTATEICON & nFlags)
		{
			Select(hTreeItem, TVGN_CARET);

			nSelectedLevel = getLevel(hTreeItem);
			bCheckStatus = !getCheckStatus(*this,hTreeItem);
	
			if (ItemHasChildren(hTreeItem))
			{

				while (hTreeItem != NULL)
				{
					hTreeItem = GetNextTreeItem(*this,hTreeItem);
			
					nNextLevel = getLevel(hTreeItem);
					if (nNextLevel > nSelectedLevel)
					{
						SetCheck(hTreeItem,bCheckStatus);
					}
					else
						break;
				}	// while (hTreeItem != NULL)
			}	// if (ItemHasChildren(hTreeItem))

			// Setup parents, from hit item; 060428 p�d
			setTreeParents(*this,hHitItem,bCheckStatus);
		} // if (TVHT_ONITEMSTATEICON & nFlags)
		else
		{
			if (TVHT_ONITEM & nFlags)
			{
				pRec = (CTreeList*)GetItemData(hTreeItem);
				if (pRec != NULL)
				{
					m_bIsTopLevelItem = (_tcscmp(pRec->getNodeName(),MEXEC_ITEM) == 0);
				}
			}	// if (TVHT_ONITEM & nFlags)

		}
	}	// if ((hTreeItem != NULL))

	CTreeCtrl::OnLButtonDown(nFlags,point);
}

void CPADExtTreeCtrl::OnRButtonDown(UINT nFlags, CPoint point) 
{
	HTREEITEM hSelItem = GetSelectedItem();

	BOOL bOnItem = FALSE;
	CMenu menu;
	menu.LoadMenu(IDR_SHOW_HIDE_TREE);
	CMenu* pPopup = menu.GetSubMenu(0);

	HTREEITEM hItem = HitTest(point, &nFlags);
  if ((hItem != NULL) && (TVHT_ONITEM & nFlags))
  {
		bOnItem = TRUE;
		Select(hItem, TVGN_CARET);
	}
	pPopup->ModifyMenu(0,MF_BYPOSITION | MF_STRING,ID_POPUP_EXPAND_TREE,(getResStr(IDS_STRING151)));
	pPopup->InsertMenu(1,MF_BYPOSITION | MF_STRING,ID_POPUP_COLLAPSE_TREE,(getResStr(IDS_STRING152)));
	if (_tcscmp(extractFileName(m_sSuitePathAndFN),SHELLDATA_ShortcutsFN) == 0)
	{
		pPopup->InsertMenu(2,MF_BYPOSITION | MF_SEPARATOR);
		if (!bOnItem)
		{
			pPopup->InsertMenu(3,MF_BYPOSITION | MF_STRING,ID_POPUP_ADD_MAINITEM,(getResStr(IDS_STRING153)));
		}
		pPopup->InsertMenu(4,MF_BYPOSITION | MF_STRING,ID_POPUP_DEL_MAINITEM,(getResStr(IDS_STRING154)));
		pPopup->InsertMenu(5,MF_BYPOSITION | MF_STRING,ID_POPUP_CHANGE_ITEMNAME,(getResStr(IDS_STRING155)));
		pPopup->InsertMenu(6,MF_BYPOSITION | MF_STRING,ID_POPUP_CHANGE_ITEM_TYPE,(getResStr(IDS_STRING173)));
	}
	ClientToScreen (&point);
	UINT cmd = TrackPopupMenu(pPopup->GetSafeHmenu(),
				TPM_LEFTALIGN | TPM_TOPALIGN | TPM_RIGHTBUTTON,
					point.x, point.y, 0, GetSafeHwnd(), 0);
}

void CPADExtTreeCtrl::OnPopupMenuItem(UINT nMenuID)
{
	CString sChangeSubLevelMsg1;
	CString sChangeSubLevelMsg2;
	HTREEITEM hItem = NULL;
	switch (nMenuID)
	{
		case ID_POPUP_EXPAND_TREE :
		{
			ExpandTree(GetFirstVisibleItem());
			break;
		}	// case ID_POPUP_EXPAND_TREE :

		case ID_POPUP_COLLAPSE_TREE :
		{
			CollapseTree(GetFirstVisibleItem());
			break;
		}	// case ID_POPUP_EXPAND_TREE :
		
		// Add main item
		case ID_POPUP_ADD_MAINITEM :
		{
			CollapseTree(GetFirstVisibleItem());

			hItem =	InsertItem(_T("Ny"),0,0,TVI_ROOT,TVI_LAST);
			if (hItem)
			{
				CTreeList *pRec = new CTreeList(_T(""),_T(""),(MAIN_ITEM),_T(""),1,1,_T(""),_T(""),-1,
																				(m_sSuitePathAndFN),1,GetCount(),_T(""),1,_T(""),_T(""));
				SetItemState(hItem, TVIS_BOLD, TVIS_BOLD);
				SetItemData(hItem,DWORD_PTR(pRec));
				SetCheck(hItem);
				input(hItem);
			}
			break;
		}

		// Delete main item
		case ID_POPUP_DEL_MAINITEM :
		{
			hItem =	GetSelectedItem();
			if (hItem)
			{
				if (GetCount() > 1)
				{
					DeleteItem(hItem);
				}
				else
				{
					// Tell user that he can't remove this item; 070322 p�d
					::MessageBox(this->GetSafeHwnd(),getResStr(IDS_STRING164),getResStr(IDS_STRING900),MB_ICONEXCLAMATION | MB_OK);
				}
			}	// if (hItem)
			break;
		} // case ID_POPUP_DEL_MAINITEM

		// Change name of an MainItem
		case ID_POPUP_CHANGE_ITEMNAME :
		{
			hItem =	GetSelectedItem();
			if (hItem)
			{
				input(hItem);
				m_nSelectedLevel = getLevel(hItem);
			}	// if (hItem)
			break;
		}	// case ID_POPUP_CHANGE_ITEMNAME

		// Change type of from MEXEC_ITEM to EXEC_ITEM,
		// so item can be added to a sublevel; 081024 p�d
		case ID_POPUP_CHANGE_ITEM_TYPE :
		{
			hItem =	GetSelectedItem();
			if (hItem)
			{
				CTreeList *pRec = (CTreeList*)GetItemData(hItem);
				if (pRec != NULL)
				{
					if (_tcscmp(pRec->getNodeName(),MEXEC_ITEM) == 0)
					{
						pRec->setNodeName(EXEC_ITEM);
						SetItemData(hItem,(DWORD_PTR)pRec);
						m_nSelectedLevel = getLevel(hItem);

						sChangeSubLevelMsg1.Format(_T("%s\n%s\n"),
							getResStr(IDS_STRING174),
							getResStr(IDS_STRING175));
						::MessageBox(this->GetSafeHwnd(),sChangeSubLevelMsg1,getResStr(IDS_STRING900),MB_ICONEXCLAMATION | MB_OK);
					}	// if (_tcscmp(pRec->getNodeName(),MEXEC_ITEM) == 0)
					else if (_tcscmp(pRec->getNodeName(),EXEC_ITEM) == 0)
					{
						pRec->setNodeName(MEXEC_ITEM);
						SetItemData(hItem,(DWORD_PTR)pRec);
						m_nSelectedLevel = getLevel(hItem);

						sChangeSubLevelMsg2.Format(_T("%s\n%s\n"),
							getResStr(IDS_STRING176),
							getResStr(IDS_STRING177));
						::MessageBox(this->GetSafeHwnd(),sChangeSubLevelMsg2,getResStr(IDS_STRING900),MB_ICONEXCLAMATION | MB_OK);
					}	// if (_tcscmp(pRec->getNodeName(),MEXEC_ITEM) == 0)
					else
						::MessageBox(this->GetSafeHwnd(),getResStr(IDS_STRING178),getResStr(IDS_STRING900),MB_ICONEXCLAMATION | MB_OK);

				}
			}	// if (hItem)
			break;
		}	// case ID_POPUP_CHANGE_ITEM_TYPE

	};
	m_menuItemSelected = nMenuID;
}


void CPADExtTreeCtrl::OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	*pResult = 0;

	m_hitemDrag = pNMTreeView->itemNew.hItem;
	m_hitemDrop = NULL;

	m_pDragImage = CreateDragImage(m_hitemDrag);  // get the image list for dragging
	// CreateDragImage() returns NULL if no image list
	// associated with the tree view control
	if( !m_pDragImage )
		return;

	m_bLDragging = TRUE;
	m_pDragImage->BeginDrag(0, CPoint(-15,-15));
	POINT pt = pNMTreeView->ptDrag;
	ClientToScreen( &pt );
	m_pDragImage->DragEnter(NULL, pt);
	SetCapture();

}

void CPADExtTreeCtrl::OnMouseMove(UINT nFlags, CPoint point)
{
	HTREEITEM	hitem;
	UINT		flags;

	if (m_bLDragging)
	{
		POINT pt = point;
		ClientToScreen( &pt );
		CImageList::DragMove(pt);
		if ((hitem = HitTest(point, &flags)) != NULL)
		{
			CTreeList *pRec = (CTreeList*)GetItemData(hitem);
			if (pRec != NULL)
			{
				// Check if we're on a drop-target; 081024 p�d
				m_bIsDropTarget = (_tcscmp(pRec->getNodeName(),MAIN_ITEM) == 0);
			}
			CImageList::DragShowNolock(FALSE);
			if (!m_bIsTopLevelItem && m_bIsDropTarget)
			{
				SelectDropTarget(hitem);
				m_hitemDrop = hitem;
			}
			CImageList::DragShowNolock(TRUE);
		}
	}

	CTreeCtrl::OnMouseMove(nFlags, point);
}

void CPADExtTreeCtrl::OnLButtonUp(UINT nFlags, CPoint point)
{
	CTreeCtrl::OnLButtonUp(nFlags, point);

	if (m_bLDragging)
	{
		m_bLDragging = FALSE;
		CImageList::DragLeave(this);
		CImageList::EndDrag();
		ReleaseCapture();
		delete m_pDragImage;
		if(m_bIsDropTarget)
		{
			// Remove drop target highlighting
			SelectDropTarget(NULL);

			if( m_hitemDrag == m_hitemDrop )
				return;

			// If Drag item is an ancestor of Drop item then return
			HTREEITEM htiParent = m_hitemDrop;
			while( (htiParent = GetParentItem( htiParent )) != NULL )
			{
				if( htiParent == m_hitemDrag ) return;
			}

			Expand( m_hitemDrop, TVE_EXPAND ) ;

			HTREEITEM htiNew = CopyBranch( m_hitemDrag, m_hitemDrop, TVI_FIRST );
		
			DeleteItem(m_hitemDrag);
			SelectItem( htiNew );
			m_bIsTopLevelItem = FALSE;	// reset
		}
	}
}

void CPADExtTreeCtrl::OnBeginlabeledit(NMHDR* pNMHDR, LRESULT* pResult)
{
	TV_DISPINFO* pTVDispInfo = (TV_DISPINFO*)pNMHDR;
	
	GetEditControl()->LimitText( 128 );
	
	*pResult = 0;
}

void CPADExtTreeCtrl::OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult)
{
	CTreeList *pRec = NULL;
	TV_DISPINFO* pTVDispInfo = (TV_DISPINFO*)pNMHDR;
	if (pTVDispInfo->item.pszText)
	{
		SetItemText(pTVDispInfo->item.hItem,(pTVDispInfo->item.pszText));
		pRec = (CTreeList *)GetItemData(pTVDispInfo->item.hItem);
		pRec->setItemtext((pTVDispInfo->item.pszText));
	}
}

void CPADExtTreeCtrl::OnItemCopied(HTREEITEM /*hItem*/, HTREEITEM /*hNewItem*/ )
{
        // Virtual function 
}

HTREEITEM CPADExtTreeCtrl::CopyBranch( HTREEITEM htiBranch, HTREEITEM htiNewParent, HTREEITEM htiAfter /*= TVI_LAST*/ )
{
  HTREEITEM hChild;

  HTREEITEM hNewItem = CopyItem( htiBranch, htiNewParent, htiAfter );
  hChild = GetChildItem(htiBranch);
  while( hChild != NULL)
  {
          // recursively transfer all the items
          CopyBranch(hChild, hNewItem);  
          hChild = GetNextSiblingItem( hChild );
  }
  return hNewItem;
}

HTREEITEM CPADExtTreeCtrl::CopyItem( HTREEITEM hItem, HTREEITEM htiNewParent,HTREEITEM htiAfter /*= TVI_LAST*/ )
{
        TV_INSERTSTRUCT         tvstruct;
        HTREEITEM               hNewItem;
        CString                 sText;

        // get information of the source item
        tvstruct.item.hItem = hItem;
        tvstruct.item.mask = TVIF_CHILDREN | TVIF_HANDLE | 
                                TVIF_IMAGE | TVIF_SELECTEDIMAGE;
        GetItem(&tvstruct.item);  
        sText = GetItemText( hItem );
      
        tvstruct.item.cchTextMax = sText.GetLength();
        tvstruct.item.pszText = sText.LockBuffer();

        // Insert the item at proper location
        tvstruct.hParent = htiNewParent;
        tvstruct.hInsertAfter = htiAfter;
        tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
        hNewItem = InsertItem(&tvstruct);
        sText.ReleaseBuffer();

        // Now copy item data and item state.
        SetItemData( hNewItem, GetItemData( hItem ));
        SetItemState( hNewItem, GetItemState( hItem, TVIS_STATEIMAGEMASK ), 
                                                        TVIS_STATEIMAGEMASK );

        // Call virtual function to allow further processing in derived class
        OnItemCopied( hItem, hNewItem );

        return hNewItem;
}

// PROTEDTED
void CPADExtTreeCtrl::ExpandTree(HTREEITEM hItem)
{
	if (hItem != NULL)
	{
		while (hItem != NULL)
		{
			Expand(hItem,TVE_EXPAND);
			
			hItem = GetNextItem(hItem,TVGN_NEXTVISIBLE);
		}
	}	// if (hItem != NULL)
}

void CPADExtTreeCtrl::CollapseTree(HTREEITEM hItem)
{
	if (hItem != NULL)
	{
		while (hItem != NULL)
		{
			Expand(hItem,TVE_COLLAPSE);
			
			hItem = GetNextItem(hItem,TVGN_NEXTVISIBLE);
		}
	}	// if (hItem != NULL)
}


/////////////////////////////////////////////////////////////////////////////
// CDockingFrame; this Frame Window is child of the CXTPDockingPane

IMPLEMENT_DYNCREATE(CDockingFrame, CXTPFrameWnd)

CDockingFrame::CDockingFrame()
{
}

CDockingFrame::~CDockingFrame()
{
	if (m_ilPaneIconsSmall.GetSafeHandle() != NULL)
	{
		m_ilPaneIconsSmall.DeleteImageList();
	}
	if (m_ilPaneIconsLarge.GetSafeHandle() != NULL)
	{
		m_ilPaneIconsLarge.DeleteImageList();
	}
	// Free allocated memory for NavigationPanes; 051205 p�d
	for (UINT kk = 0;kk < m_vecPanes.size();kk++)
	{
		if (m_vecPanes[kk].getNavPane() != NULL)
			delete m_vecPanes[kk].getNavPane();
	}

	// Save state of Naviagtionbar; 051115 p�d
	m_wndNavigationBar.SaveState(NAVIGAITONBARS_SETUP);
}

BOOL CDockingFrame::Create(CWnd *parent)
{
	return	CXTPFrameWnd::Create(NULL, NULL, WS_CHILD|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), parent, 0);
}

BEGIN_MESSAGE_MAP(CDockingFrame, CXTPFrameWnd)
	//{{AFX_MSG_MAP(CDockingFrame)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CDockingFrame::clearImageLists(void)
{
	if (m_ilPaneIconsSmall.GetSafeHandle() != NULL)
	{
		m_ilPaneIconsSmall.DeleteImageList();
	}
	m_ilPaneIconsSmall.Create(16, 16, ILC_MASK|ILC_COLOR32, 1, 1);

	if (m_ilPaneIconsLarge.GetSafeHandle() != NULL)
	{
		m_ilPaneIconsLarge.DeleteImageList();
	}
	m_ilPaneIconsLarge.Create(24, 24, ILC_MASK|ILC_COLOR32, 1, 1);
}

/////////////////////////////////////////////////////////////////////////////
// CDockingFrame message handlers

int CDockingFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{

	if (CXTPFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	m_wndNavigationBar.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CXTPEmptyRect(),this,AFX_IDW_PANE_FIRST);
	m_wndNavigationBar.SetOwner(this);
	m_wndNavigationBar.GetToolTipContext()->SetStyle(xtpToolTipOffice);
	
	return 0;
}

void CDockingFrame::setTreeListVec(vecTreeList &v)
{
	m_vecTreeList = v;
	// Setup icons for navigation bar; 051116 p�d
	CString sIconPath = L"",sDefaultIconPath = L"";
	clearImageLists();
	sDefaultIconPath.Format(L"%s\\%s",getShellDataDir(),L"default.ico");
	for (UINT i = 0;i < m_vecTreeList.size();i++)
	{
		CTreeList tl = m_vecTreeList[i];
		if (tl.getCheck() != NO_VALUE)
		{
			sIconPath.Format(_T("%s\\%s"),getShellDataDir(),tl.getIcon());
			if (fileExists(sIconPath))
			{
				HICON	hIcon1 = (HICON)::LoadImage( NULL,(LPCTSTR) sIconPath, IMAGE_ICON, 24, 24, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
				if (hIcon1 != NULL)
				{
					m_ilPaneIconsLarge.Add(hIcon1);
				}
				HICON	hIcon2 = (HICON)::LoadImage( NULL,(LPCTSTR) sIconPath, IMAGE_ICON, 16, 16, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
				if (hIcon2 != NULL)
				{
					m_ilPaneIconsSmall.Add(hIcon2);
				}
			}	// if (fileExists(sIconPath))
			// Lagt till kontroll av om icon saknas f�r svit i navigeringstr�det, s� l�ggs en "default" ikon in.
			else
			{
				HICON	hIcon1 = (HICON)::LoadImage( NULL,(LPCTSTR) sDefaultIconPath, IMAGE_ICON, 24, 24, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
				if (hIcon1 != NULL)
				{
					m_ilPaneIconsLarge.Add(hIcon1);
				}
				HICON	hIcon2 = (HICON)::LoadImage( NULL,(LPCTSTR) sDefaultIconPath, IMAGE_ICON, 16, 16, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
				if (hIcon2 != NULL)
				{
					m_ilPaneIconsSmall.Add(hIcon2);
				}
			}
		}	// if (tl.getID() != NO_VALUE)
	} // for (UINT i = 0;i < list.size();i++)
}

void CDockingFrame::setVisiblePanes(vecVisibleItemsOnNavPane &vec) const
{
	int nItems = m_wndNavigationBar.GetItemCount();
	CXTPShortcutBarItem *pItem = NULL;
	if (m_wndNavigationBar.GetSafeHwnd() != NULL)
	{
		for (int i = 0;i < nItems;i++)
		{
			pItem = m_wndNavigationBar.GetItem(i);
			if (pItem != NULL)
			{
				if (pItem->GetID() >= 0)
				{
					// Try to find Pane caption in vec and check if
					// it's supposed be visible or not; 081208 p�d
					if (vec.size() > 0)
					{
						for (UINT ii = 0;ii < vec.size();ii++)
						{
							if (vec[ii].getCaption().CompareNoCase(pItem->GetCaption()) == 0)
							{
								pItem->SetVisible(vec[ii].getIsVisible());
								break;
							}	// if (vec[ii].getCaption().CompareNoCase(pItem->GetCaption()) == 0)
						}	// for (UINT ii = 0;ii < vec.size();ii++)
					}	// if (vec.size() > 0)
				}	// if (pItem->GetID() >= 0)
			}	// if (pItem != NULL)
		}	// for (int i = 0;i < nItems;i++)
	}	// if (m_wndNavigationBar.GetSafeHwnd() != NULL)
}

void CDockingFrame::addPanes(int id)
{
	UINT i;
	UINT nCounter = 0;
	DWORD nItemID = 0;
	static UINT *arr_Shortcuts;
	CTreeList *data = NULL;
	SetRedraw(FALSE);
	// Free allocated memory for NavigationPanes; 051205 p�d
	for (UINT kk = 0;kk < m_vecPanes.size();kk++)
	{
		if (m_vecPanes[kk].getNavPane() != NULL)
			delete m_vecPanes[kk].getNavPane();
	}
	// Clear vector
	m_vecPanes.clear();
	// Remove all navigation panes from NavogationBar
	m_wndNavigationBar.RemoveAllItems();
	// Setup the Panes in the Navigationbar
	if (m_vecTreeList.size() > 0)
	{
		// Setup panes in vector
		for (i = 0;i < m_vecTreeList.size();i++)
		{
			CTreeList tl = m_vecTreeList[i];
			if (tl.getCheck() != NO_VALUE)
			{
				m_vecPanes.push_back(CNavPaneInfo(new CNavigationPane(),
																					tl.getCaption(),
																					tl.getNodeName(),
																					tl.getSuitePath(),
																					tl));
				nCounter++;
			}	// if (tl.getID() != NO_VALUE)
		} // for (UINT i = 0;i < list.size();i++)
	}		

	if (m_vecPanes.size() > 0)
	{

		arr_Shortcuts = new UINT[m_vecPanes.size()];	
	
		for (i = 0;i < m_vecPanes.size();i++)
		{
			CNavigationPane *m_pane = (CNavigationPane *)m_vecPanes[i].getNavPane();

			if (m_pane)
			{

				arr_Shortcuts[i] = i+1;
				m_pane->Create((m_vecPanes[i].getCaption()),(getResStr(IDS_STRING133)), &m_wndNavigationBar);

				if (m_wndNavigationBar.GetSafeHwnd() != NULL)
				{
					// Setup data in the ShellTree; 051115 p�d
					m_pane->setupShellTreeData(m_vecTreeList,m_vecPanes[i].getSuitePath(),TRUE);
					// Get navigation pane item(s); 051205 p�d
					//CTreeList tl1 = m_vecPanes[i].getTreeList();
					CXTPShortcutBarItem* pItem = m_wndNavigationBar.AddItem(i+1, m_pane);
					// Set caption in the bottom of navigationbar equal to Caption on the top/suite; 051114 p�d
					pItem->SetCaption((m_vecPanes[i].getCaption()));
					pItem->SetTooltip((m_vecPanes[i].getCaption()));
					pItem->SetID(i+1);
					data = new CTreeList(m_vecPanes[i].getTreeList());
					pItem->SetItemData((DWORD_PTR)data);

					// Select administration pane by default (if no pane is selected)
					if( id < 0 )
					{
						CString cmp(_T("administration.xml"));
						CString l(CString(m_vecPanes[i].getTreeList().getShellDataFile()).Right(cmp.GetLength()));
						if( CString(m_vecPanes[i].getTreeList().getShellDataFile()).Right(cmp.GetLength()).CompareNoCase(cmp) == 0 )
						{
							id = i;
						}
					}

					// Set item id, based on nodename; 060425 p�d
					if (id == i && id < m_vecPanes.size() && pItem)
					{
						m_wndNavigationBar.SelectItem(pItem);
					}
				}	// if (m_wndNavigationBar.GetSafeHwnd() != NULL)
			}	// if (m_pane)
		}	// for (unsigned int i = 0;i < m_vecPanes.size();i++)
	
		XTPImageManager()->SetIcons(m_ilPaneIconsLarge,arr_Shortcuts,nCounter,CSize(24,24));
		XTPImageManager()->SetIcons(m_ilPaneIconsSmall,arr_Shortcuts,nCounter,CSize(16,16));
		
		if (arr_Shortcuts)
			delete arr_Shortcuts;

	} // if (m_vecPanes.size() > 0)

	m_wndNavigationBar.SetExpandedLinesCount(m_wndNavigationBar.GetItemCount());
	m_wndNavigationBar.LoadState(NAVIGAITONBARS_SETUP);

	SetRedraw(TRUE);
	RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);
}

void CDockingFrame::updPane(UINT index)
{
	SetRedraw(FALSE);
	if (m_vecPanes.size() > 0 && index < m_vecPanes.size())
	{
		CNavigationPane *m_pane = (CNavigationPane *)m_vecPanes[index].getNavPane();

		if (m_pane)
		{

			if (m_wndNavigationBar.GetSafeHwnd() != NULL)
			{
				m_pane->clearShellTreeData();
				// Setup data in the ShellTree; 051115 p�d
				m_pane->setupShellTreeData(m_vecTreeList,m_vecPanes[index].getSuitePath(),FALSE);
			}	// if (m_wndNavigationBar.GetSafeHwnd() != NULL)	

		}	// if (m_pane)
	} // if (m_vecPanes.size() > 0)
	SetRedraw(TRUE);
	RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);

}

void CDockingFrame::getItemsToShowHide(UINT index,vecInt& vec)
{
	if (m_vecPanes.size() > 0 && index < m_vecPanes.size())
	{
		CNavigationPane *m_pane = (CNavigationPane *)m_vecPanes[index].getNavPane();

		if (m_pane)
		{
			if (m_wndNavigationBar.GetSafeHwnd() != NULL)
			{
				m_pane->getItemsToShowHide(vec);
			}	// if (m_wndNavigationBar.GetSafeHwnd() != NULL)	
		}	// if (m_pane)
	} // if (m_vecPanes.size() > 0)
}

BOOL CDockingFrame::getPaneIDForSuite(LPCTSTR suite,int *pane_id)
{
	CNavPaneInfo rec;
	CString sSuiteName;
	if (m_vecPanes.size() > 0)
	{
		for (UINT i = 0;i < m_vecPanes.size();i++)
		{
			rec = m_vecPanes[i];
			sSuiteName = extractFileName(rec.getSuitePath());
			if (_tcscmp(sSuiteName,SHELLDATA_ShortcutsFN) == 0)
			{
				*pane_id = i;
				return TRUE;
			}	// if (_tcscmp(sSuiteName,SHELLDATA_ShortcutsFN) == 0)
		}	// for (UINT i = 0;i < m_vecPanes.size();i++)
	}	// if (m_vecPanes.size() > 0)
	return FALSE;
}

//////////////////////////////////////////////////////////////////////
// CNavigationPane; holds the TreeView
IMPLEMENT_DYNAMIC( CNavigationPane, CXTPShortcutBarPane )


BEGIN_MESSAGE_MAP( CNavigationPane, CXTPShortcutBarPane )
	//{{AFX_MSG_MAP(CNavigationPane)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNavigationPane::CNavigationPane()
{
	m_ilTreeIcons.Create(16, 16, ILC_MASK|ILC_COLOR32, 1, 1);
	HICON hIcon = NULL;

	// changed how to load the icons from the resource. (091012 ag)
	hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 54);	// RSTR_TREE_FOLDER_CLOSED
	if (hIcon) m_ilTreeIcons.Add(hIcon);

	hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 55);	// RSTR_TREE_FOLDER_OPEN
	if (hIcon) m_ilTreeIcons.Add(hIcon);

	hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 56);	// RSTR_TREE_FORM
	if (hIcon) m_ilTreeIcons.Add(hIcon);

	hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 57);	// RSTR_TREE_REPORT
	if (hIcon) m_ilTreeIcons.Add(hIcon);

	hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 58);	// RSTR_TREE_HELP
	if (hIcon) m_ilTreeIcons.Add(hIcon);
}

CNavigationPane::~CNavigationPane()
{

}

BOOL CNavigationPane::Create(LPCTSTR lpszCaption,LPCTSTR lpszTodoCaption, CXTPShortcutBar* pParent)
{
	if (!CXTPShortcutBarPane::Create(lpszCaption, pParent))
		return FALSE;

	m_wndTreeCtrl.Create(WS_VISIBLE|TVS_HASBUTTONS|TVS_LINESATROOT|TVS_HASLINES, CXTPEmptyRect(), this, AFX_IDW_PANE_FIRST);

	m_wndTreeCtrl.SetImageList(&m_ilTreeIcons, TVSIL_NORMAL);

	m_sToDoCaption = lpszTodoCaption;

	return TRUE;
}

/*
#define ID_TREECTRL_FOLDER_ITEM				0
#define ID_TREECTRL_EXEC_ITEM					2
#define ID_TREECTRL_REPORT_ITEM				3
#define ID_TREECTRL_HELP_ITEM					4
*/

void CNavigationPane::setupShellTreeData(vecTreeList &list,LPCTSTR suite_fn,BOOL do_add)
{
	HTREEITEM rootNode	  = NULL;
	HTREEITEM childNode1  = NULL;
	HTREEITEM childNode2  = NULL;
	HTREEITEM childNode3  = NULL;
	HTREEITEM childNode4  = NULL;
	HTREEITEM execNode	  = NULL;
	int nVisible,nItemIndex,nIndex,nLevel;
	TCHAR sCaption[50];
	TCHAR sNodeName[50];
	TCHAR sText[50];
	TCHAR sAttr_Func[50];
	TCHAR sAttr_Suite[64];
	HINSTANCE hModule = NULL;

	for (UINT j = 0;j < list.size();j++)
	{
		CTreeList *tree = new CTreeList(list[j]);
		nVisible	= tree->getVisible();
		if (nVisible > 0)
		{
			_tcscpy(sCaption,tree->getCaption());	
			_tcscpy(sNodeName,tree->getNodeName());	
			// Check if ItemText holds text, if so use ItemText instead of
			// Resourcefile string id value; 060503 p�d
			if (_tcscmp(tree->getItemText(),_T("")) != 0)
			{
				_tcscpy(sText,tree->getItemText());
			}
			else
			{
				_tcscpy(sText,getResStr(tree->getStrID()));// tree->getElement());	
			}

			_tcscpy(sAttr_Func,tree->getFunc());	
			_tcscpy(sAttr_Suite,tree->getSuite());	
			nIndex		= tree->getIndex();
			nLevel = tree->getLevel();

			// Check that data is from the same suite
			if (_tcscmp(suite_fn,tree->getSuitePath()) == 0)
			{		
				if (_tcscmp(sCaption,GetCaption()) == 0)
				{
					if (_tcscmp(sNodeName,MAIN_ITEM) == 0)
					{
						rootNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_FOLDER_ITEM,ID_TREECTRL_FOLDER_ITEM,TVI_ROOT);
						m_wndTreeCtrl.SetItemState(rootNode, TVIS_BOLD, TVIS_BOLD);
						m_wndTreeCtrl.SetItemData(rootNode,DWORD_PTR(tree));
					} // if (_tcscmp(sNodeName,MAIN_ITEM) == 0)
					if (_tcscmp(sNodeName,MEXEC_ITEM) == 0)
					{
						rootNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_MEXEC_ITEM,ID_TREECTRL_MEXEC_ITEM,TVI_ROOT);
						//m_wndTreeCtrl.SetItemState(rootNode, TVIS_BOLD, TVIS_BOLD);
						m_wndTreeCtrl.SetItemData(rootNode,DWORD_PTR(tree));
					} // if (_tcscmp(sNodeName,MAIN_ITEM) == 0)
					// Added 100630 p�d
					if (_tcscmp(sNodeName,MHELP_ITEM) == 0)
					{
						rootNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_MHELP_ITEM,ID_TREECTRL_MHELP_ITEM,TVI_ROOT);
						//m_wndTreeCtrl.SetItemState(rootNode, TVIS_BOLD, TVIS_BOLD);
						m_wndTreeCtrl.SetItemData(rootNode,DWORD_PTR(tree));
					} // if (_tcscmp(sNodeName,MAIN_ITEM) == 0)
					else if (_tcscmp(sNodeName,SUB_ITEM1) == 0)
					{
						childNode1 = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_FOLDER_ITEM,ID_TREECTRL_FOLDER_ITEM,rootNode);
						m_wndTreeCtrl.SetItemData(childNode1,DWORD_PTR(tree));
					} // if (_tcscmp(sNodeName,SUB_ITEM1) == 0)
					else if (_tcscmp(sNodeName,SUB_ITEM2) == 0)
					{
						childNode2 = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_FOLDER_ITEM,ID_TREECTRL_FOLDER_ITEM,childNode1);
						m_wndTreeCtrl.SetItemData(childNode2,DWORD_PTR(tree));
					} // if (_tcscmp(sNodeName,SUB_ITEM2) == 0)
					else if (_tcscmp(sNodeName,SUB_ITEM3) == 0)
					{
						childNode3 = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_FOLDER_ITEM,ID_TREECTRL_FOLDER_ITEM,childNode2);
						m_wndTreeCtrl.SetItemData(childNode3,DWORD_PTR(tree));
					} // if (_tcscmp(sNodeName,SUB_ITEM3) == 0)
					else if (_tcscmp(sNodeName,SUB_ITEM4) == 0)
					{
						childNode4 = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_FOLDER_ITEM,ID_TREECTRL_FOLDER_ITEM,childNode3);
						m_wndTreeCtrl.SetItemData(childNode4,DWORD_PTR(tree));
					} // if (_tcscmp(sNodeName,SUB_ITEM4) == 0)
					else if (_tcscmp(sNodeName,EXEC_ITEM) == 0)
					{
						nItemIndex = nLevel - 2;
						if (nItemIndex == 0)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,rootNode);
						}
						else if (nItemIndex == 1)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode1);
						}
						else if (nItemIndex == 2)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode2);
						}
						else if (nItemIndex == 3)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode3);
						}
						else if (nItemIndex == 4)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode4);
						}
						// Add tree data to Data item in tree view; 050315 p�d
						m_wndTreeCtrl.SetItemData(execNode,DWORD_PTR(tree));
					}
					else if (_tcscmp(sNodeName,REPORT_ITEM) == 0)
					{
						nItemIndex = nLevel - 2;

						if (nItemIndex == 0)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_REPORT_ITEM,ID_TREECTRL_REPORT_ITEM,rootNode);
						}
						else if (nItemIndex == 1)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_REPORT_ITEM,ID_TREECTRL_REPORT_ITEM,childNode1);
						}
						else if (nItemIndex == 2)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_REPORT_ITEM,ID_TREECTRL_REPORT_ITEM,childNode2);
						}
						else if (nItemIndex == 3)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_REPORT_ITEM,ID_TREECTRL_REPORT_ITEM,childNode3);
						}
						else if (nItemIndex == 4)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_REPORT_ITEM,ID_TREECTRL_REPORT_ITEM,childNode4);
						}
						// Add tree data to Data item in tree view; 050315 p�d
						m_wndTreeCtrl.SetItemData(execNode,DWORD_PTR(tree));
					}
					else if (_tcscmp(sNodeName,HELP_ITEM) == 0)
					{
						nItemIndex = nLevel - 2;

						if (nItemIndex == 0)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_HELP_ITEM,ID_TREECTRL_HELP_ITEM,rootNode);
						}
						else if (nItemIndex == 1)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_HELP_ITEM,ID_TREECTRL_HELP_ITEM,childNode1);
						}
						else if (nItemIndex == 2)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_HELP_ITEM,ID_TREECTRL_HELP_ITEM,childNode2);
						}
						else if (nItemIndex == 3)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_HELP_ITEM,ID_TREECTRL_HELP_ITEM,childNode3);
						}
						else if (nItemIndex == 4)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_HELP_ITEM,ID_TREECTRL_HELP_ITEM,childNode4);
						}
						// Add tree data to Data item in tree view; 050315 p�d
						m_wndTreeCtrl.SetItemData(execNode,DWORD_PTR(tree));
					}
					else if (_tcscmp(sNodeName,EXTERN_ITEM) == 0)
					{
						nItemIndex = nLevel - 2;

						if (nItemIndex == 0)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,rootNode);
						}
						else if (nItemIndex == 1)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode1);
						}
						else if (nItemIndex == 2)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode2);
						}
						else if (nItemIndex == 3)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode3);
						}
						else if (nItemIndex == 4)
						{           
							execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode4);
						}
						// Add tree data to Data item in tree view; 050315 p�d
						m_wndTreeCtrl.SetItemData(execNode,DWORD_PTR(tree));
					}
				} // if (_tcscmp(sNodeName,EXEC_ITEM) == 0)
			}	// if (_tcscmp(sCaption,GetCaption()) == 0)
		}
	}	// for (unsigned int j = 0;j < list.size();j++)

	if (do_add)
	{
		AddItem((m_sToDoCaption), &m_wndTreeCtrl, 0);
	}

}

void CNavigationPane::getItemsToShowHide(vecInt& vec)
{
	CTreeList *pRec = NULL;
	HTREEITEM hTreeItem = m_wndTreeCtrl.GetSelectedItem();
	HTREEITEM hHitItem = hTreeItem;
	int nLevel = -1;
	int nHitLevel;
	if (hTreeItem != NULL)
	{
		nHitLevel = getLevel(m_wndTreeCtrl,hHitItem);						// Selected level
		pRec = (CTreeList*)m_wndTreeCtrl.GetItemData(hHitItem);	// Selected item
		vec.push_back(pRec->getCounter());
		while (hTreeItem != NULL)
		{
			if (nLevel <= nHitLevel && nLevel > -1)
				return;

			if (nLevel >= nHitLevel)
			{
				pRec = (CTreeList*)m_wndTreeCtrl.GetItemData(hTreeItem);
				vec.push_back(pRec->getCounter());
			}

			hTreeItem = GetNextTreeItem(m_wndTreeCtrl,hTreeItem);

			nLevel = getLevel(m_wndTreeCtrl,hTreeItem);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// Misc functions

BOOL getRSSXMLFile(CStringArray &str_array,long *size)
{
	// FTP get RSS xml file from the net; 060801 p�d
	W3Client w3;
	long lSize = 0;
	CString sBuffer;
	CString sMsg;
	TCHAR szRSS_Url[128],szRSS_FilePath[MAX_PATH];
	BOOL bReturn = FALSE;
	memset(&szRSS_Url, 0, sizeof(szRSS_Url));
	memset(&szRSS_FilePath, 0, sizeof(szRSS_FilePath));

	if(!getRSSLocation(szRSS_Url,szRSS_FilePath)) return FALSE;

    // get ISO639 abbrevation of the country and extend the url to include it. (091013 ag)
    TCHAR szBuf[64];
    CString sLangAbrev;
    CLanguageInfo linfo;
    sLangAbrev = getLangSet();
    if (getGlobLangInfo(sLangAbrev,linfo))
    {
        GetLocaleInfo(linfo.getLCID(), LOCALE_SISO3166CTRYNAME, szBuf, 64);
        _stprintf(szRSS_FilePath, _T("%s&lang=%s"), szRSS_FilePath, szBuf);
    }

	if(w3.Connect(szRSS_Url))
	{
		if(w3.Request(szRSS_FilePath))
		{
			int nRet;
			char buf[1025];
			memset(&buf, 0, sizeof(buf));

			while (nRet = w3.Response(reinterpret_cast<unsigned char *>(buf), 1024))
			{
				buf[nRet] = '\0';
				sBuffer += buf;
				lSize += nRet;
			}

			*size = lSize;
			str_array.RemoveAll();	// Clear list
			str_array.Add(sBuffer);

			bReturn = ((str_array.GetCount() > 0) ? 1 : 0);
		}
		w3.Close();
	}
	else
		AfxMessageBox(getResStr(IDS_STRING189));

	return bReturn;
}


BOOL makeDBConnection(SAConnection &con, bool silent /*=false*/)
{
	CString S;
	// Datbase info data members
	TCHAR m_sDB_PATH[127];
	TCHAR m_sDB_NAME[127];
	TCHAR m_sDB_LOCATION[127];
	TCHAR m_sUserName[127];
	TCHAR m_sPSW[127];
	SAClient_t m_saClient;
	int nAuthentication;
	BOOL bReturn = FALSE;
	try
	{
		// Setup connection for Database handling; 070122
		if (getDBUserInfo(m_sDB_PATH,m_sDB_NAME,m_sUserName,m_sPSW,&m_saClient,&nAuthentication))
		{
			S.Format(_T("m_sDB_PATH %s\nm_sDB_NAME %s\nm_sUserName %s\nm_sPSW %s\nnAuthentication %d\n"),m_sDB_PATH,m_sDB_NAME,m_sUserName,m_sPSW,nAuthentication);
//			AfxMessageBox(S);

			// Make sure DBLocation (server) is set; 090225 Peter
			m_sDB_LOCATION[0] = NULL;
			getDBLocation(m_sDB_LOCATION);
			if( m_sDB_LOCATION[0] )
			{
				if (m_saClient != SA_Client_NotSpecified)
				{
					con.setClient(m_saClient);
					//HMS-104 s�tt options p� uppkoppling 20230405 J�
					con.setOption(_TSA("UseAPI")) = ("OLEDB");
					con.setOption("SSPROP_INIT_TRUST_SERVER_CERTIFICATE") = _TSA("VARIANT_TRUE"); 	
					if (nAuthentication == 0 && 
							_tcscmp(m_sDB_PATH,_T("@")) != 0 &&
							_tcscmp(m_sDB_NAME,_T("")) != 0)
					{
						con.Connect(m_sDB_PATH, _T(""), _T(""), m_saClient);
					}
					else if (nAuthentication == 1 &&
									_tcscmp(m_sDB_PATH,_T("@")) != 0 &&
									_tcscmp(m_sUserName,_T("")) != 0 &&
									_tcscmp(m_sPSW,_T("")) != 0)
					{
						con.Connect(m_sDB_PATH, m_sUserName, m_sPSW, m_saClient);
					}
				}
				// If the connection isn't speciefied, we'll also
				// clear the register key for ServerType e.g. "Microsoft SQL Server"; 081030 p�d
				else
				{
					regSetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_DBSERVER,_T(""));
				}

				// Check SQL Server version, require min 2005
				CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
				if (pDB)
				{
					if (pDB->connectToDatabase(nAuthentication,m_sUserName,m_sPSW,m_sDB_LOCATION))
					{
						pDB->recordsetQuery(_T("SELECT CAST(LEFT(CAST(SERVERPROPERTY('productversion') AS varchar), PATINDEX('%.%', CAST(SERVERPROPERTY('productversion') AS varchar))-1) AS int) ver"));
						pDB->firstRec();
						int version = pDB->getInt(_T("ver"));
						pDB->recordsetClose();
						if(version < 10)
						{
							AfxMessageBox(getResStr(IDS_STRING198));
							con.Disconnect();
							return FALSE;
						}
					}
				}
			}
		}
	}
	catch(SAException &e)
	{
		if( !silent )
		{
			// print error message
			AfxMessageBox(e.ErrText());
		}
		return FALSE;
	}

	return (con.isConnected());
}

BOOL makeAdminDBConnection(SAConnection &con, bool silent/*=false*/)
{
	// Datbase info data members
	TCHAR szLocation[128];
	TCHAR szDBServerPath[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	int nAuthentication;

	SAClient_t client;
	BOOL bReturn = FALSE;

	int nReturnAccess;

	try
	{
		if(getDBInfo(HMS_ADMINISTRATOR_DBNAME,szDBServerPath,szDBUser,szDBPsw,&client,&nAuthentication))
		{
			// Create admin database if not exist; 090224 Peter
			CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
			if (pDB)
			{
				szLocation[0] = NULL;
				getDBLocation(szLocation);
				if( szLocation[0] != NULL )
				{
					if (pDB->connectToDatabase(nAuthentication,szDBUser,szDBPsw,szLocation))
					{
						// Make sure hms_administrator exist; 091007 Peter
						CString sql, sql2, table;
						sql.Format( _T("IF DB_ID('%s') IS NULL CREATE DATABASE %s"), HMS_ADMINISTRATOR_DBNAME, HMS_ADMINISTRATOR_DBNAME );
						if( !pDB->connectionExecute(sql) )
						{
							// Execution of statement failed, may be caused if user doesn't have appropriate permissions; 090527 Peter
							return FALSE;
						}
						//-----------------------------------------------------------------------------------
						// Check if logged in user has access to database; 090407 p�d
						sql.Format(_T("SELECT HAS_DBACCESS('%s') AS 'access'"),HMS_ADMINISTRATOR_DBNAME);
						if (pDB->recordsetQuery(sql))
						{
							if (pDB->getNumOf() > 0)
							{
								pDB->firstRec();
								nReturnAccess = pDB->getInt(_T("access"));
							}	// if (lNumOfRec > 0)
							pDB->recordsetClose();
							// Tell user; 090407 p�d
							if (nReturnAccess == 0)
								::MessageBox(0,getResStr(192),getResStr(900),MB_ICONSTOP | MB_OK);
						}
						//-----------------------------------------------------------------------------------
						if (nReturnAccess == 1)
						{
							sql = CString(_T("IF NOT EXISTS(SELECT 1 FROM ")) + HMS_ADMINISTRATOR_DBNAME + CString(_T("..sysobjects WHERE name LIKE N'")) + HMS_ADMINISTRATOR_TABLE + CString(_T("' AND xtype='U') "));
							table.Format( _T("%s%s%s"), HMS_ADMINISTRATOR_DBNAME, _T(".dbo."), HMS_ADMINISTRATOR_TABLE );
							sql2.Format( table_Administrator, table );
							sql += sql2;
							if( !pDB->connectionExecute(sql) )
							{
								// See comment above; 090527 Peter
								return FALSE;
							}
						}	// if (nReturnAccess == 1)
					}
				}
				else
				{
					return FALSE; // DBLocation (server) not specified
				}
				delete pDB;
				pDB = NULL;
			}
			else
			{
				return FALSE;
			}
			if (nReturnAccess == 1)
			{
				//HMS-104 s�tt options p� uppkoppling 20230405 J�
				con.setOption(_TSA("UseAPI")) = ("OLEDB");
				con.setOption("SSPROP_INIT_TRUST_SERVER_CERTIFICATE") = _TSA("VARIANT_TRUE"); 	
				if (nAuthentication == 0)
					con.Connect(SAString(szDBServerPath),_T(""),_T(""),client);
				else if (nAuthentication == 1)
					con.Connect(SAString(szDBServerPath),SAString(szDBUser),SAString(szDBPsw),client);
			}	// if (nReturnAccess == 1)
		}	// if(getDBInfo(HMS_ADMINISTRATOR_DBNAME,szDBServerPath,szDBUser,szDBPsw,&client,&nAuthentication))
	}
	catch(SAException &e)
	{
		if( !silent )
		{
			// print error message
			AfxMessageBox(e.ErrText());
		}
		return FALSE;
	}
	if (nReturnAccess == 1)	return (con.isConnected());
	else return FALSE;	// Not connected; 090407 p�d
}

int SplitString(const CString& input, const CString& delimiter, CStringArray& results)
{
	CString tmp;
  int iPos = 0;
  int newPos = -1;
  int sizeS2 = delimiter.GetLength();
  int isize = input.GetLength();

  CArray<INT, int> positions;

  newPos = input.Find(delimiter, 0);

  if( newPos < 0 ) { return 0; }

  int numFound = 0;

  while( newPos >= iPos )
  {
    numFound++;
    positions.Add(newPos);
    iPos = newPos;
    newPos = input.Find (delimiter, iPos+sizeS2); //+1);
  }

  for( int i = 0; i < positions.GetSize(); i++ )
  {
    CString s;
    if( i == 0 )
		{
      s = input.Mid( i, positions[i] );
		}
    else
    {
      int offset = positions[i-1] + sizeS2;
      if( offset < isize )
      {
        if ( i == positions.GetSize() )
				{
          s = input.Mid(offset);
				}
        else if ( i > 0 )
				{
          s = input.Mid( positions[i-1] + sizeS2, 
                 positions[i] - positions[i-1] - sizeS2 );
				}
      }
    }
    if( s.GetLength() >= 0 )
      results.Add(s);
  }
  return numFound;
}


int setupAdminDatabase(void)
{
	return runSQLScriptFileEx((HMS_ADMINISTRATOR_TABLE),(table_Administrator));
}


// Create database tables form SQL scriptfiles; 061109 p�d
int runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table)
{
	int nReturn = 1; // OK
	UINT nFileSize;
	char sBuffer[BUFFER_SIZE];	// 10 kb buffer

	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	SAClient_t m_saClient;
	int nAuthentication;

	BOOL bIsOK = FALSE;

	CString sSQL,S;

	try
	{

		if (fileExists(fn))
		{
			CFile file(fn,CFile::modeRead);
			nFileSize = file.Read(sBuffer,BUFFER_SIZE);
			file.Close();

			sBuffer[nFileSize] = '\0';

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					// Only check Location on Windows Authentication
					bIsOK = (_tcscmp(sLocation,_T("")) != 0); // &&
//									 _tcscmp(sDBName,"") != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
//									 _tcscmp(sDBName,"") != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;
				
				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{

							// Set Dabasename to hms_administrator; 061113 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							_tcscpy(sDBName,HMS_ADMINISTRATOR_DBNAME);
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,check_table))
								{
									pDB->setDefaultDatabase(sDBName);
									pDB->commandExecute(CString(sBuffer));
								}
								nReturn = 1; // OK

							}	// if (!pDB->existsDatabase(sDBName))
							else
							{
								sSQL.Format(_T("create database %s"),sDBName);
								if (pDB->commandExecute(sSQL))
								{
									pDB->setDefaultDatabase(sDBName);
									pDB->commandExecute(CString(sBuffer));
									nReturn = -1; // Had to create the administrator database
								}
							}
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (bIsOK)

			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
	}
	
	
	return nReturn;
}

// Create database table from string; 080627 p�d
int runSQLScriptFileEx(LPCTSTR table_name,CString script)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	int nReturn = 1;	// OK
	BOOL bIsOK = FALSE;

	CString sScript,sSQL;

	try
	{

		if (!script.IsEmpty())
		{
			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
//									 _tcscmp(sDBName,"") != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set Dabasename to hms_administrator; 080701 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 080701 p�d
							_tcscpy(sDBName,HMS_ADMINISTRATOR_DBNAME);
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,table_name))
								{
									pDB->setDefaultDatabase(sDBName);
									sScript.Format(script,table_name);
									pDB->commandExecute(sScript);
									nReturn = -1;	// Had to create the administrator table
								}
							}	// if (pDB->existsDatabase(sDBName))
							else
							{
								sSQL.Format(_T("create database %s"),sDBName);
								if (pDB->commandExecute(sSQL))
								{
									pDB->setDefaultDatabase(sDBName);
									sScript.Format(script,table_name);
									pDB->commandExecute(sScript);
									nReturn = -2; // Had to create the administrator database and table
								}
							}

						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		nReturn = 0;
	}
		
	return nReturn;
}


#pragma managed(push, on)
