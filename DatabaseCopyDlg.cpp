// DatabaseCopyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DatabaseCopyDlg.h"


// CDatabaseCopyDlg dialog

IMPLEMENT_DYNAMIC(CDatabaseCopyDlg, CDialog)

BEGIN_MESSAGE_MAP(CDatabaseCopyDlg, CDialog)
	ON_NOTIFY(NM_CLICK, IDC_LIST1, &CDatabaseCopyDlg::OnNMClickList1)
	ON_NOTIFY(NM_CLICK, IDC_LIST2, &CDatabaseCopyDlg::OnNMClickList2)
	ON_BN_CLICKED(IDC_BUTTON1, &CDatabaseCopyDlg::OnBnClickedButton1)
END_MESSAGE_MAP()

CDatabaseCopyDlg::CDatabaseCopyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDatabaseCopyDlg::IDD, pParent)
{
		m_bInitialized = FALSE;
}

CDatabaseCopyDlg::~CDatabaseCopyDlg()
{
}

void CDatabaseCopyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LIST1, m_wndFromLB);
	DDX_Control(pDX, IDC_LIST2, m_wndToLB);

	DDX_Control(pDX, IDC_INFO_LBL, m_wndLbl1);
	DDX_Control(pDX, IDC_FROM_LBL, m_wndLbl2);
	DDX_Control(pDX, IDC_TO_LBL, m_wndLbl3);
	DDX_Control(pDX, IDC_ACTIVE_DB_LBL, m_wndLbl4);

	DDX_Control(pDX, IDC_BUTTON1, m_wndBtnDoCopy);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);

	//}}AFX_DATA_MAP
}

// CDatabaseCopyDlg message handlers
BOOL CDatabaseCopyDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if (!m_bInitialized)
	{
		SetWindowText((getResStr(IDS_STRING40169)));

		m_wndLbl1.SetBkColor(INFOBK);
		m_wndLbl1.SetTextColor(BLUE);

		m_wndLbl1.SetWindowText((getResStr(IDS_STRING40172)));
		m_wndLbl2.SetWindowText((getResStr(IDS_STRING40170)));
		m_wndLbl3.SetWindowText((getResStr(IDS_STRING40171)));


		m_wndBtnDoCopy.SetWindowText((getResStr(IDS_STRING40173)));
		m_wndBtnDoCopy.EnableWindow(FALSE);
		m_wndCancelBtn.SetWindowText((getResStr(IDS_STRING40157)));

		// Setup ListControl; 081004 p�d
		// Get the windows handle to the header control for the
		// list control then subclass the control.
		
		HWND hWndHeaderFrom = m_wndFromLB.GetDlgItem(0)->GetSafeHwnd();
		m_HeaderFrom.SubclassWindow(hWndHeaderFrom);
		m_HeaderFrom.SetTheme(new CXTHeaderCtrlThemeOffice2003());
		
		HWND hWndHeaderTo = m_wndToLB.GetDlgItem(0)->GetSafeHwnd();
		m_HeaderTo.SubclassWindow(hWndHeaderTo);
		m_HeaderTo.SetTheme(new CXTHeaderCtrlThemeOffice2003());
		
		m_wndFromLB.InsertColumn(0,(getResStr(IDS_STRING40174)),LVCFMT_LEFT,80);
		m_wndFromLB.InsertColumn(1,(getResStr(IDS_STRING40175)),LVCFMT_LEFT,100);
		m_wndFromLB.SetExtendedStyle(LVS_EX_FULLROWSELECT);

		m_wndToLB.InsertColumn(0,(getResStr(IDS_STRING40174)),LVCFMT_LEFT,80);
		m_wndToLB.InsertColumn(1,(getResStr(IDS_STRING40175)),LVCFMT_LEFT,100);
		m_wndToLB.SetExtendedStyle(LVS_EX_FULLROWSELECT);

		m_sActiveDBMsg.Format(_T("%s\n%s\n"),
				(getResStr(IDS_STRING40176)),
				(getResStr(IDS_STRING40177)));



		m_sCopyDoneDBMsg = (getResStr(IDS_STRING40180));
		
		populateFromLB();

		GetUserDBInRegistry(m_szUserDB);

		m_sActiveDBLbl.Format(getResStr(IDS_STRING40185),m_szUserDB);
		m_wndLbl4.SetWindowText(m_sActiveDBLbl);

		m_bInitialized = TRUE;
	}

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDatabaseCopyDlg::populateFromLB(void)
{
	
	CString sLBText;
	m_wndFromLB.DeleteAllItems();
	// Is there anything to display; 081006 p�d
	if (m_vecHmsUserDBs.size() > 0)
	{
		for (UINT i = 0;i < m_vecHmsUserDBs.size();i++)
		{
			m_recHmsDBs = m_vecHmsUserDBs[i];
			InsertRow(m_wndFromLB,i,2,(m_recHmsDBs.getDBName()),(m_recHmsDBs.getNotes()));
		}	// for (int i = 0;i < m_vecHmsUserDBs.size();i++)
	}
}

void CDatabaseCopyDlg::populateToLB(void)
{
	CString sLBText;
	int nIndex = GetSelectedItem(m_wndFromLB);
	if (nIndex != LB_ERR)
	{
		m_wndToLB.DeleteAllItems();
		// Is there anything to display; 081006 p�d
		if (m_vecHmsUserDBs.size() > 0)
		{
			for (UINT i = 0;i < m_vecHmsUserDBs.size();i++)
			{
				if (i != nIndex)
				{
					m_recHmsDBs = m_vecHmsUserDBs[i];

					if (m_recHmsDBs.getDBName().CompareNoCase(m_szUserDB) != 0)
					{
						InsertRow(m_wndToLB,i,2,(m_recHmsDBs.getDBName()),(m_recHmsDBs.getNotes()));
					}
				}	// if (i != nIndex)
			}	// for (int i = 0;i < m_vecHmsUserDBs.size();i++)
		}	// if (m_vecHmsUserDBs.size() > 0)
	}	// if (nIndex != LB_ERR)
}

void CDatabaseCopyDlg::OnNMClickList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	int nIndex = GetSelectedItem(m_wndFromLB);
	
	m_sSelectedFromDB = m_wndFromLB.GetItemText(nIndex,0);

	m_wndLbl2.SetWindowText((getResStr(IDS_STRING40170)+m_sSelectedFromDB));
	m_wndLbl3.SetWindowText((getResStr(IDS_STRING40171)));
	m_wndBtnDoCopy.EnableWindow(FALSE);
	populateToLB();
	*pResult = 0;
}

void CDatabaseCopyDlg::OnNMClickList2(NMHDR *pNMHDR, LRESULT *pResult)
{
	int nIndex = GetSelectedItem(m_wndToLB);

	m_sSelectedToDB = m_wndToLB.GetItemText(nIndex,0);
	m_wndLbl3.SetWindowText((getResStr(IDS_STRING40171)+m_sSelectedToDB));
	m_wndBtnDoCopy.EnableWindow(nIndex > LB_ERR);

	*pResult = 0;
}

void CDatabaseCopyDlg::OnBnClickedButton1()
{
	doCopyDB();
}

void CDatabaseCopyDlg::doCopyDB()
{
	CString sBackupFileName;
	CString sBackupFilePath;
	CString sDBName;
	CString sDBName_log;
	CString sPath_data;
	CString sPath_data_log;
	CString sSQL;

	CString sDBBackupMissingMsg;
	CString sCopyWarningDBMsg;

	HCURSOR hPrevCursor = ::GetCursor();
	HCURSOR hWaitCursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);

	sCopyWarningDBMsg.Format(getResStr(IDS_STRING40178),m_sSelectedToDB);
	sCopyWarningDBMsg.Append(_T("\n\n")+getResStr(IDS_STRING40179)+_T("\n"));

	if (::MessageBox(this->GetSafeHwnd(),
									   (sCopyWarningDBMsg),
										 (getResStr(IDS_STRING900)),
										 MB_ICONWARNING | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
			::SetCursor(hWaitCursor);

			CString csTmp1, csTmp2;
			getServerDBDataLogDir(&csTmp1, &csTmp2);	// get server data and log-directories
			sBackupFilePath.Format(_T("%s\\%s.bak"), csTmp1, m_sSelectedFromDB);

			// create the backup
			sSQL.Format(script_backupDB, m_sSelectedFromDB, m_sSelectedFromDB, sBackupFilePath, m_sSelectedFromDB, sBackupFilePath);
			if (runSQLScript(sSQL, m_sSelectedFromDB))
			{
				sSQL.Format(script_filelistOnlyDB,(sBackupFilePath));
				getDBBackupFilelistOnly(sSQL,sDBName,sDBName_log);
		
				// Setup the SQL restorestring and run script; 081003 p�d					
				// OBS! Script for restore db, can be used to copy also.
				// The diffirense is that the RESTORE TO is the DB to be copied to; 081007 p�d
				sPath_data.Format(_T("%s\\%s.mdf"), csTmp1, m_sSelectedToDB);
				sPath_data_log.Format(_T("%s\\%s.ldf"), csTmp2, m_sSelectedToDB);

				// restore the backup into the destination database
				sSQL.Format(script_restoreDB, m_sSelectedToDB, sBackupFilePath, sDBName, sPath_data, sDBName_log, sPath_data_log);
				runSQLScript(sSQL, m_sSelectedToDB);

				// remove the backup file, since we are done with it
				DeleteFile(sBackupFilePath);

				::SetCursor(hPrevCursor);

				// Tell user, the deed is done; 081003 p�d
				::MessageBox(this->GetSafeHwnd(),
					 (m_sCopyDoneDBMsg),
					 (getResStr(IDS_STRING900)),
					 MB_ICONINFORMATION | MB_OK);
			}	// if (fileExists(sBackupFilePath))
			else	// unable to create the backup
			{
				::MessageBox(this->GetSafeHwnd(),
					 getResStr(IDS_STRING40181),
					 getResStr(IDS_STRING901),
					 MB_ICONERROR | MB_OK);
			}
	}
}