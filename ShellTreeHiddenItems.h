#pragma once

#include "Resource.h"

// CShellTreeHiddenItems dialog

class CShellTreeHiddenItems : public CDialog
{
	DECLARE_DYNAMIC(CShellTreeHiddenItems)
//private:

	vecTreeList m_vecTreeList;
	vecTreeList m_vecReturnTreeList;

	CString m_sSuitePath;
	CString m_sCaption;

	CEdit *m_pTreeEdit;

protected:
	CPADExtTreeCtrl m_wndTreeCtrl;

//	CMyExtStatic m_wndLbl1;

	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;
	CButton m_wndBtnReset;

	CImageList m_ilTreeIcons;

	void setupData(void);
	void setupTreeData(vecTreeList &list);
public:
	CShellTreeHiddenItems(vecTreeList &list,LPCTSTR suite_path,LPCTSTR caption,CWnd* pParent = NULL);   // standard constructor
	virtual ~CShellTreeHiddenItems();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

	void getReturnTreeListVec(vecTreeList &);

	UINT getSelectedPopupMenuID(void)
	{
		return m_wndTreeCtrl.getSelectedMenuID();
	}

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShellTreeHiddenItems)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	
	//{{AFX_MSG(CShellTreeHiddenItems)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedBtnResetNavtree();
};
