//  InputDlg.cpp : implementation file


#include "stdafx.h"
#include "InputDlg.h"


// CInputDlg dialog

IMPLEMENT_DYNAMIC(CInputDlg, CDialog)


BEGIN_MESSAGE_MAP(CInputDlg, CDialog)
	ON_WM_SHOWWINDOW()
	ON_WM_CREATE()
	ON_BN_CLICKED(IDOK, &CInputDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CInputDlg::CInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputDlg::IDD, pParent)
{
	m_bInitialized = FALSE;
	m_sEditText = _T("");

}

CInputDlg::~CInputDlg()
{
}

void CInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLanguageDlg)
	DDX_Control(pDX, IDC_EDIT1, m_wndEdit);
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP
}

// CLanguageDlg message handlers
BOOL CInputDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if (! m_bInitialized )
	{
		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
		m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
	
		m_bInitialized = TRUE;
	}

	setupLanguage();

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CInputDlg::OnShowWindow(BOOL bShow,UINT nStatus)
{
	m_wndEdit.SetWindowText(m_sEditText);
	DWORD dwSel = m_wndEdit.GetSel();
	m_wndEdit.SetSel(HIWORD(dwSel),-1);
	m_wndEdit.SetFocus();

	CDialog::OnShowWindow(bShow,nStatus);
}

// CInputDlg message handlers

// PROTECTED METHODS
void CInputDlg::setupLanguage(void)
{
	XMLHandler *xml = new XMLHandler();
	if (xml->load(m_sLangFN))
	{
		SetWindowText(getResStr(IDS_STRING155));
		m_wndBtnOK.SetWindowText(getResStr(IDS_STRING902));
		m_wndBtnCancel.SetWindowText(getResStr(IDS_STRING903));
	}	// if (xml->Start(m_arrLangFiles[j]))

	delete xml;
}

void CInputDlg::OnBnClickedOk()
{
	m_wndEdit.GetWindowText(m_sEditText);

	OnOK();
}
