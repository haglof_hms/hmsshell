#if !defined(AFX_DBHANDLER_H)
#define AFX_DBHANDLER_H

#include "StdAfx.h"

#include "DBBaseClass_SQLApi.h"	// ... in DBTransaction_lib
#include "DBBaseClass_ADODirect.h"	// ... in DBTransaction_lib


// SQL script to create a backup of a database; 081001 p�d
const LPCTSTR script_backupDB = _T("USE master;ALTER DATABASE \"%s\" SET RECOVERY FULL;BACKUP DATABASE \"%s\" TO DISK='%s' WITH FORMAT; BACKUP LOG \"%s\" TO DISK='%s';");
/*
RESTORE DATABASE hms3
   FROM DISK = 'C:\Program Files\HmsFullRM.bak'
   WITH 
      MOVE 'hms' TO 'c:\Program files\data.bak' ,
      MOVE 'hms_log'  TO 'c:\Program files\data_log.log', REPLACE
*/
/*
RESTORE LOG <database_name> FROM <backup_device> WITH NORECOVERY; 
RESTORE DATABASE <database_name> WITH RECOVERY;
GO
*/

//const LPCTSTR script_restoreLog = _T("USE master; RESTORE LOG \"%s\" FROM DISK='%s' WITH NORECOVERY;");
const LPCTSTR script_restoreDB = _T("USE master; RESTORE DATABASE \"%s\" FROM DISK='%s' WITH MOVE '%s' TO '%s' , MOVE '%s' TO '%s', REPLACE, RECOVERY;");

const LPCTSTR script_filelistOnlyDB = _T("RESTORE FILELISTONLY FROM DISK='%s';");

const LPCTSTR script_selectBackupDevices = _T("select * from sys.backup_devices;");

const LPCTSTR scrtpt_createBackupDevice = _T("use master;EXEC sp_addumpdevice 'disk', 'hms_backup_device%d', '%s';");

const LPCTSTR script_getBackupDir = _T("declare @rc int,@dir nvarchar(4000);exec @rc = master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\\Microsoft\\MSSQLServer\\MSSQLServer',N'BackupDirectory',@dir output, 'no_output';select BackupDir = @dir;");

const LPCTSTR script_getServerDataDir = _T("declare @rc int,@dir nvarchar(4000); exec @rc = master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\\Microsoft\\MSSQLServer\\MSSQLServer',N'DefaultData', @dir output, 'no_output'; if (@dir is null) begin exec @rc = master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\\Microsoft\\MSSQLServer\\Setup',N'SQLDataRoot', @dir output, 'no_output'; select @dir = @dir + N'\\Data'; end; select DataDir = @dir;");
const LPCTSTR script_getServerLogDir = _T("declare @rc int,@dir nvarchar(4000);exec @rc = master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\\Microsoft\\MSSQLServer\\MSSQLServer',N'DefaultLog',@dir output, 'no_output';if (@dir is null) begin exec @rc = master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\\Microsoft\\MSSQLServer\\Setup',N'SQLDataRoot',@dir output, 'no_output';select @dir = @dir + N'\\Data';end;select LogDir = @dir;");

//////////////////////////////////////////////////////////////////////////////
// Class holding data for backup_devices in SQL Server; 081022 p�d
class CBackupDecvices
{
	//private
	CString m_sDeviceName;
	CString m_sPhysicalName;
public:
	CBackupDecvices(void)
	{
		m_sDeviceName = _T("");
		m_sPhysicalName = _T("");
	}
	CBackupDecvices(LPCTSTR name,LPCTSTR phys_name)
	{
		m_sDeviceName = (name);
		m_sPhysicalName = (phys_name);
	}
	CBackupDecvices(const CBackupDecvices &c)
	{
		*this = c;
	}

	CString getDeviceName(void)		{ return m_sDeviceName; }
	CString getPhysicalName(void)	{ return m_sPhysicalName; }
};

typedef std::vector<CBackupDecvices> vecBackupDecvices;

//////////////////////////////////////////////////////////////////////////////
//	CHmsUserDBRec; transaction class for data in hms_user_types table; 060216 p�d

class CHmsUserDBRec
{
//private:
	CString m_sDBName;								// Name of database in server
	CString m_sUserName;							// User name of datbase (Company or whatever)
	CString m_sNotes;									// Notes 
	CString m_sCreatedBy;							// Name of creator
	CString m_sCreated;								// Date/Time; created/updated
public:
	CHmsUserDBRec(void);
	CHmsUserDBRec(LPCTSTR db_name,
						    LPCTSTR user_name,
							  LPCTSTR notes,
						    LPCTSTR created_by = _T(""),
							  LPCTSTR created = _T(""));

	CHmsUserDBRec(const CHmsUserDBRec &c)
	{
		*this = c;
	}
	// Public methods

	CString getDBName(void)				{ return m_sDBName;	}
	CString getUserName(void)			{ return m_sUserName; }
	CString getNotes(void)				{ return m_sNotes;	}
	CString getCreatedBy(void)		{ return m_sCreatedBy;	}
	CString getCreated(void)			{ return m_sCreated; }
};

typedef std::vector<CHmsUserDBRec> vecHmsUserDBs;



//////////////////////////////////////////////////////////////////////////////////
// CDBAdmin; Handle administrator database server transactions; 060228 p�d

class CDBAdmin : public CDBBaseClass_SQLApi
{
protected:
	// Data members

	// Methods
	BOOL userdb_Exists(CHmsUserDBRec &rec);

	// This method's used (for now) in SQL Server
	BOOL user_Exists(CHmsUserDBRec &rec,LPCTSTR user_name);

public:
	CDBAdmin(void);
	CDBAdmin(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw);
	CDBAdmin(DB_CONNECTION_DATA &db_connection);


	// Handle user db table; 060301 p�d
	BOOL userdb_New(CHmsUserDBRec &rec);						// Adds or updates a userdb in db
	BOOL userdb_Remove(CHmsUserDBRec &rec);						// Removes a userdb from db
	BOOL userdb_Get(vecHmsUserDBs &vec);							// Get all items in userdb table

	// Create user database method; 060302 p�d
	BOOL create_UserDatabase(CHmsUserDBRec &rec);
	BOOL delete_UserDatabase(CHmsUserDBRec &rec);

	BOOL getDBBckDir();
	
};

BOOL runSQLScript(CString script,CString dbname = _T(""));

BOOL restoreDB(CString script,CString dbname = _T(""));

BOOL getDBBackupFilelistOnly(CString script,CString &db_name,CString &db_log_name);

CString getServerDBBackupDir();
BOOL getServerDBDataLogDir(CString*, CString*);

#endif