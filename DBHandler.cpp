#include "StdAfx.h"
#include "DBHandler.h"


//  ////////////////////////////////////////////////////////////////////////////
//CHmsUserDBRec; transaction class for data in specie table; 060210 p�d

CHmsUserDBRec::CHmsUserDBRec(void)
{
	m_sDBName			= _T("");	// Username of database (company)
	m_sUserName			= _T("");	// Location
	m_sNotes			= _T("");	// Notes 
	m_sCreatedBy		= _T("");	// Name of creator
	m_sCreated			= _T("");	// Date/Time; created/updated
}

CHmsUserDBRec::CHmsUserDBRec(LPCTSTR db_name,
														 LPCTSTR user_name,
														 LPCTSTR notes,
														 LPCTSTR created_by,
														 LPCTSTR created)

{
	m_sDBName			= (db_name);				// Name of database on server
	m_sUserName			= (user_name);				// Username of datbase (Company or whatever)
	m_sNotes			= (notes);					// Notes 
	m_sCreatedBy		= (created_by);				// Name of creator
	m_sCreated			= (created);				// Date/Time; created/updated
}

//////////////////////////////////////////////////////////////////////////////////
// CUMShellDB; Handle ALL transactions for Forrest suite specifics; 061116 p�d
//////////////////////////////////////////////////////////////////////////////////
// CDBAdmin; Handle MySQL database server transactions for Administrator db; 060228 p�d

CDBAdmin::CDBAdmin(void) 
	: CDBBaseClass_SQLApi()
{
}

CDBAdmin::CDBAdmin(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CDBAdmin::CDBAdmin(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,2)
{
}

// PROTECTED
BOOL CDBAdmin::userdb_Exists(CHmsUserDBRec &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where db_name='%s'"),
						HMS_ADMINISTRATOR_TABLE,
						rec.getDBName());

	return CDBBaseClass_SQLApi::exists(sSQL);
}


// PUBLIC
BOOL CDBAdmin::userdb_New(CHmsUserDBRec &rec)
{
	CString sSQL;
	try
	{
		// Check if post already exists; 060210 p�d
		if (!userdb_Exists(rec))
		{
			sSQL.Format(_T("insert into %s (db_name,user_name,notes,created_by) values(:1,:2,:3,:4)"),
										HMS_ADMINISTRATOR_TABLE);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getDBName();
			m_saCommand.Param(2).setAsString()	= rec.getUserName();
			m_saCommand.Param(3).setAsString()	= rec.getNotes();
			m_saCommand.Param(4).setAsString()	= getUserName();
			m_saCommand.Execute();
		} // if (!exists(rec.getSpcID())
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Get all items in userprofile table
BOOL CDBAdmin::userdb_Get(vecHmsUserDBs &vec)
{
	SAString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"), HMS_ADMINISTRATOR_TABLE);
		m_saCommand.setCommandText(sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CHmsUserDBRec(m_saCommand.Field(1).asString(),
										m_saCommand.Field(2).asString(),
										m_saCommand.Field(3).asString(),
										m_saCommand.Field(4).asString()));

		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}


	return TRUE;
}


BOOL restoreDB(CString script,CString dbname)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString sScript(script);

	try
	{

		if (!sScript.IsEmpty())
		{
			GetAuthentication(&nAuthentication);


			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (!dbname.IsEmpty())
					_tcscpy_s(sDBName,127,dbname);
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							bReturn = pDB->commandExecute(sScript);
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

//----------------------------------------------------------------------------------------------------
// Create database table from string; 080627 p�d
BOOL runSQLScript(CString script,CString dbname)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString sScript(script);

	try
	{

		if (!sScript.IsEmpty())
		{
			GetAuthentication(&nAuthentication);


			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (!dbname.IsEmpty())
					_tcscpy_s(sDBName,127,dbname);
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								pDB->setDefaultDatabase(sDBName);
								if(pDB->commandExecute(sScript))
									bReturn = TRUE;
								else
									bReturn = FALSE;
							}	// if (pDB->existsDatabase(sDBName))
							else
							{
								bReturn = FALSE;
							}
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}


BOOL getDBBackupFilelistOnly(CString script,CString &db_name,CString &db_log_name)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;
	long lNumOfRec = 0;

	CString sScript(script);

	try
	{
		if (!sScript.IsEmpty())
		{
			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							if (pDB->recordsetQuery(sScript))
							{
								lNumOfRec = pDB->getNumOf();
								if (lNumOfRec == 2)
								{
									pDB->firstRec();
									db_name = pDB->getStr(_T("LogicalName"));
									pDB->nextRec();
									db_log_name = pDB->getStr(_T("LogicalName"));
								}	// if (lNumOfRec == 2)

								pDB->recordsetClose();
							}
							bReturn = TRUE;

						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

CString getServerDBBackupDir()
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	CString csBackupDir;
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;
	long lNumOfRec = 0;

	CString sScript(script_getBackupDir);

	try
	{
		if (!sScript.IsEmpty())
		{
			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
						_tcscmp(sDBName,_T("")) != 0 &&
						_tcscmp(sUserName,_T("")) != 0 &&
						_tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							if (pDB->recordsetQuery(sScript))
							{
								lNumOfRec = pDB->getNumOf();
								if (lNumOfRec > 0)
								{
									pDB->firstRec();
									for (short i = 0;i < lNumOfRec;i++)
									{
										csBackupDir = pDB->getStr(_T("BackupDir"));
										pDB->nextRec();
									}
								}
								pDB->recordsetClose();
							}
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		csBackupDir = _T("");
	}

	return csBackupDir;
}


BOOL getServerDBDataLogDir(CString *pcsDataDir, CString *pcsLogDir)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;
	long lNumOfRec = 0;

	CString sScriptDataDir(script_getServerDataDir);
	CString sScriptLogDir(script_getServerLogDir);

	try
	{
		GetAuthentication(&nAuthentication);

		if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		{
			if (nAuthentication == 0)	// Windows login
			{
				bIsOK = (_tcscmp(sLocation,_T("")) != 0);
			}
			else if (nAuthentication == 1)	// Server login
			{
				bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
					_tcscmp(sDBName,_T("")) != 0 &&
					_tcscmp(sUserName,_T("")) != 0 &&
					_tcscmp(sPSW,_T("")) != 0);
			}
			else
				bIsOK = FALSE;

			if (bIsOK)
			{
				CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
				if (pDB)
				{
					if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
					{
						if (pDB->recordsetQuery(sScriptDataDir))
						{
							lNumOfRec = pDB->getNumOf();
							if (lNumOfRec > 0)
							{
								pDB->firstRec();
								for (short i = 0;i < lNumOfRec;i++)
								{
									*pcsDataDir = pDB->getStr(_T("DataDir"));
									pDB->nextRec();
								}
							}
							pDB->recordsetClose();
						}

						if (pDB->recordsetQuery(sScriptLogDir))
						{
							lNumOfRec = pDB->getNumOf();
							if (lNumOfRec > 0)
							{
								pDB->firstRec();
								for (short i = 0;i < lNumOfRec;i++)
								{
									*pcsLogDir = pDB->getStr(_T("LogDir"));
									pDB->nextRec();
								}
							}
							pDB->recordsetClose();
						}

						bReturn = TRUE;
					}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
					delete pDB;
				}	// if (pDB)
			}	// if (_tcscmp(sLocation,"") != 0 &&
		}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}

	return bReturn;
}
