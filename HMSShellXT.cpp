//HMSShellXT.cpp : Defines the class behaviors for the application.
//


#include "stdafx.h"
#include <locale.h>
#include <iostream>
#include "winnls.h"
#include "HMSShellXT.h"
#include "MainFrm.h"
#include "PPMessageBox.h"
#include "Shlwapi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Required for the path functions
#if ( _MFC_VER < 0x0700 && !defined _INC_SHLWAPI )
	#include < Shlwapi.h >
	#pragma comment( lib, "Shlwapi.lib" )
#endif
#include <Strsafe.h>


// CHMSShellXTApp

BEGIN_MESSAGE_MAP(CHMSShellXTApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)

	ON_COMMAND(CMDID_CHANGE_LANGUAGE, OnChangeLanguageCmd)

END_MESSAGE_MAP()


// CHMSShellXTApp construction

CHMSShellXTApp::CHMSShellXTApp():
m_bLogging(FALSE),
m_hMDIMenu(NULL),
m_hMDIAccel(NULL),
m_pSplashDlg(NULL)
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CHMSShellXTApp object

CHMSShellXTApp theApp;

// CHMSShellXTApp initialization

BOOL CHMSShellXTApp::InitInstance()
{
	char szTempPath[MAX_PATH];
	CStringA str, path;

	CWinApp::InitInstance();
	CWinApp *pApp = AfxGetApp();

	// Check if logging should be enabled; 090323 Peter
	if( CString(m_lpCmdLine).Find(_T("-log")) >= 0 )
	{
		m_bLogging = TRUE;

		// Set up path
		GetTempPathA(sizeof(szTempPath), szTempPath);
		path.Format("%shms.log", szTempPath);

		// Redirect cout to log file
		m_logfile.open(path, ios_base::out | ios_base::app);
		if( m_logfile.is_open() )
		{
			m_logfile.rdbuf()->pubsetbuf( NULL, 0 ); // unbuffered output (i.e. immediate update)
			cout.rdbuf( m_logfile.rdbuf() );
		}

		WriteToLog("---------- Logging started ----------");
	}


	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		WriteToLog("OLE init failed!", true);
//		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	#if _MSC_VER <= 1200 // MFC 6.0 or earlier
	#ifdef _AFXDLL
		Enable3dControls();         // Call this when using MFC in a shared DLL
	#else
		Enable3dControlsStatic();   // Call this when linking to MFC statically
	#endif
	#endif // MFC 6.0 or earlier

	//--------------------------------------------------------------------------------

	// Check if there's already an instance of this program running. If so get window
	// into foreground; 060509 p�d
	CWnd *pWnd = CWnd::FindWindow(PROGRAM_NAME,NULL);
	CMainFrame* pFrame = NULL;
	if (pWnd)
	{
		pWnd->SetForegroundWindow();
		WriteToLog("Another instance of HMS is already running. Shutting down...");
	}
	else
	{
		// Create and setup Splash screen; 060804 p�d
		if (!m_pSplashDlg)
			m_pSplashDlg = new CSplashDialog;
		if (!::IsWindow(m_pSplashDlg->GetSafeHwnd()))
				m_pSplashDlg->Create(CSplashDialog::IDD);

		m_pSplashDlg->ShowWindow(SW_NORMAL);
		m_pSplashDlg->UpdateWindow();

		WriteToLog("Splash screen successfully created.");

		// Standard initialization
		// If you are not using these features and wish to reduce the size
		// of your final executable, you should remove from the following
		// the specific initialization routines you do not need
		// Change the registry key under which our settings are stored
		// TODO: You should modify this string to be something appropriate
		// such as the name of your company or organization
		SetRegistryKey((HMS_SHELL_REGISTRY_KEY));

		// Check language set. If not set, set language to ENU (American English); 060424 p�d
		// Change; check operating system language and set accordingly. If language not supported
		// set to default = ENU; 2012-11-27 #3497 P�D
		CString sSystemLang = getSystemLanguage();
		CString sLangSet = getLangSet();
		if (sLangSet.IsEmpty())
		{
			// No language set.
			// Supported languages at the moment
			if (sSystemLang.CompareNoCase(L"SVE") == 0 ||
					sSystemLang.CompareNoCase(L"ENU") == 0)
			{
					sLangSet = sSystemLang;
			}
			else
			{
				// Set default language if system language isn't supported; 2012-11-27 P�D #3497
				sLangSet = _T("ENU");
			}
			setLangSet(sLangSet);
		}
		str.Format("Using language set %S.", sLangSet);
		WriteToLog(str);

		m_bShowChangeLanguageMsg = FALSE;	// Don't show message; 071106 p�d
		// Set key in registry; 051114 p�d
		free((void*)pApp->m_pszProfileName);
		pApp->m_pszProfileName = _tcsdup((PROGRAM_NAME));
		if( !setupLanguage(sLangSet) )
		{
			WriteToLog("setupLanguage failed!", true);
		}

		// Register your unique class name that you wish to use
		WNDCLASS wndcls;
		memset(&wndcls, 0, sizeof(WNDCLASS)); // start with NULL

		wndcls.style = CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
		//you can specify your own window procedure
		wndcls.lpfnWndProc = ::DefWindowProc; 
		wndcls.hInstance = AfxGetInstanceHandle();
		wndcls.hIcon = LoadIcon(IDR_MAINFRAME); // or load a different icon
		wndcls.hCursor = LoadCursor( IDC_ARROW );
		wndcls.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
		wndcls.lpszMenuName = NULL;

		// Specify your own class name for using FindWindow later
		wndcls.lpszClassName = (PROGRAM_NAME);
		// Register the new class and exit if it fails
		if(!AfxRegisterClass(&wndcls))
		{
			WriteToLog("Failed to register main window class!", true);
			TRACE(_T("Class Registration Failed\n"));
			return FALSE;
		}

		// Try to match ShellData-files in: HMS\Setup\ShellData
		// to ShellData-files in Documents and settings\...; 090114 p�d
		WriteToLog("Loading shell data files...");
		matchShellDataFiles();

		// Get all ShellData files in ShellData subdirectory; 060807 p�d
		if( !getShellDataFilesOnDisk(m_arrShellDataFileListOnDisk) )
		{
			WriteToLog("getShellDataFilesOnDisk failed!", true);
		}

		// Get ShellData files in Shortcuts.xml; 051115 p�d
		if( !getShellDataFiles(m_arrShellDataFileList) )
		{
			WriteToLog("No ShellData files found! (first occurence)", true);
		}

		// Check (Compare) file(s) on disk to files already in Shortcuts.
		// If there's a ShellData tree-data file on disk not in Shortcuts, 
		//	add it to the Shortcuts.xml section "SuitesInShellTree"; 060807 p�d
		// Also, do a check of files in shortcuts maybe not on disk. If any found
		// remove them from Shortcuts; 060814 p�d
		checkShellDataFiles(m_arrShellDataFileListOnDisk,m_arrShellDataFileList);

		// Reload ShellData files in Shortcuts.xml; 051115 p�d
		if( !getShellDataFiles(m_arrShellDataFileList) )
		{
			WriteToLog("No ShellData files found! (second occurence)", true);
		}

		// Get shelltree data, based on files in Shortcuts.xml; 060807 p�d
		if( !getShellTreeDataItems(m_arrShellDataFileList,m_vecShellData) )
		{
			WriteToLog("getShellTreeDataItems failed!", true);
		}
		WriteToLog("Finished loading shell data files.");

		//--------------------------------------------------------------------------------

		// IndexTable, holds a unique identifer for each form in the Suites; 051212 p�d
		m_vecIndexTable.clear();
		// InfoTable, holds information about suite/user modules version number etc; 060803 p�d
		m_vecInfoTable.clear();

		//--------------------------------------------------------------------------------

		VERIFY(CXTPReportControl::UseReportCustomHeap());
		CXTPReportControl::UseRowBatchAllocation();

		HCURSOR hCursor = SetCursor(::LoadCursor(NULL,IDC_WAIT));

		// Upgrade from versions prior to HMS Release 2.1.1 requires an update of docking-pane id's
		WriteToLog("Fixing docking-pane ID's.");
		FixDockingPaneIDs();

		// To create the main window, this code creates a new frame window
		// object and then sets it as the application's main window object
		pFrame = new CMainFrame;
		if (!pFrame)
		{
			WriteToLog("Failed to create main frame!", true);
			return FALSE;
		}

		// Check if db configuration is set; 090225 Peter
		TCHAR szDBLocation[127];
		TCHAR szDBServer[127];
		TCHAR szDBUser[127];
		TCHAR szDBPsw[127];
		TCHAR szDSNName[127];
		TCHAR szDBClient[127];
		TCHAR szDBSelected[127];
		TCHAR szIsWindowsAuthentication[10];
		int nIsWindowsAuthentication;

		GetAdminIniDataSelectedDB(szDBSelected);
		GetAdminIniData(szDBServer, szDBLocation, szDBUser, szDBPsw, szDSNName, szDBClient, &nIsWindowsAuthentication);
		if( _tcscmp(szDBLocation, _T("")) == 0 )
		{
			// Check for default settings in HKLM (if both server and db is missing); 091009 Peter
			CString key;
			HKEY hKey;
			DWORD dwSize;

			key.Format(_T("%s\\%s"), REG_ROOT, REG_KEY_DATABASE);
			if( RegOpenKeyEx(HKEY_LOCAL_MACHINE, key, NULL, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS )
			{
				// DBLocation
				dwSize = sizeof(szDBLocation);
				RegQueryValueEx(hKey, REG_STR_DBLOCATION, NULL, NULL, (LPBYTE)szDBLocation, &dwSize);

				// DBSelected
				dwSize = sizeof(szDBSelected);
				RegQueryValueEx(hKey, REG_STR_SELDB, NULL, NULL, (LPBYTE)szDBSelected, &dwSize);

				// DBServer
				dwSize = sizeof(szDBServer);
				RegQueryValueEx(hKey, REG_STR_DBSERVER, NULL, NULL, (LPBYTE)szDBServer, &dwSize);
				if( _tcslen(szDBServer) == 0 ) _tcscpy_s(szDBServer, sizeof(szDBServer), _T("Microsoft SQL Server"));

				// IsWindowsAuthentication
				_tcscpy(szIsWindowsAuthentication, _T("0")); // Assign default value in case value is not set
				dwSize = sizeof(szIsWindowsAuthentication);
				RegQueryValueEx(hKey, REG_DB_AUTHENTICATION_ITEM, NULL, NULL, (LPBYTE)szIsWindowsAuthentication, &dwSize);
				nIsWindowsAuthentication = _ttoi(szIsWindowsAuthentication);

				// Password
				dwSize = sizeof(szDBPsw);
				RegQueryValueEx(hKey, REG_STR_PSW, NULL, NULL, (LPBYTE)szDBPsw, &dwSize);

				// Username
				dwSize = sizeof(szDBUser);
				RegQueryValueEx(hKey, REG_STR_USERNAME, NULL, NULL, (LPBYTE)szDBUser, &dwSize);

				RegCloseKey(hKey);
			}

			// Apply default settings if values are missing
			if( _tcslen(szDBLocation) )
			{
				WriteToLog("Copying default database settings from HKLM.");

				key.Format(_T("%s\\%s"), REG_ROOT, REG_KEY_DATABASE);
				if( RegOpenKeyEx(HKEY_CURRENT_USER, key, NULL, KEY_SET_VALUE, &hKey) == ERROR_SUCCESS )
				{
					RegSetValueEx(hKey, REG_STR_DBLOCATION, 0, REG_SZ, (BYTE*)szDBLocation, _tcslen(szDBLocation) * sizeof(TCHAR));
					RegSetValueEx(hKey, REG_STR_SELDB, 0, REG_SZ, (BYTE*)szDBSelected, _tcslen(szDBSelected) * sizeof(TCHAR));
					RegSetValueEx(hKey, REG_STR_DBSERVER, 0, REG_SZ, (BYTE*)szDBServer, _tcslen(szDBServer) * sizeof(TCHAR));
					RegSetValueEx(hKey, REG_DB_AUTHENTICATION_ITEM, 0, REG_SZ, (BYTE*)szIsWindowsAuthentication, _tcslen(szIsWindowsAuthentication) * sizeof(TCHAR));
					RegSetValueEx(hKey, REG_STR_PSW, 0, REG_SZ, (BYTE*)szDBPsw, _tcslen(szDBPsw) * sizeof(TCHAR));
					RegSetValueEx(hKey, REG_STR_USERNAME, 0, REG_SZ, (BYTE*)szDBUser, _tcslen(szDBUser) * sizeof(TCHAR));
					RegCloseKey(hKey);
				}
			}
			else
			{
				WriteToLog("Database database settings not found in HKLM.");
			}
		}


		WriteToLog("Setting up db connection...");
		if( !pFrame->setupDBConnection(*m_pSplashDlg) )
		{
			WriteToLog("Failed to set up db connection!", true);
		}

//			HCURSOR hCursor = SetCursor(::LoadCursor(NULL,IDC_WAIT));
		// Initcialize suites; must do this here; 051128 p�d
		WriteToLog("Initializing suites...");
		InitSuites();
		WriteToLog("Finished initializing suites...");
		// Init. suite(s) and Usermodule(s) config. file (xml); 060804 p�d
		InitSuiteAndUserModuleConfig();
		WriteToLog("Suite and user module configuration file created.");

		pFrame->setTreeListVec(m_vecShellData);
		pFrame->setIndexTableVec(m_vecIndexTable);
		pFrame->setInfoTableVec(m_vecInfoTable);
		pFrame->checkIfThereIsADatabaseAdmin();

		pFrame->setHInstVec(m_vecHInstances);

		m_pMainWnd = pFrame;

		// create main MDI frame window
		if (!pFrame->LoadFrame(IDR_MAINFRAME))
		{
			WriteToLog("Failed to load main frame!", true);
			return FALSE;
		}

		ReloadDefaultMenu();
		SetRTLLayout(FALSE);

		// The main window has been initialized, so show and update it
		pFrame->ShowWindowEx(m_nCmdShow);
		pFrame->UpdateWindow();

		// Remove Splash screen; 060804 p�d
		if (m_pSplashDlg)
		{
			if (::IsWindow(m_pSplashDlg->GetSafeHwnd()))
			{
				m_pSplashDlg->EndDialog(IDCANCEL);
			}
		}
		SetCursor(hCursor);
		WriteToLog("Splash screen closed.");

		// Check if db settings is ok; 091009 Peter
		if( _tcscmp(szDBServer, _T("")) == 0 ||
			_tcscmp(szDBLocation, _T("")) == 0 ||
			_tcscmp(szDBSelected, _T("")) == 0 )
		{
			WriteToLog("Database connection is not fully configured.");

			PPMSGBOXPARAMS MsgParam;
			mapLocalBtnText mapText;
			CString strYes		= getResStr(IDS_STRING194);	mapText.insert(std::make_pair(IDYES, strYes));
			CString strNo		= getResStr(IDS_STRING195);	mapText.insert(std::make_pair(IDNO, strNo));
			CString strCheckbox = getResStr(IDS_STRING193);
			CString strMsg		= getResStr(IDS_STRING191);

			MsgParam.dwStyle = 0;
			MsgParam.lpszCheckBoxText	= strCheckbox;
			MsgParam.lpszCompanyName	= _T("HaglofManagmentSystem");
			MsgParam.lpszModuleName		= _T("ShowDBConfig");
			MsgParam.pLocalBtnText		= &mapText;
			if( PPMessageBox(m_pMainWnd->m_hWnd, strMsg, _T("HMSShellXT"), MB_YESNO | MB_CHECKBOX | MB_ICONQUESTION, IDNO, &MsgParam) == IDYES )
			{
				CString langFile;
				langFile = getLanguageFN(getLanguageDir(), _T("UMDatabase"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV);
				showFormView(UMDATABASE_MODULE_ID1, langFile);
			}
		}
		else
		{
			str.Format("Using database %S on server %S (%S).", szDBSelected, szDBLocation, szDBServer);
			WriteToLog(str);
		}
	}

	//Check om inkommande kommando inneh�ller en s�kv�g till en fil som HMS ska kunna hanteras
	//och i s� fall s�nda ett meddelande till den g�llande modulen. 2012-05-07 Mathias
	//I detta fall .keg filer som modul UMKORUS ska hantera
	CString pathkegfile = HandleFileType(KEGFILE_EXT, _T("KEGFileType.Document"), _T("UMKORUS.dll"));
	if(_tcscmp(pathkegfile,_T("")) != 0)
	{	//filtypen har hittats och det finns en s�kv�g.
    STRUCT_FILETYPE structFileType;
		structFileType.Id = ID_LPARAM_COMMAND_HANDLE_FILETYPE;
		StringCbCopy( structFileType.Path, sizeof(structFileType.Path), pathkegfile );
		StringCbCopy( structFileType.Type, sizeof(structFileType.Type), KEGFILE_EXT );

		COPYDATASTRUCT cds;
		cds.cbData = sizeof(structFileType);
		cds.lpData = &structFileType;

		if(pWnd)
		{	//S�nd meddelandet till ursprungsinstancen av HMS
			pWnd->SendMessage(WM_COPYDATA, WPARAM(ID_LPARAM_COMMAND_HANDLE_FILETYPE), reinterpret_cast<LPARAM>(&cds));
		}
		else
		{
			if(pFrame)
			{ //Detta �r ursprungsintancen, i s� fall anropa metoden i main frame, ifall det inte �r NULL.
				pFrame->SendMessage(WM_COPYDATA, WPARAM(ID_LPARAM_COMMAND_HANDLE_FILETYPE), reinterpret_cast<LPARAM>(&cds));
			}
		}

	}

	return TRUE;
}

void CHMSShellXTApp::ApplyDockingPaneID(CString subkey, DWORD id)
{
	CString dpkey(_T("Software\\HaglofManagmentSystem\\HMSShell\\DockingPaneLayouts\\"));
	DWORD dwVal, dwSize;
	HKEY hKey;

	// Check specified docking-pane id
	if( RegOpenKeyEx(HKEY_CURRENT_USER, dpkey + subkey, NULL, KEY_QUERY_VALUE | KEY_SET_VALUE, &hKey) == ERROR_SUCCESS )
	{
		dwVal = 0;
		dwSize = sizeof(DWORD);
		RegQueryValueEx(hKey, _T("ID"), NULL, NULL, (LPBYTE)&dwVal, &dwSize);

		// If value is above legal range reset it
		if( dwVal > 0xFFFF )
		{
			RegSetValueEx(hKey, _T("ID"), NULL, REG_DWORD, (LPBYTE)&id, sizeof(DWORD));
		}
		RegCloseKey(hKey);
	}
}

void CHMSShellXTApp::FixDockingPaneIDs()
{
	ApplyDockingPaneID( _T("HMS_RunLayout\\Pane-1"), 0x8008 );
	ApplyDockingPaneID( _T("UMAvgAssort_Layout\\Pane-1"), 0x8111 );
	ApplyDockingPaneID( _T("UMEstimate_Layout_StandEntry\\Pane-1"), 0x1395 );
	ApplyDockingPaneID( _T("UMEstimate_Layout_StandEntry\\Pane-2"), 0x1396 );
	ApplyDockingPaneID( _T("UMLandValue_Layout_1950ForrestNormEntry\\Pane-1"), 0x9031 );
	ApplyDockingPaneID( _T("UMPricelist_Layout\\Pane-1"), 0x8106 );
	ApplyDockingPaneID( _T("UMPricelist_Layout\\Pane-2"), 0x8115 );
	ApplyDockingPaneID( _T("UMPricelist_Layout\\Pane-3"), 0x8108 );
	ApplyDockingPaneID( _T("UMStandTable_Layout\\Pane-1"), 0x8102 );
	ApplyDockingPaneID( _T("UMStandTable_Layout\\Pane-2"), 0x8101 );
}

void CHMSShellXTApp::WriteToLog(CStringA message, bool error/*=false*/)
{
	char szTime[256];
	SYSTEMTIME st;

	if( !message.IsEmpty() )
	{
		// Set up timestamp prefix
		GetLocalTime(&st);
		_snprintf(szTime, sizeof(szTime), "%d-%02d-%02d %02d:%02d:%02d:\t", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	}
	else
	{
		// Insert empty row with no timestamp if message is empty
		szTime[0] = NULL;
	}

	cout << szTime;
	if( error )
	{
		cout << "[ERROR] ";
	}
	cout << message << "\n";
}


// CHMSShellXTApp message handlers

int CHMSShellXTApp::ExitInstance() 
{
	//TODO: handle additional resources you may have added
	if (m_hMDIMenu != NULL)
		FreeResource(m_hMDIMenu);
	if (m_hMDIAccel != NULL)
		FreeResource(m_hMDIAccel);

	XTPResourceManager()->Close();

	// Free Suits (HINSTANCE)
	// If there's data, start reading ALL ShellData files into
	// m_vecShellData; 051115 p�d
	if (m_vecHInstances.size() > 0)
	{
		for (UINT i = 0;i < m_vecHInstances.size();i++)
		{
			HINSTANCE hInst = m_vecHInstances[i].getHInst();
			if (hInst != NULL)
			{
				AfxFreeLibrary(hInst);
			}	// if (hModule != NULL)
		}
	}

	// Remove Splash screen; 060804 p�d
	if (m_pSplashDlg)
	{
		if (::IsWindow(m_pSplashDlg->GetSafeHwnd()))
        m_pSplashDlg->EndDialog(IDCANCEL);
    delete m_pSplashDlg;
	}

	// Close log file; 090323
	if( m_logfile.is_open() )
	{
		WriteToLog("---------- Logging ended ----------\n\n");
		m_logfile.close();
	}

	return CWinApp::ExitInstance();
}

CString getFileNameNoExt(LPCTSTR fn)
{
	CString sFN;
	CString sFNnoExt;
	sFN = fn;
	sFNnoExt = sFN.Left(sFN.Find(_T(".")));

	return sFNnoExt;
}

void CHMSShellXTApp::InitSuites(void)
{
	CFileFind ff;
	CString sPath;
	CString sMsg;
	CStringA str;
	typedef CRuntimeClass *(*Func)(CStringArray *,vecINDEX_TABLE &,vecINFO_TABLE &);
  Func proc;
	// Get all Suites in Suites\(subdirs)
	getSuites(m_arrSuitesList);
	// Check if there's any ShellData files; 051115 p�d
	if (m_arrSuitesList.GetCount() > 0)
	{
		// If there's data, start reading ALL ShellData files into
		// m_vecShellData; 051115 p�d
		m_vecHInstances.clear();
		for (int i = 0;i < m_arrSuitesList.GetCount();i++)
		{
			sPath = m_arrSuitesList[i];
			BOOL bFound = ff.FindFile(sPath);
			if (bFound)
			{
				ff.FindNextFile();			
				GetUserModulesPerSuite(ff.GetFileName());
			}

			str.Format("Loading %S", sPath);
			WriteToLog(str);

			HINSTANCE hModule = AfxLoadLibrary(sPath);

			if (hModule != NULL)
			{
				m_vecHInstances.push_back(CModuleInstance(hModule,sPath));
				proc = (Func)GetProcAddress((HMODULE)m_vecHInstances[m_vecHInstances.size()-1].getHInst(), INIT_SUITE_FUNCTION );
				if (proc != NULL)
				{
					if (m_pSplashDlg->GetSafeHwnd())
					{
						sMsg.Format(_T("%s : %s ..."),getResStr(IDS_STRING157),ff.GetFileTitle());
						if (m_arrUserModulesPerSuite.GetCount() > 0)
						{
							sMsg += _T("\n") + (getResStr(IDS_STRING170)) + _T("\n");
							for (int i = 0;i < m_arrUserModulesPerSuite.GetCount();i++)
							{
								sMsg += getFileNameNoExt(m_arrUserModulesPerSuite.GetAt(i));
								if (i < m_arrUserModulesPerSuite.GetCount()-1)
									sMsg += " , ";						
								if ((i % 3) == 0 && (i > 0))
									sMsg += "\n";
							}
						}
						m_pSplashDlg->setSplashInfoText(sMsg);
						//AfxMessageBox(sMsg);
					}
					// call the function
					proc(&m_arrUserModulesPerSuite,m_vecIndexTable,m_vecInfoTable);
				}	// if (proc != NULL)
				else
				{
					str.Format("Failed to init suite (%S).", sPath);
					WriteToLog(str, true);
				}
			}	// if (hModule != NULL)
			else
			{
				str.Format("Failed to load %S.", sPath);
				WriteToLog(str, true);
			}
		}
		if (m_pSplashDlg->GetSafeHwnd())
		{
			m_pSplashDlg->setSplashInfoText(getResStr(IDS_STRING169));
		}
	}
}

// Create the Suite and Usermodule configuration file; 060804 p�d
void CHMSShellXTApp::InitSuiteAndUserModuleConfig(void)
{
	CString sSuiteModulePath;
	sSuiteModulePath = getSuiteModuleConfigPathAndFile();
	CSuiteModuleInfoParser *p = new CSuiteModuleInfoParser;
	if (p)
	{
		p->CreateDoc();
		p->setSuiteModuleInfo(m_vecInfoTable);
		p->SaveToFile((sSuiteModulePath));
		delete p;
	}	// if (p)
}


void CHMSShellXTApp::GetUserModulesPerSuite(CString suite)
{
	m_arrUserModulesPerSuite.RemoveAll();
	// Check if there's any ShellData files; 051115 p�d
	if (m_arrShellDataFileList.GetCount() > 0)
	{
		XMLShellData *xml = new XMLShellData();

		suite.MakeLower();

		// If there's data, start reading ALL ShellData files into
		// m_vecShellData; 051115 p�d
		for (int i = 0;i < m_arrShellDataFileList.GetCount();i++)
		{
			if (xml->load(m_arrShellDataFileList.GetAt(i)))
			{
				CString sSuite;
				sSuite.Format(_T("%s%s"),xml->str(HEADER_NAME),SUITES_FN_EXT);
				if (sSuite.MakeLower() == suite) // case-insensive matching
				{
					xml->getUserModules(m_arrUserModulesPerSuite);
				}
			}
		}
		delete xml;
	}
}

void CHMSShellXTApp::ReloadDefaultMenu()
{
	CMainFrame* pMainFrame = DYNAMIC_DOWNCAST(CMainFrame, m_pMainWnd);
	if (pMainFrame)
	{
		pMainFrame->setLanguage();
	}
}

void SetRTLLayout(CWnd* pWnd, BOOL bRTLLayout)
{
	pWnd->ModifyStyleEx(bRTLLayout? 0: WS_EX_LAYOUTRTL, !bRTLLayout? 0: WS_EX_LAYOUTRTL);

	// walk through HWNDs to avoid creating temporary CWnd objects
	// unless we need to call this function recursively
	for (CWnd* pChild = pWnd->GetWindow(GW_CHILD); pChild != NULL;
		pChild = pChild->GetWindow(GW_HWNDNEXT))
	{

		// send to child windows after parent
		SetRTLLayout(pChild, bRTLLayout);
	}
}

void CHMSShellXTApp::SetRTLLayout(BOOL bRTLLayout)
{
	if ((BYTE)::GetVersion() < 4)
		return;

	if (m_bIsRTL == bRTLLayout)
		return;

	m_bIsRTL = bRTLLayout;


	if (m_pMainWnd)
	{
		::SetRTLLayout(m_pMainWnd, bRTLLayout);

		// then update the state of all floating windows owned by the parent
		HWND hWnd = ::GetWindow(::GetDesktopWindow(), GW_CHILD);
		while (hWnd != NULL)
		{
			if (m_pMainWnd->m_hWnd == ::GetParent(hWnd))
				::SetRTLLayout(CWnd::FromHandle(hWnd), bRTLLayout);

			hWnd = ::GetWindow(hWnd, GW_HWNDNEXT);
		}
	}

	XTPImageManager()->DrawReverted(bRTLLayout);
}
// Added 2007-11-06 p�d
void CHMSShellXTApp::cmdChangeLanguage(void)
{
	CMainFrame* pMainFrame = DYNAMIC_DOWNCAST(CMainFrame, m_pMainWnd);
	BOOL bIsOk = FALSE;
	CString sLangAbrev;
	CLanguageInfo linfo;
	// Find out and setup language set; 051205 p�d
	sLangAbrev = getLangSet();
	if (getGlobLangInfo(sLangAbrev,linfo))
	{
		if (setupLanguage(sLangAbrev))
		{
			// Get all ShellData files in ShellData subdirectory; 051115 p�d
			m_arrShellDataFileList.RemoveAll();
			m_vecShellData.clear();
			getShellDataFiles(m_arrShellDataFileList);
			getShellTreeDataItems(m_arrShellDataFileList,m_vecShellData);
			//--------------------------------------------------------------------------------
			if (pMainFrame)
			{
				pMainFrame->setTreeListVec(m_vecShellData);
				pMainFrame->setLanguage();
			}	// if (pMainFrame)
		}	// if (setupLanguage(sLangAbrev))
	}	// if (bIsOk)
}


void CHMSShellXTApp::OnChangeLanguageCmd()
{
	m_bShowChangeLanguageMsg = FALSE;
	cmdChangeLanguage();
}

// Added 071026 P�D
// This method'll set language settings, LCID etc.
// depending on language abbrevation; 071026 p�d
BOOL CHMSShellXTApp::setupLanguage(CString lang_abbrev)
{

	CString sMsg;
	sMsg.Format(_T("%s\n%s\n"),
			getResStr(IDS_STRING167),
			getResStr(IDS_STRING168));

	CString sLangAbrev = getSystemLanguage();
	CString sLangName = getSystemLanguageName();
	
	CString sPath;

	sPath = getLanguageDir() + getLanguageFileName();

	// Check if the languagefile exists; 051114 p�d

	if (fileExists(sPath))
	{
		if (!XTPResourceManager()->SetResourceManager(new CXTPResourceManagerXML(sPath)))
				return FALSE;

	} // if (cdo.FileExists(sPath))
	else
	{
		CStringA str; str.Format("Missing language file '%S'.", sPath);
		WriteToLog(str, true);
	}
	m_curLanguageLCID = MAKELANGID(LANG_NEUTRAL, SUBLANG_SYS_DEFAULT);
	::SetThreadLocale(MAKELCID(m_curLanguageLCID, SORT_DEFAULT));
	::setlocale(LC_ALL, CStringA(sLangName));

	if (m_bShowChangeLanguageMsg)
	{
		::MessageBox(0,sMsg,getResStr(IDS_STRING900),MB_ICONEXCLAMATION | MB_OK);
	}
	// Reset value to don't show message; 071106 p�d
	m_bShowChangeLanguageMsg = FALSE;

	return TRUE;

}

// Added 2009-01-13 p�d
void CHMSShellXTApp::matchShellDataFiles(void)
{
	CStringArray sarrShellDataIcons;
	CStringArray sarrShellDataFiles;
	CString sFilter;
	CString sFileAndPath_from;
	CString sFileAndPath_to;
	CString sSetupShellDataDir;
	int nVersion_from;
	int nVersion_to;

	sSetupShellDataDir.Format(_T("%s\\%s"),getSetupsDir(),SETUP_SHELLDATA_DIR);
	// Get ALL icon files; 090113 p�d
	sFilter.Format(_T("*.ico"));
	getListOfFilesInDirectory(sFilter,sSetupShellDataDir,sarrShellDataIcons);
	for (int i = 0;i < sarrShellDataIcons.GetCount();i++)
	{
		sFileAndPath_from.Format(_T("%s\\%s"),sSetupShellDataDir,sarrShellDataIcons.GetAt(i));
		sFileAndPath_to.Format(_T("%s\\%s"),getShellDataDir(),sarrShellDataIcons.GetAt(i));
		if (!fileExists(sFileAndPath_to))
		{
			copyFile(sFileAndPath_from,getShellDataDir());
		}
	}

	// Check for ShellData files that isn't included in the Setup directory; 090609 Peter
	sFilter.Format(_T("*"));
	getListOfFilesInDirectory(sFilter,getShellDataDir(),sarrShellDataFiles);
	for( int i = 0; i < sarrShellDataFiles.GetCount(); i++ )
	{
		// Delete any files that isn't included in the Setup directory
		sFileAndPath_from.Format(_T("%s\\%s"),sSetupShellDataDir,sarrShellDataFiles.GetAt(i));
		if( !fileExists(sFileAndPath_from) )
		{
			sFileAndPath_to.Format(_T("%s\\%s"),getShellDataDir(),sarrShellDataFiles.GetAt(i));
			DeleteFile(sFileAndPath_to);
		}
	}

	// Get ALL ShellData xml-files; 090113 p�d
	sFilter.Format(_T("*.xml"));
	sarrShellDataFiles.RemoveAll();
	getListOfFilesInDirectory(sFilter,sSetupShellDataDir,sarrShellDataFiles);
	for (int i = 0;i < sarrShellDataFiles.GetCount();i++)
	{
		sFileAndPath_from.Format(_T("%s\\%s"),sSetupShellDataDir,sarrShellDataFiles.GetAt(i));
		sFileAndPath_to.Format(_T("%s\\%s"),getShellDataDir(),sarrShellDataFiles.GetAt(i));
		if (!fileExists(sFileAndPath_to))
		{
			copyFile(sFileAndPath_from,getShellDataDir());
		}
		else if (fileExists(sFileAndPath_from) && fileExists(sFileAndPath_to))
		{
			XMLHandler *xml_from = new XMLHandler();	// .. in Setup\ShellData
			XMLHandler *xml_to = new XMLHandler();		// .. in Documents and settings\...
			if (xml_from->load(sFileAndPath_from))
			{
				nVersion_from = xml_from->getVersionNumber();
			}	// if (xml_from->load(sFileAndPath_from))

			if (xml_to->load(sFileAndPath_to))
			{
				nVersion_to = xml_to->getVersionNumber();
			}	// if (xml_to->load(sFileAndPath_to))

			// Compare versionnumber. If version_from > version_to
			// copy file in version_from to version_to; 090114 p�d
			if (nVersion_from > nVersion_to)
				copyFile(sFileAndPath_from,getShellDataDir());

			delete xml_to;
			delete xml_from;

		}	// else if (fileExists(sFileAndPath_from) && fileExists(sFileAndPath_to))
	}	// for (int i = 0;i < sarrShellDataFiles.GetCount();i++)

	sarrShellDataIcons.RemoveAll();
	sarrShellDataFiles.RemoveAll();
}

// App command to run the dialog
void CHMSShellXTApp::OnAppAbout()
{
	CString sMsg;
	if (m_pSplashDlg->GetSafeHwnd())
	{
		sMsg.Format(_T("%s %s\n\n%s\n%s %s\n%s %s\n%s %s"),
					getResStr(IDS_STRING158),QueryOS(),
					getResStr(IDS_STRING159),
					getResStr(IDS_STRING160),QueryTotalRAM(),
					getResStr(IDS_STRING161),QueryFreeRAM(),
					getResStr(IDS_STRING162),QueryPercentUsedRAM());
		m_pSplashDlg->setSplashInfoText((sMsg));
		m_pSplashDlg->ShowWindow(SW_NORMAL);
		m_pSplashDlg->UpdateWindow();
	}
}

//Added 2012-05-09 Mathias Dickl�n
/************************************************************************/
/* Beskrivning: G�r en koll ifall en modul finns tillg�nglig f�r HMS
/* eller inte.
/*
/* Parametrar:
/* aModuleName - Namnet p� modulen som skall kollas upp tex: "UMKORUS.dll"
/*
/* Returnerar:
/* BOOL - Ifall modulen hittas och �r inladdad i HMS s� returneras TRUE
/* annars FALSE.
/************************************************************************/
BOOL CHMSShellXTApp::ModuleExists(LPCTSTR aModuleName)
{
	BOOL moduleFound = FALSE;
	CStringArray moduleArray;
	getModules(moduleArray,FALSE);
	if (moduleArray.GetCount() > 0)
	{
		for(UINT i = 0;i < moduleArray.GetCount();i++)
		{
			if(moduleArray[i].MakeUpper() == CString(aModuleName).MakeUpper())
			{
				moduleFound = TRUE;
				break;
			}
		}
	}
	return moduleFound;
}

//Added 2012-05-07 Mathias Dickl�n
/************************************************************************/
/* Beskrivning: Metoden g�r en check mot m_lpCmdLine kommandot f�r den 
/* g�llande instansen av HMS. N�r anv�ndaren dubbel-klickar p� en fil 
/* som �r registrerad till HMSShell.exe s� kommer en s�kv�g in till 
/* kommandot.
/*
/* F�rst s� kollas om den g�llande modulen f�r filtypen finns, sedan 
/* kollas det upp om den filtypen finns registrerad och om inte s� 
/* registreras den.  Sen forts�tter metoden med att kolla om s�kv�gen 
/* st�mmer med den filtyp som ska hanteras och i s� fall returneras 
/* s�kv�gen, annars NULL.
/*
/* Parametrar:
/* aFileType - Filtypen som skall hanteras tex: ".keg"
/* aDocClassName - Klassnamnet som ev. skall lagras i registret ifall 
/* filtypen �nnu inte �r registrerad, tex: "KEGFileType.Document"
/* aModuleName - Namnet p� modulen som ska hantera filtypen, 
/* tex: "UMKORUS.dll".
/*
/* Returnerar:
/* LPCTSTR - Returnerar en s�kv�g till filen ifall det har kommit in till
/* m_lpCmdLine dvs. ifall anv�ndaren har dubbelklickat p� en fil och att
/* den g�llande modulen f�r filtypen finns tillg�nglig f�r HMS. 
/************************************************************************/
CString CHMSShellXTApp::HandleFileType(LPCTSTR  aFileType, LPCTSTR  aDocClassName, LPCTSTR aModuleName)
{
	CString rPath = _T("");;
	try
	{
		//F�rst kolla om modulen f�r den filtypen som ska hanteras finns
    if(ModuleExists(aModuleName))
		{
			//Modulen hittades, kolla i s� fall om typen redan �r associerad till HMSShell.exe
			DWORD dwSize = MAX_PATH;
			TCHAR tchApplicationPath[ MAX_PATH ] = { 0 };
			HRESULT hr = AssocQueryString( 0,
				ASSOCSTR_EXECUTABLE,
				CString(aFileType),
				_T( "open" ),
				tchApplicationPath,
				&dwSize );

			if( FAILED( hr ))
			{ //Ingen association f�r den g�llande filtypen hittades is�fall registrera f�rst 
				//filtypen i registret f�r HMSShell.exe
				CGCFileTypeAccess TheFTA;

				TCHAR	szProgPath[MAX_PATH * 2];
				::GetModuleFileName(NULL, szProgPath, sizeof(szProgPath)/sizeof(TCHAR));

				CString csTempText;

				CString fileType = CString(aFileType);
				fileType.Remove(_T('.'));
                TheFTA.SetExtension(fileType);

				csTempText  = szProgPath;
				csTempText += " \"%1\"";
				TheFTA.SetShellOpenCommand(csTempText);
				TheFTA.SetDocumentShellOpenCommand(csTempText);
				TheFTA.SetDocumentClassName(CString(aDocClassName));
				
				// s�tt iconen till filtypen, h�r s�ts HMS iconen som anv�nds av HMSShell.exe
				csTempText  = szProgPath;
				csTempText += ",0";
				TheFTA.SetDocumentDefaultIcon(csTempText);

				TheFTA.RegSetAllInfo();		
			}
		}

		//Checka om det har kommit in en s�kv�g f�r kommandot
		if( m_lpCmdLine && m_lpCmdLine[0] )
		{
			CString path=CString(m_lpCmdLine);
			path.Remove(_T('\"')); //ta bort situationstecken

			CString fileType = CString(aFileType);
			if((path.Right(fileType.GetLength())).MakeLower() == fileType.MakeLower())
			{	//F�rst kolla s� att filtypen p� den inkommande s�kv�gen st�mmer och i s� fall returnera s�kv�gen
				rPath=path;
			}
	  }	
	}
	catch (CException* e)
	{ 
	}	

	return rPath;
}