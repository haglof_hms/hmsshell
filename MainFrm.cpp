// MainFrm.cpp : implementation of the CMainFrame class
//

#include <time.h>
#include "stdafx.h"
#include "HMSShellXT.h"
#include "MainFrm.h"
#include "RSSXMLFileParser.h"

#include "ShellTreeHiddenItems.h"
#include "ResLangFileReader.h"
#include "DatabaseBackupDlg.h"
#include "DatabaseCopyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#pragma managed(push, off)

//////////////////////////////////////////////////////////////////////////////////
// CMainFrame


IMPLEMENT_DYNAMIC(CMainFrame, CXTPMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CXTPMDIFrameWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_HELPINFO()
	ON_WM_COPYDATA()
	ON_WM_SHOWWINDOW()
	ON_WM_TIMER()

	ON_MESSAGE(XTPWM_DOCKINGPANE_NOTIFY, OnDockingPaneNotify)
	ON_COMMAND(ID_VIEW_NAVIGATIONBAR, OnShowPane)

	ON_UPDATE_COMMAND_UI(ID_VIEW_NAVIGATIONBAR, OnUpdateMenuView)

	ON_COMMAND(XTP_ID_CUSTOMIZE, OnCustomize)
	// My commands
	ON_COMMAND(ID_CHANGE_LANGUAGE, OnChangeLanguage)
	ON_COMMAND(ID_ADMINISTRATION, OnAdministration)
	ON_COMMAND(ID_BACKUP_DATABASE, OnBackup)
	ON_COMMAND(ID_RESTORE_DATABASE, OnRestoreBackup)
	ON_COMMAND(ID_COPY_DATABASE, OnCopyBackup)
	ON_COMMAND(ID_RESTORE_LAYOUT, OnRestoreLayout)

	ON_COMMAND(ID_NEW_ITEM, OnNewItemBtnClick)
	ON_COMMAND(ID_OPEN_ITEM, OnOpenItemBtnClick)
	ON_COMMAND(ID_SAVE_ITEM, OnSaveItemBtnClick)
	ON_COMMAND(ID_DELETE_ITEM, OnDeleteItemBtnClick)
	ON_COMMAND(ID_PREVIEW_ITEM, OnPreviewItemBtnClick)

	ON_UPDATE_COMMAND_UI(ID_NEW_ITEM, OnUpdateNewItemBtn)
	ON_UPDATE_COMMAND_UI(ID_OPEN_ITEM, OnUpdateOpenItemBtn)
	ON_UPDATE_COMMAND_UI(ID_SAVE_ITEM, OnUpdateSaveItemBtn)
	ON_UPDATE_COMMAND_UI(ID_DELETE_ITEM, OnUpdateDeleteItemBtn)
	ON_UPDATE_COMMAND_UI(ID_PREVIEW_ITEM, OnUpdatePreviewItemBtn)

	ON_UPDATE_COMMAND_UI(ID_BACKUP_DATABASE, OnUpdateBackupBtn)
	ON_UPDATE_COMMAND_UI(ID_RESTORE_DATABASE, OnUpdateRestoreDatabaseBtn)
	ON_UPDATE_COMMAND_UI(ID_COPY_DATABASE, OnUpdateCopyDatabaseBtn)

	// Handles messages sent from one suite/user module to be directed
	// to another suite/user module; 060202 p�d
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromSuite)
	// Handles message from Shell treelist; 050422 p�d
	ON_MESSAGE(WM_USER_MSG_TREELIST, OnTreeListMessage)

	ON_COMMAND(ID_DBNAVIG_START, OnDBNavigStartBtnClick)
	ON_COMMAND(ID_DBNAVIG_PREV, OnDBNavigPrevBtnClick)
	ON_COMMAND(ID_DBNAVIG_NEXT, OnDBNavigNextBtnClick)
	ON_COMMAND(ID_DBNAVIG_END, OnDBNavigEndBtnClick)
	ON_COMMAND(ID_DBNAVIG_LIST, OnDBNavigListBtnClick)

	//ON_COMMAND(ID_MENUITEM_HELP, OnHelp) //Tagit bort, finns redan i enskilda moduler ist�llet #4493 20151013
	//ON_COMMAND(ID_MENUITEM_NEWS, OnNews) //#4574
	ON_COMMAND(ID_MENUITEM_WEBSITE, OnWebSite)

	ON_UPDATE_COMMAND_UI(ID_ADMINISTRATION, OnUpdateMenuToolsDatabase)
	//ON_UPDATE_COMMAND_UI(ID_MENUITEM_NEWS, OnUpdateMenuNews) //#4574

	ON_UPDATE_COMMAND_UI(ID_DBNAVIG_START, OnUpdateDBToolbarBtnStart)
	ON_UPDATE_COMMAND_UI(ID_DBNAVIG_PREV, OnUpdateDBToolbarBtnPrev)
	ON_UPDATE_COMMAND_UI(ID_DBNAVIG_NEXT, OnUpdateDBToolbarBtnNext)
	ON_UPDATE_COMMAND_UI(ID_DBNAVIG_END, OnUpdateDBToolbarBtnEnd)
	ON_UPDATE_COMMAND_UI(ID_DBNAVIG_LIST, OnUpdateDBToolbarBtnList)

	ON_BN_CLICKED(CMDID_CHANGE_SERVER, OnChangeServerCmd)

	ON_COMMAND(ID_TOGGLE_ONLINE_OFFLINE, OnOnlineOfflineToggleBtn)
	ON_MESSAGE(XTPWM_POPUPCONTROL_NOTIFY, OnPopUpNotify)

END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_SEPARATOR,           // status line indicator
	ID_SEPARATOR,           // status line indicator
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_pDockingFrame = 0;

	bFirstOpened = TRUE;
	m_bConnected = FALSE;
	m_bAdminConnected = FALSE;
	m_bIsFocusOnNavTree = TRUE;

	m_bTBMenuNews = (getConnectToWEB() == 1);
	m_nTheme = xtpPopupThemeOffice2003;	// Set this as defaule theme for popup news control; 061030 p�d
	m_nAnimation = 2;
	m_uAnimDelay = 500;
	m_uShowDelay = 3000;
	m_bAllowMove = FALSE;
	m_pActivePopup = NULL;
	m_sizeNewsPopup = CSize(350,100); // Size of New bulletine popup; 061030 p�d
	m_bShowNews = FALSE;

	// Data member, holding selected item in NavigationTree
	// in a CTreeList class; 070410 p�d
	m_pActiveTreeListItem = NULL;

	m_enumDATABASE_CONNECTION_STATUS = DCS_NOTHING;	// No connection yet; 071221 p�d
	m_enumCONN_STATUS = CONN_NOTHING;	// Not deteremin connection yet; 071221 p�d
}

CMainFrame::~CMainFrame()
{
	while (!m_lstPopupControl.IsEmpty()) 
	{
		delete m_lstPopupControl.RemoveTail();
	}
}

// Open Help file for HMSShell; 060804 p�d
/*BOOL CMainFrame::OnHelpInfo(HELPINFO* lpHelpInfo)
{
	CString sLangSet;
 	CString sPath;
	sLangSet = getLangSet();
	sPath.Format(_T("%s\\%s%s%s"),getHelpDir(),_T("HMS"),sLangSet,HELPFILE_EXT);
	::HtmlHelp(this->m_hWnd,sPath,HH_DISPLAY_TOPIC,NULL);

	return TRUE;
}*/

void CMainFrame::OnClose()
{
	// Stop the timer control
	KillTimer(ID_TIMER);
	KillTimer(ID_TIMER2);

	// Save the current state for command bar(s)
	SaveCommandBars((COMMANDBAR_SETUP));

	// Save state of docking pane(s)
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	m_paneManager.GetLayout(&layoutNormal);
	layoutNormal.Save((RUNLAYOUT_SETUP));

	if (m_pDockingFrame)
	{
		CXTPShortcutBarItem *pItem = m_pDockingFrame->m_wndNavigationBar.GetSelectedItem();
		if (pItem)
		{
			// Set currently selected Suite
			regSetStr((REG_ROOT_HMSSHELL),(REG_PANESELECTED_KEY),(REG_PANESELECTED),(LPCTSTR)pItem->GetCaption());
			regSetInt((REG_ROOT_HMSSHELL),(REG_PANESELECTED_KEY),(REG_PANESELECTED_ID),(DWORD)pItem->GetID()-1);	// Zero based; 060509 p�d
		}
	}

	// Save frame window size and position.
	m_wndPosition.SaveWindowPos(this);

	// Check if we are connected to database, if so
	// disconnet us from it; 070119 p�d
	if (m_saConnection.isConnected())
		m_saConnection.Disconnect();

	// Check if we are connected to database, if so
	// disconnet us from it; 070119 p�d
	if (m_saAdminConnection.isConnected())
		m_saAdminConnection.Disconnect();

	if (m_MTIClientWnd.IsAttached())
		m_MTIClientWnd.Detach();

	CMDIFrameWnd::OnClose();
}

LRESULT CMainFrame::OnMDIDestroy(WPARAM wParam, LPARAM lParam)
{
	return 0l;
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	CString sCaption,sReleaseNum(getRegisterRelease()),sRelease;
	sCaption.Format(_T("%s"),getResStr(IDS_STRING1));
	SetWindowText(sCaption);

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	m_wndStatusBar.UseCommandBarsTheme(TRUE);

	m_wndStatusBar.GetPane(0)->SetVisible(FALSE);
	m_wndStatusBar.SetPaneWidth(0, 0);
	m_wndStatusBar.SetPaneStyle(0, SBPS_NOBORDERS);
	m_wndStatusBar.SetPaneStyle(1, SBPS_STRETCH | SBPS_NORMAL);
	m_wndStatusBar.SetPaneStyle(2, SBPS_STRETCH | SBPS_NORMAL);
	if (!sReleaseNum.IsEmpty())
	{
		m_wndStatusBar.SetPaneWidth(3, 100);
		m_wndStatusBar.SetPaneStyle(3, SBPS_NORMAL);
		sRelease.Format(_T("%s: %s"),getResStr(IDS_STRING4),getRegisterRelease());
		m_wndStatusBar.SetPaneText(3,sRelease);
	}

	if (!InitCommandBars())
		return -1;

	// Get window settings from registry; 060113 p�d

	// Get last selected pane from registry; 060509 p�d
	m_sLastSelectedPane = regGetStr((REG_ROOT_HMSSHELL),(REG_PANESELECTED_KEY),(REG_PANESELECTED));
	m_nLastSelectedPaneID = regGetInt((REG_ROOT_HMSSHELL),(REG_PANESELECTED_KEY),(REG_PANESELECTED_ID));	// Zero based; 060509 p�d

	CXTPPaintManager::SetTheme(xtpThemeOffice2003);

	pCommandBars = GetCommandBars();

	pMenuBar = pCommandBars->SetMenu(_T("Menu Bar"), IDR_MAINFRAME);
	pMenuBar->SetFlags(xtpFlagHideMinimizeBox);

	CXTPToolBar* pCommandBar = (CXTPToolBar*)pCommandBars->Add(_T(""), xtpBarTop);
	if (!pCommandBar ||
		!pCommandBar->LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;
	}

	CXTPToolBar* pDBNavigBar = (CXTPToolBar*)pCommandBars->Add(_T(""), xtpBarTop);
	if (!pCommandBar ||
		!pDBNavigBar->LoadToolBar(IDR_TOOLBAR_DBNAVIG))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;
	}
	DockRightOf(pDBNavigBar, pCommandBar);

	// Load the previous state for toolbars and menus.
	LoadCommandBars((COMMANDBAR_SETUP));

	// Initialize the docking pane manager and set the
	// initial them for the docking panes.  Do this only after all
	// control bars objects have been created and docked.
	GetDockingPaneManager()->InstallDockingPanes(this);
	SetDockingPaneTheme(xtpPaneThemeOffice2003);
	GetDockingPaneManager()->SetFloatingFrameCaption(_T("Panes"));
	m_paneManager.SetThemedFloatingFrames(TRUE);

	// Set correct pane ID #4623
	regSetInt(REG_ROOT,CString(_T("HMSShell\\DockingPaneLayouts\\HMS_RunLayout\\Pane-1")),_T("ID"),ID_PANE_NAVIGATION);

	// Create docking pane for NavigationBar
	m_wndNavBarPane = GetDockingPaneManager()->CreatePane(
		ID_PANE_NAVIGATION, CRect(0, 0, 200, 120), xtpPaneDockLeft);

	// Load the previous state for docking panes.
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	if (layoutNormal.Load((RUNLAYOUT_SETUP)))
	{
		m_paneManager.SetLayout(&layoutNormal);
	}

	if (m_wndLangDlg.GetSafeHwnd() == NULL)
	{
		if (!m_wndLangDlg.Create(CLanguageDlg::IDD,this))
		{
			return 0;
		}
	}

	m_bIsUMDatabase = TRUE;
	bIsFirstTime = TRUE;

	// Variables, handles Enable/Disable button on the DBNavigation toolbar; 060112 p�d
	m_bTBNewBtnStatus							= FALSE;
	m_bTBOpenBtnStatus						= FALSE;
	m_bTBSaveBtnStatus						= FALSE;
	m_bTBDeleteBtnStatus					= FALSE;
	m_bTBPreviewBtnStatus					= FALSE;
	
	m_bDBNavigationBtnStart				= FALSE;
	m_bDBNavigationBtnPrev				= FALSE;
	m_bDBNavigationBtnNext				= FALSE;
	m_bDBNavigationBtnEnd					= FALSE;
	m_bDBNavigationBtnList				= FALSE;

	m_bTBBackupDatabaseStatus			= getRegisterDBBackupAllowed();
	m_bTBRestoreDatabaseStatus		= getRegisterDBRestoreAllowed();
	m_bTBCopyDatabaseStatus				= getRegisterDBCopyAllowed();

	CSize sz = pCommandBar->GetButtonSize();
	regSetInt((REG_ROOT_HMSSHELL),(REG_TOOLBAR_KEY),(REG_TOOLBAR_HEIGHT),(DWORD)sz.cy);

//	UNCOMMENT THIS TO ADD TABBED MDI WINDOWS
	VERIFY(m_MTIClientWnd.Attach(this));

	m_MTIClientWnd.EnableToolTips();

	CXTPTabPaintManager* pTabPaintManager = new CXTPTabPaintManager();
	pTabPaintManager->SetAppearance(xtpTabAppearancePropertyPage2003);
	pTabPaintManager->m_bOneNoteColors = FALSE;
	pTabPaintManager->m_bHotTracking = TRUE;
	pTabPaintManager->m_bShowIcons = FALSE;
	pTabPaintManager->m_bBoldSelected = TRUE;
	pTabPaintManager->SetPosition( xtpTabPositionTop );

	m_MTIClientWnd.SetPaintManager(pTabPaintManager);

	// Just deternim if we,re on a network; 071221 p�d
	setOnlineOfflineOnStatusBar();

	setServerInfoOnStatusBar(m_bConnected);

	// Get info. from hms_administrator table; 080415 p�d
	getHmsUserDB();
	

	// Try to read NEW information from Registry, to determin'
	// if there's any; 081212 p�d
	TCHAR szURL[128];
	TCHAR szFilePath[MAX_PATH];
	getRSSLocation(szURL,szFilePath);
	if (_tcslen(szURL) > 0 && _tcslen(szFilePath) > 0)
	{
		// Check registry kye, to see if we're supposed to
		// check out news from the man; 081201 p�d
		if (getConnectToWEB() == 1 && m_enumCONN_STATUS == CONN_NETWORK)
		{
			// Start the timer control
			SetTimer(ID_TIMER, 1000*2, 0);
		}
	}

	// Create a timer to make sure db connection is alive; 090423 Peter
	if(m_bAdminConnected == TRUE && m_bConnected == TRUE) SetTimer(ID_TIMER2, 5000, 0);

	return 0;
}

void CMainFrame::OnTimer(UINT nIDEvent)
{
	switch( nIDEvent )
	{
	case ID_TIMER:
		// Run news only once; 081202 p�d
		KillTimer(ID_TIMER);
		// Run newsbulletines after timeout; 081202 p�d
		newsBulletines();

		break;
	case ID_TIMER2:
		// If db connection is dead try reconnecting; 090325 Peter
		// Note! In case auto-commit is turned off connection may appear to be dead until the transaction is committed.
		if( !m_saConnection.isAlive() && m_saConnection.AutoCommit() )
		{
			if( !m_saConnection.isAlive() && m_saConnection.AutoCommit() )
			{
				SAConnection *pNewCon = new SAConnection;
				if( makeDBConnection(*pNewCon, true) )
				{
					// Disconnect old connection; DISABLED this may cause a crash if the connection is used before the new connection instance have been copied
					/*if( m_saConnection.isConnected() )
					{
						m_saConnection.Disconnect();
					}
					if( m_saAdminConnection.isConnected() )
					{
						m_saAdminConnection.Disconnect();
					}
					*/

					SAConnection *pNewAdmin = new SAConnection;
					if( makeAdminDBConnection(*pNewAdmin, true) )
					{
						memcpy(&m_saAdminConnection, pNewAdmin, sizeof(SAConnection));
					}
					else
					{
						// If connect failed we don't need this new instance
						//delete pNewAdmin; COMMENTED OUT deleting this pointer will trigger an exception
					}

					// HACKHACK: copy new connection instance into out existing connection instance (this will cause a small memory leak as the destructor of the old instance will never be called)
					// This is the only solution using this approach; we cannot leave the old connection in a disconnected state, it will cause the program to crash when trying to execute a sql statement.
					// Another approach would be to use a two-dimensional pointer (a pointer to an address which points to the address of the connection instance), this way
					// we could just swap the first pointer to point to the new connection instance.
					memcpy(&m_saConnection, pNewCon, sizeof(SAConnection));
				}
				else
				{
					// If connect failed we don't need this new instance
					//delete pNewCon; COMMENTED OUT deleting this pointer will trigger an exception
				}
			}
		}
		break;
	}

	CMDIFrameWnd::OnTimer(nIDEvent);
}

void CMainFrame::OnShowWindow(BOOL bShow,UINT nStatus)
{
	CMDIFrameWnd::OnShowWindow(bShow,nStatus);
}


BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.lpszClass = (PROGRAM_NAME);
	if( !CXTPMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}

// 2009-01-30 P�D
// Handle key actions
BOOL CMainFrame::PreTranslateMessage(MSG *pMsg)
{
	BOOL bIsShiftKey = (GetKeyState(VK_SHIFT) < 0);
	BOOL bIsCtrlKey = (GetKeyState(VK_CONTROL) < 0);
	BOOL bMax;
	// Check to see if there's any Childwindows open, if not there's no need
	// to process any messages; 090130 p�d
	CMDIChildWnd *pWnd = MDIGetActive(&bMax);
	if (pWnd == NULL) return CXTPMDIFrameWnd::PreTranslateMessage(pMsg);
	// Check if virtual SHIFT key is pressed. If so, don't process message; 090130 p�d
	if (bIsShiftKey) return CXTPMDIFrameWnd::PreTranslateMessage(pMsg);

	// Setup Key according to PL document HMSFS 14-101-2002; 090130 p�d
	if (pMsg->message == WM_KEYUP)
	{
		switch (pMsg->wParam)
		{
			case VK_F3 : if (m_bTBNewBtnStatus && !bIsCtrlKey) sendMessageToChild(ID_NEW_ITEM); return TRUE;
			case VK_F4 : if (m_bTBDeleteBtnStatus && !bIsCtrlKey) sendMessageToChild(ID_DELETE_ITEM); return TRUE; 
			case VK_F5 : if (m_bDBNavigationBtnList && !bIsCtrlKey) sendMessageToChild(ID_DBNAVIG_LIST); return TRUE;
			case VK_F11 : if (!bIsCtrlKey) sendMessageToChild(ID_EXECUTE_ITEM); return TRUE;
			// Global function for Save = CTRL+S and Open = CTRL+O not used, because CTRL+S is used
			// in ReportControl as EDIT (for some reason!); 090202 p�d
			// 0x53 = S => CTRL+S (SAVE)
			//case 0x53	: if (m_bTBSaveBtnStatus && bIsCtrlKey) sendMessageToChild(ID_SAVE_ITEM); return TRUE;
			// 0x4F = O => CTRL+O (OPEN)
			//case 0x4F	: if (m_bTBOpenBtnStatus && bIsCtrlKey) sendMessageToChild(ID_OPEN_ITEM); return TRUE;
		};
	}	// if (pMsg->message == WM_KEYUP)

	return CXTPMDIFrameWnd::PreTranslateMessage(pMsg);
}

BOOL CMainFrame::ShowWindowEx(int nCmdShow)
{
	ASSERT_VALID(this);

	// Restore frame window size and position.
	m_wndPosition.LoadWindowPos(this);
	nCmdShow = m_wndPosition.showCmd;

	return ShowWindow(nCmdShow);
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	if ( CMDIFrameWnd::OnCreateClient(lpcs, pContext) ) 
  { 
		return TRUE; 
  } 
  else 
    return FALSE; 
}

void CMainFrame::OnChangeServerCmd()
{
	setServerInfoOnStatusBar(m_bConnected);
}


// Handle commands etc, from Popup controil for news items; 061030 p�d
LRESULT CMainFrame::OnPopUpNotify(WPARAM wParam, LPARAM lParam)
{
	// process notify from popup wnd

	if(wParam == XTP_PCN_ITEMCLICK)
	{
		CXTPPopupItem* pItem = (CXTPPopupItem*)lParam;
		ASSERT(pItem);

		int nId = pItem->GetID();

		if(nId >= ID_GOTO_SITE)
		{
			// display the news in a browser
			::ShellExecute(NULL, _T("open"), m_vecNewsBulletines[nId - ID_GOTO_SITE].getLink(), NULL, NULL, SW_SHOW);
		}
		else if(nId == ID_POPUP_CLOSE)
		{
			pItem->GetPopupControl()->Close();
		}
	}
	else if(wParam == XTP_PCN_STATECHANGED)
	{
		CXTPPopupControl* pControl = (CXTPPopupControl*)lParam;
		ASSERT(pControl);

		if(pControl->GetPopupState() == xtpPopupStateClosed)
		{
			m_lstPopupControl.RemoveAt(m_lstPopupControl.Find(pControl));
			m_pActivePopup = NULL;

			if (m_lstPopupControl.IsEmpty())
			{
				enableItems(TRUE);				
			}	// if (m_lstPopupControl.IsEmpty())
			
			delete pControl;
		}	// if(pControl->GetPopupState() == xtpPopupStateClosed)
	}	// else if(wParam == XTP_PCN_STATECHANGED)

	return TRUE;
}


BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	STRUCT_FILETYPE structFileType;

	// Handle incoming filetype message.
	if (pData->cbData == sizeof(structFileType))
	{
		try
		{
			memcpy_s(&structFileType, sizeof(structFileType), pData->lpData, pData->cbData);

			if(structFileType.Id == ID_LPARAM_COMMAND_HANDLE_FILETYPE)
			{
				CString fileType=CString(structFileType.Type);
				if(fileType == KEGFILE_EXT)
				{ 
          //Visa och maximera f�nstret f�r HMS
					ShowWindow( SW_SHOWMAXIMIZED);
					//Handle .KEG filetype
					//.keg filer aktiverar modul 2201 f�r Holmen som �r import av .keg rapporter f�r KORUS
					executeShellTreeItem(2201,_T("OpenSuite"),_T("Holmen.dll"));
					//S�nd meddelande till importdelen i UMKORUS.dll med commando och s�kv�gen som g�ller
					sendMessageToChild(ID_LPARAM_COMMAND_HANDLE_FILETYPE,(LPARAM)structFileType.Path);	
					return TRUE;
				}
			}
		}
		catch (CException* e)
		{ 
		}

		return FALSE;	  
	}
	else
	{
		// Just deternim if we're on a network; 071221 p�d
		setOnlineOfflineOnStatusBar();
		// Only do this if we're on a network; 071221 p�d
		if (m_enumDATABASE_CONNECTION_STATUS == DCS_ON_NETWORK ||
			m_enumDATABASE_CONNECTION_STATUS == DCS_ON_LOCAL ||
			m_enumDATABASE_CONNECTION_STATUS == DCS_NOTHING)
		{
			if( m_enumCONN_STATUS == CONN_NO_NETWORK )	// No connection to LAN; 071221 p�d
			{
				// Disconnects from database and set m_bConnected to FALSE; 071221 p�d
				setServerInfoOnStatusBar(FALSE);
			}
			else if( m_enumCONN_STATUS == CONN_NETWORK )	// Connected to LAN; 071221 p�d
			{
				if (!m_bConnected)
				{
					setServerInfoOnStatusBar(TRUE);
				}
			}

		}	// if (m_enumDATABASE_CONNECTION_STATUS == DCS_ON_NETWORK)
		else
		{
			if (!m_bConnected)
			{
				setServerInfoOnStatusBar(TRUE);
			}
		}
 
		// local system is connected to network
		// if size doesn't match we don't know what this is
		if (pData->cbData == sizeof( DB_CONNECTION_DATA))
		{
			COPYDATASTRUCT HSData;
			memset(&HSData, 0, sizeof(COPYDATASTRUCT));
			HSData.dwData = 1;
			HSData.cbData = sizeof(DB_CONNECTION_DATA);
			HSData.lpData = &m_DBConnection;
			m_DBConnection.conn = &m_saConnection;
			m_DBConnection.admin_conn = &m_saAdminConnection;

			::SendMessage(pWnd->GetSafeHwnd(),WM_COPYDATA,(WPARAM)this->GetSafeHwnd(), (LPARAM)&HSData);

		}
	}
	return CXTPMDIFrameWnd::OnCopyData(pWnd, pData);

}
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CXTPMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CXTPMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnAbout(void)
{
}

void CMainFrame::OnChangeLanguage(void)
{

	if (m_wndLangDlg.GetSafeHwnd() != NULL)
	{
		m_wndLangDlg.setLanguage();
		m_wndLangDlg.ShowWindow(SW_NORMAL);
		m_wndLangDlg.CenterWindow();
		m_wndLangDlg.UpdateWindow();
	}
}


void CMainFrame::OnAdministration(void)
{
	doAdminSetup();
}

void CMainFrame::OnBackup(void)
{
	doDatabaseBackup();
}

void CMainFrame::OnRestoreBackup(void)
{
	doDatabaseRestore();
}

void CMainFrame::OnCopyBackup(void)
{
	doDatabaseCopy();
}

void CMainFrame::OnRestoreLayout(void)
{
	CString root;
	HKEY hKey;
	TCHAR szPath[MAX_PATH];

	// Restore window layout and menus
	if( AfxMessageBox(getResStr(IDS_STRING911), MB_YESNO|MB_ICONEXCLAMATION) == IDYES )
	{
		// Delete all Placement and Report keys
		if( RegOpenKeyEx(HKEY_CURRENT_USER, REG_ROOT, NULL, KEY_READ, &hKey) == ERROR_SUCCESS )
		{
			DeleteLayoutKeys(hKey, REG_ROOT);
			RegCloseKey(hKey);
		}

		// Delete individual keys
		root = REG_ROOT;
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_CommandBars"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_CommandBars-Bar0"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_CommandBars-Bar1"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_CommandBars-Bar2"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_CommandBars-Controls"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_CommandBars-DockBar0"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_CommandBars-DockBar1"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_CommandBars-DockBar2"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_CommandBars-DockBar3"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_CommandBars-Options"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_CommandBars-Summary"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_NavigationBars"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_PANE_SELECTED"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\HMSShell\\HMS_TOOLBAR"));
		RegDeleteTree(HKEY_CURRENT_USER,root + _T("\\HMSShell\\DockingPaneLayouts"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\UMGIS\\CommandBarLayout"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\UMGIS\\CommandBarLayout-Bar0"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\UMGIS\\CommandBarLayout-Bar1"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\UMGIS\\CommandBarLayout-Bar2"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\UMGIS\\CommandBarLayout-Bar3"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\UMGIS\\CommandBarLayout-Controls"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\UMGIS\\CommandBarLayout-DockBar0"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\UMGIS\\CommandBarLayout-Options"));
		RegDeleteKey(HKEY_CURRENT_USER, root + _T("\\UMGIS\\CommandBarLayout-Summary"));
		RegDeleteTree(HKEY_CURRENT_USER,root + _T("\\UMGIS\\DockingPaneLayouts"));

		// Delete ShellData files
		if ( SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szPath)) )
		{
			_tcscpy(szPath + _tcslen(szPath), _T("\\HMS\\ShellData"));

			SHFILEOPSTRUCTW fileOperation;
			fileOperation.wFunc = FO_DELETE;
			fileOperation.pFrom = szPath;
			fileOperation.fFlags = FOF_NO_UI | FOF_NOCONFIRMATION;
			SHFileOperation(&fileOperation);
		}

		// Quit after deleting registry keys
		PostQuitMessage(0);
	}
}

int CMainFrame::DeleteLayoutKeys(HKEY hKey, CString path)
{
	const int MAX_KEY_LENGTH = 255;
	TCHAR    achKey[MAX_KEY_LENGTH];   // buffer for subkey name
    DWORD    cbName;                   // size of name string 
    TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
    DWORD    cchClassName = MAX_PATH;  // size of class string 
    DWORD    cSubKeys=0;               // number of subkeys 
	DWORD    cCheckedSubKeys=0;        // number checked of subkeys 
    DWORD    cbMaxSubKey;              // longest subkey size 
    DWORD    cchMaxClass;              // longest class string 
    DWORD    cValues;              // number of values for key 
    DWORD    cchMaxValue;          // longest value name 
    DWORD    cbMaxValueData;       // longest value data 
    DWORD    cbSecurityDescriptor; // size of security descriptor 
    FILETIME ftLastWriteTime;      // last write time 
	CString  csKey;
	HKEY     hSubKey;
	LRESULT  ret;

	ret = RegQueryInfoKey(hKey,
						  achClass,
						  &cchClassName,
						  NULL,
						  &cSubKeys,
						  &cbMaxSubKey,
						  &cchMaxClass,
						  &cValues,
						  &cchMaxValue,
						  &cbMaxValueData,
						  &cbSecurityDescriptor,
						  &ftLastWriteTime);
	if( ret == ERROR_SUCCESS )
	{
		// Query subkeys
		for( int i=0; i<cSubKeys; i++ )
		{ 
			cbName = MAX_KEY_LENGTH;
			ret = RegEnumKeyEx(hKey, i,
							   achKey,
							   &cbName,
							   NULL,
							   NULL,
							   NULL,
							   &ftLastWriteTime);

			if( ret == ERROR_SUCCESS )
			{
				// Check subkeys of this key
				csKey.Format(_T("%s\\%s"), path, achKey);
				ret = RegOpenKeyEx(HKEY_CURRENT_USER, csKey, NULL, KEY_READ, &hSubKey);
				if( ret == ERROR_SUCCESS )
				{
					// Check if key should be deleted
					cCheckedSubKeys = DeleteLayoutKeys(hSubKey, csKey);
					if( cCheckedSubKeys == 0 && 
						(CString(achKey) == _T("Placement") || CString(achKey) == _T("Report")) )
					{
						RegDeleteKey(HKEY_CURRENT_USER, csKey);
					}
					RegCloseKey(hSubKey);
				}
			}
		}
	}

	return cSubKeys;
}

void CMainFrame::OnNewItemBtnClick()
{
	sendMessageToChild(ID_NEW_ITEM);
}

void CMainFrame::OnOpenItemBtnClick()
{
	sendMessageToChild(ID_OPEN_ITEM);
}

void CMainFrame::OnSaveItemBtnClick()
{
	sendMessageToChild(ID_SAVE_ITEM);
}

void CMainFrame::OnDeleteItemBtnClick()
{
	sendMessageToChild(ID_DELETE_ITEM);
}

void CMainFrame::OnPreviewItemBtnClick()
{
	sendMessageToChild(ID_PREVIEW_ITEM);
}

void CMainFrame::OnDBNavigStartBtnClick()
{
	sendMessageToChild(ID_DBNAVIG_START);
}

void CMainFrame::OnDBNavigPrevBtnClick()
{
	sendMessageToChild(ID_DBNAVIG_PREV);
}

void CMainFrame::OnDBNavigNextBtnClick()
{
	sendMessageToChild(ID_DBNAVIG_NEXT);
}

void CMainFrame::OnDBNavigEndBtnClick()
{
	sendMessageToChild(ID_DBNAVIG_END);
}

void CMainFrame::OnDBNavigListBtnClick()
{
	sendMessageToChild(ID_DBNAVIG_LIST);
}

//Tagit bort, finns redan i enskilda moduler ist�llet #4493 20151013
/* 
void CMainFrame::OnHelp()
{
	CString sLangSet;
 	CString sPath;
	sLangSet = getLangSet();
	sPath.Format(_T("%s\\%s%s%s"),getHelpDir(),_T("HMS"),sLangSet,HELPFILE_EXT);
	::HtmlHelp(this->m_hWnd,sPath,HH_DISPLAY_TOPIC,NULL);
}*/

void CMainFrame::OnNews()
{
	setOnlineOfflineOnStatusBar();

	m_bShowNews = TRUE;	// we don't want the popup to pop up.
	newsBulletines();	// gather news
}

void CMainFrame::OnWebSite()
{
	TCHAR szURL[256];
	if (getHMSHomepageURL(szURL))
	{
		::ShellExecute(NULL, _T("open"), (szURL), NULL, NULL, SW_SHOW);
	}
}

LRESULT CMainFrame::OnDockingPaneNotify(WPARAM wParam, LPARAM lParam)
{
	CString S;

	if (wParam == XTP_DPN_SHOWWINDOW)
	{
		// get a pointer to the docking pane being shown.
		CXTPDockingPane* pPane = (CXTPDockingPane*)lParam;
		if (!pPane->IsValid())
		{
			if (pPane->GetID() == ID_PANE_NAVIGATION)
			{
				if (m_pDockingFrame == 0)
				{
					m_pDockingFrame = (CDockingFrame*)CDockingFrame::CreateObject();
					m_pDockingFrame->Create(this);
					m_pDockingFrame->ModifyStyleEx(WS_EX_CLIENTEDGE, 0);
					m_pDockingFrame->setTreeListVec( m_vecShellData );
					m_pDockingFrame->addPanes(m_nLastSelectedPaneID);
					// Added 081205 p�d
					getPanesHiddenOrVisible();
					m_pDockingFrame->setVisiblePanes(m_vecVisibleItemsOnNavPane);
				}	// if (m_pDockingFrame == 0)
				pPane->Attach(m_pDockingFrame);
			} // if (pPane->GetID() == ID_PANE_NAVIGATION)
		} // if (!pPane->IsValid())
		return TRUE; // handled
	}	// if (wParam == XTP_DPN_SHOWWINDOW)

	return FALSE;
}

void CMainFrame::OnShowPane(void)
{
	BOOL bIsNavBarHidden = 	GetDockingPaneManager()->IsPaneHidden(ID_PANE_NAVIGATION);
	BOOL bIsNavBarClosed = 	GetDockingPaneManager()->IsPaneClosed(ID_PANE_NAVIGATION);
	// Show/Hide the navigation pane; 051114 p�d
	if (bIsNavBarHidden || bIsNavBarClosed)
		GetDockingPaneManager()->ShowPane(ID_PANE_NAVIGATION);
	else
		GetDockingPaneManager()->HidePane(ID_PANE_NAVIGATION);
}

void CMainFrame::OnUpdateMenuView(CCmdUI* pCmdUI)
{
	BOOL bIsNavBarHidden = 	GetDockingPaneManager()->IsPaneHidden(ID_PANE_NAVIGATION);
	BOOL bIsNavBarClosed = 	GetDockingPaneManager()->IsPaneClosed(ID_PANE_NAVIGATION);

	pCmdUI->SetCheck(!bIsNavBarHidden && !bIsNavBarClosed);
}

void CMainFrame::OnUpdateMenuToolsDatabase(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsUMDatabase );
}

void CMainFrame::OnUpdateMenuNews(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bTBMenuNews );
}

//	m_bDBNavigationBtnStart
//	m_bDBNavigationBtnPrev
//	m_bDBNavigationBtnNext
//	m_bDBNavigationBtnEnd
//	m_bDBNavigationBtnList

void CMainFrame::OnUpdateNewItemBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bTBNewBtnStatus );
}

void CMainFrame::OnUpdateOpenItemBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bTBOpenBtnStatus );
}

void CMainFrame::OnUpdateSaveItemBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bTBSaveBtnStatus );
}

void CMainFrame::OnUpdateDeleteItemBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bTBDeleteBtnStatus );
}

void CMainFrame::OnUpdatePreviewItemBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bTBPreviewBtnStatus );
}

void CMainFrame::OnUpdateBackupBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bTBBackupDatabaseStatus );
}

void CMainFrame::OnUpdateRestoreDatabaseBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bTBRestoreDatabaseStatus );
}

void CMainFrame::OnUpdateCopyDatabaseBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bTBCopyDatabaseStatus );
}

void CMainFrame::OnUpdateDBToolbarBtnStart(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bDBNavigationBtnStart );
}

void CMainFrame::OnUpdateDBToolbarBtnPrev(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bDBNavigationBtnPrev );
}

void CMainFrame::OnUpdateDBToolbarBtnNext(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bDBNavigationBtnNext );
}

void CMainFrame::OnUpdateDBToolbarBtnEnd(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bDBNavigationBtnEnd );
}

void CMainFrame::OnUpdateDBToolbarBtnList(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bDBNavigationBtnList );
}

void CMainFrame::OnCustomize()
{
	// get a pointer to the command bars object.
	CXTPCommandBars* pCommandBars = GetCommandBars();
	if (pCommandBars == NULL)
		return;

	// instanciate the customize dialog
	CXTPCustomizeSheet dlg(pCommandBars);

	// add the options page to the customize dialog.
	CXTPCustomizeOptionsPage pageOptions(&dlg);
	dlg.AddPage(&pageOptions);

	// add the commands page to the customize dialog.
	CXTPCustomizeCommandsPage* pPageCommands = dlg.GetCommandsPage();
	pPageCommands->AddCategories(IDR_MAINFRAME);

	// initialize the commands page page.
	pPageCommands->InsertAllCommandsCategory();
	pPageCommands->InsertBuiltInMenus(IDR_MAINFRAME);
	pPageCommands->InsertNewMenuCategory();

	// display the customize dialog.
	dlg.DoModal();
}

void CMainFrame::OnOnlineOfflineToggleBtn(void)
{
	// Try to connect; 070329 p�d
	setServerInfoOnStatusBar(((m_bConnected) ? FALSE : TRUE));
}

// Try to find the right Module instance (SUITE) for
// NavigationBar selection; 051124 p�d
HINSTANCE CMainFrame::getModuleInstance(LPCTSTR path)
{
	CModuleInstance data;
	if (m_vecHInst.size() > 0)
	{
		for (UINT i = 0;i < m_vecHInst.size();i++)
		{
			data = m_vecHInst[i];
			if (_tcscmp(CString(data.getModulePath()).MakeLower(),CString(path).MakeLower()) == 0)
			{
				return data.getHInst();
			}
		}
	}
	return NULL;

}

// Handles messages from the TreeView in NavigatorBar; 051117 p�d
LRESULT CMainFrame::OnTreeListMessage( WPARAM wParam, LPARAM lParam )
{
	m_sSuitePath = _T("");
	m_sCaption = _T("");
	m_sNodeName = _T("");
	m_nCounter = -1;
	m_nPaneID = -1;
	m_nNumOf = 0;
	m_bContinue = TRUE;

	switch (wParam)
	{
		case 0 :	// Messages from CPADTreeCtrl
		{
			m_pActiveTreeListItem = (CTreeList *)lParam;
			if (m_pActiveTreeListItem != NULL)
			{
				// Wait cursor added 070704 p�d
				HCURSOR hCursor = SetCursor(::LoadCursor(NULL,IDC_WAIT));

				executeShellTreeItem(*m_pActiveTreeListItem);
				
				// Back to Original cursor; 070704 p�d
				SetCursor(hCursor);

			}	// if (data != NULL)
			break;
		}	// case 0 :

		////////////////////////////////////////////////////////////////////////////////////////////
		// CASE ID_POPUP_HIDE
		case ID_POPUP_HIDE :	// Messages from CPADTreeCtrl popup menu
		{
			treeItemHide(lParam);
			break;
		}	// case ID_POPUP_HIDE :

		////////////////////////////////////////////////////////////////////////////////////////////
		// CASE ID_POPUP_SHOW
		case ID_POPUP_SHOW :	// Messages from CPADTreeCtrl popup menu
		{
			treeItemDialog(lParam);
			break;
		}	// case ID_POPUP_SHOW :

		////////////////////////////////////////////////////////////////////////////////////////////
		// CASE ID_POPUP_ADD_TO_SHORTCUTS
		case ID_POPUP_ADD_TO_SHORTCUTS :	// Messages from CPADTreeCtrl popup menu
		{
			treeItemAddToShortcuts(lParam);
			break;
		}	// case ID_POPUP_ADD_TO_SHORTCUTS

	};	// switch (wParam)

	return 0L;
}

// This method can be used to handle messages from Suite/User modules; 051205 p�d
LRESULT CMainFrame::OnMessageFromSuite( WPARAM wParam, LPARAM lParam )
{
	/* Commented out, handle these messages in method: PreTranslateMessage(); 090130 p�d
	////////////////////////////////////////////////////////////////////////
	// Handle messages equals to key strokes, in e.g. reportcontrols; 060227 p�d
	switch(wParam)
	{
	// Handle adding new item to e.g. a ReportControl
	case VK_F3 :
	case VK_INSERT :
	{
	sendMessageToChild(ID_NEW_ITEM);
	break;
	}	// case VK_F3

	// Equals to klicking on the List toolbar button; 070103 p�d
	case VK_F5 :
	{
	sendMessageToChild(ID_DBNAVIG_LIST);
	break;
	}	// case VK_F5

	// Handle deleting item from e.g. a ReportControl
	case VK_F4 :
	case VK_DELETE :
	{
	sendMessageToChild(ID_DELETE_ITEM);
	break;
	}	// case VK_F4	

	};
	*/
	////////////////////////////////////////////////////////////////////////
	// This part handles toolbar enable/disable
	// IDR_MAINFRAME
	if (wParam == ID_NEW_ITEM)
	{
		m_bTBNewBtnStatus	= (lParam == 1);
	}
	else if (wParam == ID_OPEN_ITEM)
	{
		m_bTBOpenBtnStatus	= (lParam == 1);
	}
	else if (wParam == ID_SAVE_ITEM)
	{
		m_bTBSaveBtnStatus	= (lParam == 1);
	}
	else if (wParam == ID_DELETE_ITEM)
	{
		m_bTBDeleteBtnStatus	= (lParam == 1);
	}
	else if (wParam == ID_PREVIEW_ITEM)
	{
		m_bTBPreviewBtnStatus	= (lParam == 1);
	}

	////////////////////////////////////////////////////////////////////////
	// This part handles database navigation toolbar enable/disable
	// IDR_TOOLBAR_NAVIG
	else if (wParam == ID_DBNAVIG_START || wParam == ID_DBNAVIG_PREV)
	{
		m_bDBNavigationBtnStart	= (lParam == 1);
		m_bDBNavigationBtnPrev	= (lParam == 1);
	}
	else if (wParam == ID_DBNAVIG_END || wParam == ID_DBNAVIG_NEXT)
	{
		m_bDBNavigationBtnNext	= (lParam == 1);
		m_bDBNavigationBtnEnd	= (lParam == 1);
	}
	else if (wParam == ID_DBNAVIG_LIST)
	{
		m_bDBNavigationBtnList	= (lParam == 1);
	}

	////////////////////////////////////////////////////////////////////////
	// Handle messages from suite/user module, handling Reports; 060119 p�d
	else if (wParam == USER_MSG_EXECUTE_SHELL)
	{
		_user_msg *msg = (_user_msg *)lParam;
		// Make sure that lParam holds the _user_msg by checkin' it's size; 070813 p�d
		if (sizeof(*msg) == sizeof(_user_msg))
		{
			executeShellTreeItemEx(msg);
			sendMessageToChild(ID_REPORT_OPEN,(LPARAM)lParam);
		}
	}

	////////////////////////////////////////////////////////////////////////
	// WPARAM and LPARAM combained to a unike number. Used e.g. in CMyReportCtrl
	// in HMSFuncLib
	else if (wParam >= ID_WPARAM_VALUE_FROM && wParam <= ID_WPARAM_VALUE_TO)
	{
		CString S;
		// Make sure that lParam holds the _doc_identifer_msg by checkin' it's size; 070813 p�d
		if (wParam == ID_WPARAM_VALUE_FROM + 0x02)
		{
			_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
			if (sizeof(*msg) == sizeof(_doc_identifer_msg))
				sendMessageToAChild((int)wParam,msg);
			else
				sendMessageToChild((int)wParam,(LPARAM)lParam);
		}
		else
			sendMessageToChild((int)wParam,(LPARAM)lParam);
	}

	////////////////////////////////////////////////////////////////////////
	// Handle message;ID_DO_SOMETHING_IN_SHELL, tells "me" to run a method.
	// ID_LPARAM_COMMAND1 = run setServerInfoOnStatusBar()
	else if (wParam == ID_DO_SOMETHING_IN_SHELL)
	{
		switch (lParam)
		{
			case ID_LPARAM_COMMAND1 :
				{
					// Reload information on created user-databases; 081021 p�d
					getHmsUserDB();
					// Updates the DB server Information on the StatusBar; 070410 p�d
					setServerInfoOnStatusBar(TRUE);
					// Close open child-windows; 090316 p�d
					//closeChildWindows(FALSE /* = Don't close database-window */);
				}
				break;
			case ID_LPARAM_COMMAND2 :
				{
					// Uses the _user_msg structure to send information to 
					// Suite/Module; 070410 p�d
					if (m_pActiveTreeListItem != NULL)
					{
						_user_msg	msg = _user_msg(m_pActiveTreeListItem->getIndex(),
							_T(""),_T(""),_T(""),
							m_pActiveTreeListItem->getShellDataFile(),
							m_pActiveTreeListItem->getSuite());
						sendMessageToChild((int)wParam+lParam,(LPARAM)&msg);
					}
				}
				break;
			// Command comes from UMDatabase.dll if there's no 
			// Items in hms_administrator database and user adds
			// a database; 080421 p�d
			case ID_LPARAM_COMMAND4 :
				{
					getHmsUserDB();
					// Just deternim if we,re on a network; 071221 p�d
					setOnlineOfflineOnStatusBar();
					setServerInfoOnStatusBar(m_bConnected);
					// Create tables for Active database; 080421 p�d
					runDoDatabaseTablesInSuitesModules();	
				}
				break;
			case ID_LPARAM_COMMAND_DBTIMER_START :	// Start the database checking timer (ID_TIMER2)
				{
					SetTimer(ID_TIMER2, 5000, 0);
				}
				break;
			case ID_LPARAM_COMMAND_DBTIMER_STOP :	// Stop the database checking timer (ID_TIMER2)
				{
					KillTimer(ID_TIMER2);
				}
				break;
		};

	}

	return 0L;
}

// Public method
void CMainFrame::executeShellTreeItem(int index,LPCTSTR func,LPCTSTR suite)
{
  HTREEITEM hNode = NULL;
	CString sPath;
	int nRetValue = 1;
	HINSTANCE hInst = NULL;

	// Check index value: if index > 0 then execute a Suite or User module
	// function. If index = -1 then this is a HelpFile Item, if index = -2
	// this is a

	// CRuntimeClass used to open a form in the Suite (DLL); 051117 p�d
	typedef CRuntimeClass *(*Func)(int idx,LPCTSTR func,CWnd *wnd,vecINDEX_TABLE &,int *ret);
	
	Func proc;
	sPath.Format(_T("%s%s"),getSuitesDir(),suite);
	if ((hInst = getModuleInstance(sPath)) != NULL)
	{
		if (hInst != NULL)
		{
			proc = (Func)GetProcAddress((HMODULE)hInst, OPEN_SUITE_FUNCTION);
			if (proc != NULL)
			{
				// call the function
				proc(index,func,this,m_vecIndexTable,&nRetValue);		
			} // if (proc != NULL)
		}	// if (hInst != NULL)
	} // if ((hInst = getModuleInstance(sPath) != NULL)

	if (nRetValue == 0)
	{
		::MessageBox(0,getResStr(IDS_STRING136),
			             getResStr(IDS_STRING900),
									 MB_ICONASTERISK | MB_OK);
	}
}

void CMainFrame::executeShellTreeItem(CTreeList &data) //(int index,LPCTSTR node_name,LPCTSTR func,LPCTSTR suite)
{
  HTREEITEM hNode = NULL;
	CString sPath,S;
	CString sLangSet;
	CString sLangFN;
	CString sDBDataMissingMsg;
	BOOL bNoDB = FALSE;
	int nRetValue = 1;
	HINSTANCE hInst = NULL;
	/*TCHAR szDBLocation[127];
	TCHAR szDBServer[127];
	TCHAR szDBUser[127];
	TCHAR szDBPsw[127];
	TCHAR szDSNName[127];
	TCHAR szDBClient[127];
	TCHAR szDataBase[127];
	int nIsWindowsAuthentication;*/

	/*if (_tcscmp(data.getNodeName(),(EXEC_ITEM)) == 0 ||
			_tcscmp(data.getNodeName(),(MEXEC_ITEM)) == 0)
	{
		// Get info. on which database server's used; 060310 p�d
		GetAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,&nIsWindowsAuthentication);
		GetUserDBInRegistry(szDataBase);

		// Do a check of database-connection. If any of the below is  missing
		// in registry. Ask user to fill in information; 090119 p�d
		if (_tcscmp(szDBServer,_T("")) == 0 &&
				_tcscmp(szDBLocation,_T("")) == 0 &&
				_tcscmp(szDBClient,_T("")) == 0 &&
				_tcscmp(szDataBase,_T("")) == 0)
		{
			bNoDB = TRUE;
		}

		if (!bNoDB && 
				(_tcscmp(szDBServer,_T("")) == 0 ||
				_tcscmp(szDBLocation,_T("")) == 0 ||
				_tcscmp(szDBClient,_T("")) == 0 ||
				_tcscmp(szDataBase,_T("")) == 0))
		{
			sDBDataMissingMsg.Format(_T("%s\n%s\n\n%s\n%s\n%s\n%s\n\n"),
							getResStr(IDS_STRING183),
							getResStr(IDS_STRING184),
							getResStr(IDS_STRING185),
							getResStr(IDS_STRING186),
							getResStr(IDS_STRING187),
							getResStr(IDS_STRING188));
			AfxMessageBox(sDBDataMissingMsg);
			doAdminSetup();
			return;
		}
	}*/

	// Check index value: if index > 0 then execute a Suite or User module
	// function. If index = -1 then this is a HelpFile Item, if index = -2
	// this is a
	
	if (_tcscmp(data.getNodeName(),(EXEC_ITEM)) == 0 ||
			_tcscmp(data.getNodeName(),(MEXEC_ITEM)) == 0 ||
		  _tcscmp(data.getNodeName(),(REPORT_ITEM)) == 0)
	{

		// CRuntimeClass used to open a form in the Suite (DLL); 051117 p�d
		typedef CRuntimeClass *(*Func)(int idx, LPCTSTR func, CWnd *wnd, vecINDEX_TABLE &, int *ret);
		typedef CRuntimeClass *(*Func1)(int idx, LPCTSTR func, CWnd *wnd, vecINDEX_TABLE &, LPCTSTR arg1, int *ret);
	  
		Func proc;
		Func1 proc1;
		sPath.Format(_T("%s%s"),getSuitesDir(),data.getSuite());

		if ((hInst = getModuleInstance(sPath)) != NULL)
		{
			if (hInst != NULL)
			{
				if (_tcslen(data.getArg1()) == 0)
				{		
					proc = (Func)GetProcAddress((HMODULE)hInst, OPEN_SUITE_FUNCTION);
					if (proc != NULL)
					{
						// call the function
						proc(data.getIndex(), data.getFunc(), this, m_vecIndexTable, &nRetValue);
					} // if (proc != NULL)
				}	// if (_tcslen(data.getArg1()) == 0)
				else
				{
					proc1 = (Func1)GetProcAddress((HMODULE)hInst, OPEN_SUITE_FUNCTION1);
					if (proc1 != NULL)
					{
						proc1(data.getIndex(), data.getFunc(), this, m_vecIndexTable,data.getArg1(), &nRetValue);
					} // if (proc != NULL)
				}
			}	// if (hInst != NULL)
		} // if ((hInst = getModuleInstance(sPath) != NULL)
		if (nRetValue == 0)
		{
			::MessageBox(0,getResStr(IDS_STRING136),
				             getResStr(IDS_STRING900),
										 MB_ICONASTERISK | MB_OK);
		}
	}	// if (_tcscmp(data.getNodeName(),_T("ExecItem")) == 0 ||
	else if (_tcscmp(data.getNodeName(),HELP_ITEM) == 0 ||
		      _tcscmp(data.getNodeName(),MHELP_ITEM) == 0)
	{
		// Try to determin what we have here; 081126 p�d
		// chm = "Hj�lpfil"
		// doc = "Dokument (Word)"
		// pdf = "Acrobat Reader"
		// xls = "Excel"
		// www = Webaddress
		int nCurPos = 0;
		CString sToken1;
		CString sToken2;
		CString sFunc(data.getFunc());
		if (sFunc.Right(1) != ';')
			sFunc += ';';
		
		sToken1 = sFunc.Tokenize(_T(";;"),nCurPos);
		sToken2 = sFunc.Tokenize(_T(";;"),nCurPos);

		if (sToken2.CompareNoCase(_T("chm")) == 0)
		{
			sPath.Format(_T("%s\\%s%s.%s"),getHelpDir(),sToken1,m_sLangSet,sToken2);
			// We need to check that the file exists with language.
			// If not, we'll set default language = ENU (American english"
			// and hope for the best; 081126 p�d
			if (!fileExists(sPath))
				sPath.Format(_T("%s\\%s%s.%s"),getHelpDir(),sToken1,_T("ENU"),sToken2);
			// If we stall can't find the file, we'll skip the
			// language and run the filename as is; 081126 p�d
			if (!fileExists(getHelpDir() + _T("\\") + sPath))
				sPath.Format(_T("%s\\%s.%s"),getHelpDir(),sToken1,sToken2);

			::HtmlHelp(this->m_hWnd,sPath,HH_DISPLAY_TOPIC,NULL);
		}
		else if (sToken2.CompareNoCase(_T("doc")) == 0 || 
						 sToken2.CompareNoCase(_T("xls")) == 0 || 
						 sToken2.CompareNoCase(_T("pdf")) == 0)
		{
			sPath.Format(_T("%s%s.%s"),sToken1,m_sLangSet,sToken2);
			// We need to check that the file exists with language.
			// If not, we'll set default language = ENU (American english"
			// and hope for the best; 081126 p�d
			if (!fileExists(getHelpDir() + _T("\\") + sPath))
					sPath.Format(_T("%s%s.%s"),sToken1,_T("ENU"),sToken2);
			// If we still can't find the file, we'll skip the
			// language and run the filename as is; 081126 p�d
			if (!fileExists(getHelpDir() + _T("\\") + sPath))
					sPath.Format(_T("%s.%s"),sToken1,sToken2);

			ShellExecute(NULL,_T("Open"),sPath,NULL,getHelpDir(),SW_SHOW);
		}
		else if (sToken2.CompareNoCase(_T("www")) == 0)
		{
			ShellExecute(NULL,_T("Open"),sToken1,NULL,NULL,SW_SHOW);
		}

	}	// else if (_tcscmp(data.getNodeName(),_T("HelpItem")) == 0)
	else if (_tcscmp(data.getNodeName(),EXTERN_ITEM) == 0)
	{
		CString sFunc = data.getFunc();
		CString sExec;
		CString sArgs;
		CStringArray arrArgs;
		// Make sure the sFun string ends with a semicolon; 080124 p�d
		if (sFunc.Right(1).Compare(_T(";")) != 0)
			sFunc += _T(";");

		SplitString(sFunc,_T(";"),arrArgs);
		// Check if there's an argument string in data.getFunc()
		// If so, we need to divide the string into
		// The program and the argumenta; 080124 p�d
		if (arrArgs.GetCount() == 1)	// No arguments
		{
			sExec = arrArgs.GetAt(0);	// First item equals program-name; 080124 p�d
			ShellExecute(NULL,_T("Open"),sExec,NULL,data.getSuite(),SW_SHOW);
		}
		else if (arrArgs.GetCount() > 1)
		{
			sExec = arrArgs.GetAt(0);	// First item equals program-name; 080124 p�d
			sArgs.Empty();	// Clear
			// Bulid the argument string; 080124 p�d
			for (int i = 1 /* Don't add the first item */;i < arrArgs.GetCount();i++)
			{
				// Only one argument; 080124 p�d
				if (arrArgs.GetCount() == 2)
				{
					sArgs += arrArgs.GetAt(i);
				}	// if (arrArgs.GetCount() == 2)
				// More than one argument; 080124 p�d
				else
				{
					sArgs += arrArgs.GetAt(i) + _T(" ");
				}
			}	// for (int i = 0;i < arrArgs.GetCount();i++)
			ShellExecute(NULL,_T("Open"),sExec,sArgs,data.getSuite(),SW_SHOW);
		}	// else if (arrArgs.GetCount() > 1)
	}	// else if (_tcscmp(data.getNodeName(),_T("HelpItem")) == 0)
}


// Use this method when calling from a sute/user module through the _user_msg
// structure; 060619 p�d

void CMainFrame::executeShellTreeItemEx(_user_msg *msg)
{
  HTREEITEM hNode = NULL;
	CString sPath;
	int nRetValue = 1;
	HINSTANCE hInst = NULL;
	
  // CRuntimeClass used to open a form in the Suite (DLL); 051117 p�d
	typedef CRuntimeClass *(*Func)(_user_msg *msg,CWnd *wnd,vecINDEX_TABLE &,int *ret);
  
	Func proc;
	sPath.Format(_T("%s%s"),getSuitesDir(),msg->getSuite());
	if ((hInst = getModuleInstance(sPath)) != NULL)
	{
		if (hInst != NULL)
		{
			proc = (Func)GetProcAddress((HMODULE)hInst, CStringA(msg->getFunc()));
			if (proc != NULL)
			{
				try
				{
					// call the function
					proc(msg,this,m_vecIndexTable,&nRetValue);		
				}
				catch( System::Exception ^e )
				{
					AfxMessageBox(CString(e->Message));
				}
			} // if (proc != NULL)
		}	// if (hInst != NULL)
	} // if ((hInst = getModuleInstance(sPath) != NULL)

	if (nRetValue == 0)
	{
		::MessageBox(0,getResStr(IDS_STRING136),
			             getResStr(IDS_STRING900),
									 MB_ICONASTERISK | MB_OK);
	}
}

// Protected method

void CMainFrame::sendMessageToChild(int id,LPARAM param)
{
	// Find out which Child window to send message to; 060215 p�d
	CMDIFrameWnd *pFrame = (CMDIFrameWnd*)GetActiveFrame();
	if (pFrame != NULL)
	{
		CMDIChildWnd *pChild = (CMDIChildWnd *) pFrame->GetActiveFrame();
		if (pChild != NULL)
		{
			CDocument *pDoc = (CDocument *) pChild->GetActiveDocument();
			pChild->SendMessage(WM_USER_MSG_SUITE,(WPARAM)id,(LPARAM)param);
		}	// if (pChild != NULL)
	}	// if (pFrame != NULL)
}
// Method added 2007-08-13 p�d
// Pass a specific message class on to a Module, identified by it's Modulename.
// I.s. name set in StringTable e.g. Module5000 etc; 070813 p�d
void CMainFrame::sendMessageToAChild(int id,_doc_identifer_msg *msg)
{
	CString S;
	CString sDocName;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		POSITION posDOC = pTemplate->GetFirstDocPosition();
		// Open only one instance of this window; 061002 p�d
		while(posDOC != NULL)
		{
			CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
			if (sDocName.Compare(msg->getSendTo()) == 0)
			{
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						::SendMessage(pView->GetParent()->GetSafeHwnd(), WM_USER_MSG_SUITE,(WPARAM)id,(LPARAM)msg);
						break;
					}	// if (pView)
				}	// if(posView != NULL)
			}	// if (sDocName.Compare(msg->getDocName()) == 0)
		}	// while(posDOC != NULL)
	}	// while(pos != NULL)
}

BOOL CMainFrame::treeItemHide(LPARAM lParam)
{
	CString S;
	// Get CTreeList data sent from CPADTreeCtrl; 060425 p�d
	CTreeList *data = (CTreeList *)lParam;
	vecInt vecShowHideItems;
	if (data != NULL)
	{
		m_sCaption = data->getCaption();
		m_sNodeName = data->getNodeName();
		m_sSuitePath = data->getSuitePath();
		m_nCounter = data->getCounter();
	}	// if (data != NULL)

	// Compare selected item, from CPADTreeCtrl to items in TreeList vector
	// in CDockingFrame and also check Caption; 060425 p�d
	if (m_pDockingFrame)
	{
		CXTPShortcutBarItem *pItem = m_pDockingFrame->m_wndNavigationBar.GetSelectedItem();
		if (pItem)
		{
			m_nPaneID = pItem->GetID() - 1;
			vecShowHideItems.clear();
			m_pDockingFrame->getItemsToShowHide(m_nPaneID,vecShowHideItems);
			if (vecShowHideItems.size() > 0)
			{
				XMLHandler *xml = new XMLHandler();
				if (xml)
				{
					if (xml->load(m_sSuitePath))
					{
						for (UINT i = 0;i < vecShowHideItems.size();i++)
						{
								m_nCounter = vecShowHideItems[i];
								xml->setVisible(m_nCounter,FALSE);
						}	// for (UINT i = 0;i < vecShowHideItems.size();i++)

						xml->save(m_sSuitePath);
						xml->getShellTreeData( m_vecShellData, TRUE	/* Clear vector */ );
					}
	
					delete xml;

					if (m_pDockingFrame != 0)
					{
         		m_pDockingFrame->setTreeListVec( m_vecShellData );
						m_pDockingFrame->updPane(m_nPaneID);
					}

					m_bInRecalcLayout = FALSE;
					RecalcLayout();
					AfxGetApp()->OnIdle(0);
					UpdateWindow();
		
				} // if (xml)
			}	// if (vecShowHideItems.size() > 0)
		}	// if (pItem)
	}	// if (m_pDockingFrame)

	return TRUE;
}

BOOL CMainFrame::treeItemDialog(LPARAM lParam)
{
	UINT nSelectedPopupMenuID;
	XMLHandler *xml = new XMLHandler();
	CXTPShortcutBarItem *pItem = NULL;
	vecTreeList m_vecTreeList;
	// Original treelist vector, use this to compare with
	// vector from CShellTreeHiddenItems; 060504 p�d
	vecTreeList m_vecTreeListOrig;
	CTreeList *data = (CTreeList *)lParam;
	if (data != NULL)
	{
		m_sSuitePath = data->getSuitePath();
		m_sCaption = data->getCaption();
	}	// if (data != NULL)
	// Compare selected item, from CPADTreeCtrl to items in TreeList vector
	// in CDockingFrame and also check Caption; 060425 p�d
	if (m_pDockingFrame)
	{
		pItem = m_pDockingFrame->m_wndNavigationBar.GetSelectedItem();
		if (pItem)
		{
			m_nPaneID = pItem->GetID() - 1;
			// If data from CPADTreeList is NULL. I.e. there's no items in the
			// tree control, use items set in NavigationBar; 060427 p�d
			if (data == NULL || m_sSuitePath == _T(""))
			{
				data = (CTreeList *)pItem->GetItemData();
				m_sSuitePath = data->getSuitePath();
				m_sCaption = data->getCaption();
			}
		}	// if (pItem)
	}	// if (m_pDockingFrame)
	if (xml)
	{
		if (xml->load(m_sSuitePath))
		{
			xml->getShellTreeData(m_vecTreeList,TRUE);			
			xml->getShellTreeData(m_vecTreeListOrig,TRUE);			
		}	// if (xml->load(sSuitePath))
	}	// if (xml)

	// Open dialog show/hide items in selected shell tree etc.; 060428 p�d
	CShellTreeHiddenItems *pDlg = new CShellTreeHiddenItems(m_vecTreeList,m_sSuitePath,m_sCaption);
	if (pDlg)
	{
		m_bContinue = (pDlg->DoModal() == IDOK);
		if (m_bContinue)
		{
			// Use same  vector; 060427 p�d
			m_vecTreeList.clear();
			pDlg->getReturnTreeListVec(m_vecTreeList);
			nSelectedPopupMenuID = pDlg->getSelectedPopupMenuID();
		}
		delete pDlg;
	}
	// Do actual actions to shelltree; 060505 p�d
	if (m_vecTreeList.size() > 0 && m_bContinue)
	{
		// Compare selected item, from CPADTreeCtrl to items in TreeList vector
		// in CDockingFrame and also check Caption; 060425 p�d
		if (xml)
		{
			if (xml->load(m_sSuitePath))
			{
				xml->setupShellDataInXMLFile(m_vecTreeList);

				xml->save(m_sSuitePath);
				xml->getShellTreeData( m_vecShellData, TRUE	/* Clear vector */ );
				xml->getShellTreeData( m_vecTreeList, TRUE	/* Clear vector */ );
			}	// if (xml->load(sSuitePath))
		} // if (xml)

		if (m_pDockingFrame)
		{
			m_pDockingFrame->setTreeListVec( m_vecTreeList );
			m_pDockingFrame->updPane(m_nPaneID);
		}	// if (m_pDockingFrame)

		m_bInRecalcLayout = FALSE;
		RecalcLayout();
		AfxGetApp()->OnIdle(0);
		UpdateWindow();

	}	// if (vec.size() > 0)

	if (xml != NULL)
		delete xml;

	return TRUE;
}

BOOL CMainFrame::treeItemAddToShortcuts(LPARAM lParam)
{
	vecTreeList m_vecTreeList;
	XMLHandler *xml = new XMLHandler();
	// Get CTreeList data sent from CPADTreeCtrl; 060425 p�d
	CTreeList *data = (CTreeList *)lParam;
	if (data != NULL)
	{
		// Check user selection; only ExecItem or ReportItem can be selected; 060503 p�d

		// Added MEXEC_ITEM; 081023 p�d
		if (_tcscmp(data->getNodeName(),EXEC_ITEM) == 0 ||
				_tcscmp(data->getNodeName(),MEXEC_ITEM) == 0 ||
				_tcscmp(data->getNodeName(),REPORT_ITEM) == 0)
		{
			// Setup Shortcuts path; 060505 p�d			
			m_sSuitePath.Format(_T("%s%s"),extractFilePath(data->getSuitePath()),SHELLDATA_ShortcutsFN);
			if (fileExists(m_sSuitePath))
			{
				if (xml)
				{
					if (xml->load(m_sSuitePath))
					{
						xml->getShellTreeData(m_vecTreeList,TRUE);
						m_vecTreeList.push_back(CTreeList(*data));
						xml->setupShellDataInXMLFile(m_vecTreeList);
						xml->save(m_sSuitePath);
						xml->getShellTreeData( m_vecTreeList, TRUE	/* Clear vector */ );
					}	// if (xml->load(sSuitePath))
				}	// if (xml)
			}	// if (fileExists(sSuitePath)
		}	// if (_tcscmp(data->getNodeName(),EXEC_ITEM) == 0 ||

		if (m_pDockingFrame)
		{
			// Find PaneID for Shortcuts; 060505 p�d
			if (m_pDockingFrame->getPaneIDForSuite(SHELLDATA_ShortcutsFN,&m_nPaneID))
			{
				m_pDockingFrame->setTreeListVec( m_vecTreeList );
				m_pDockingFrame->updPane(m_nPaneID);
			}
		}	// if (m_pDockingFrame)

		m_bInRecalcLayout = FALSE;
		RecalcLayout();
		AfxGetApp()->OnIdle(0);
		UpdateWindow();

	}	// if (data != NULL)
	
	if (xml != NULL)
		delete xml;

	return TRUE;
}

void CMainFrame::setServerInfoOnStatusBar(BOOL on_line)
{
	CString S;
	CHmsUserDBRec rec;
	BOOL bIsDBOnServer;
	CString sComputerName;
	CString sDBLocation;
	CString sMsgPane1;
	CString sMsgPane2;
	CString sMsgPane3;
	TCHAR szDBLocation[127];
	TCHAR szDBServer[127];
	TCHAR szDBUser[127];
	TCHAR szDBPsw[127];
	TCHAR szDSNName[127];
	TCHAR szDBClient[127];
	TCHAR szDataBase[127];
	int nIsWindowsAuthentication;
	int nNetworkReturn;	

//	if (on_line || m_enumDATABASE_CONNECTION_STATUS == DCS_ON_LOCAL)
//	{
		// Get info. on which database server's used; 060310 p�d
		GetAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,&nIsWindowsAuthentication);
		GetUserDBInRegistry(szDataBase);
		// Reload connection to admin database; 010729 p�d
		if (m_saAdminConnection.isConnected() || !m_bAdminConnected)
		{
			m_bAdminConnected = FALSE;
			// Only disconnect if we're already connected.
			// Not if only m_bAdminConnected = FALSE (We're already OFFLINE); 070510 p�d
			if (m_saAdminConnection.isConnected())
				m_saAdminConnection.Disconnect();
			if (!m_bAdminConnected)
				m_bAdminConnected = makeAdminDBConnection(m_saAdminConnection);
			// No connection, no need to go any further;
			if (!m_bAdminConnected) 
			{
				m_enumDATABASE_CONNECTION_STATUS = DCS_NOTHING;	// No connection at all; 071221 p�d
				// Set, in registry, if we're connected to database; 080116 p�d
				setIsDBConSet(0);
				m_wndStatusBar.SetPaneText(1,getResStr(IDS_STRING40112));
				m_wndStatusBar.SetPaneText(2,getResStr(IDS_STRING172));

				return;
			}
			// Reload information on created user-databases; 081021 p�d
			BOOL bConnectedBackup = m_bConnected; // Back this up; 090227 Peter
			m_bConnected = TRUE;
			getHmsUserDB();
			m_bConnected = bConnectedBackup;
		}

		// Reload connection to selected database; 010719 p�d
		if (m_saConnection.isConnected() || !m_bConnected)
		{
			m_bConnected = FALSE;

			if (_tcslen(szDataBase) > 0)
			{
				// Only disconnect if we're already connected.
				// Not if only m_bConnected = FALSE (We're already OFFLINE); 070510 p�d
				if (m_saConnection.isConnected())
					m_saConnection.Disconnect();
				if (!m_saConnection.isConnected())
					m_bConnected = makeDBConnection(m_saConnection);
			}

			// Try to determin if the database is running on the network
			// or if it's running on the local machine; 071221 p�d
			sDBLocation = szDBLocation;
			sDBLocation.MakeLower();
			sComputerName = getNameOfComputer().MakeLower();
			if (sDBLocation.Find( (sComputerName)) > -1 ||
				  sDBLocation.Find(_T("(local)")) > -1)
			{
				bIsDBOnServer = FALSE;
			}
			else
			{
				bIsDBOnServer = TRUE;
			}

			nNetworkReturn = isNetwork();	
			// Try to determin type of connection; 071221 p�d
//			if (m_bConnected && nNetworkReturn == 1) // && bIsDBOnServer)
//			if (m_bConnected && nNetworkReturn == 1 && bIsDBOnServer)
			if (nNetworkReturn == 1 && bIsDBOnServer)
			{
				m_enumDATABASE_CONNECTION_STATUS = DCS_ON_NETWORK;
			}
			else if (nNetworkReturn != 1  && !bIsDBOnServer)
			{
				m_enumDATABASE_CONNECTION_STATUS = DCS_ON_LOCAL;
			}
			else if (nNetworkReturn != 1  && bIsDBOnServer)
			{
				m_enumDATABASE_CONNECTION_STATUS = DCS_NOTHING;	// No connection at all; 071221 p�d
				// Set, in registry, if we're connected to database; 080116 p�d
				setIsDBConSet(0);
				return;
			}
			// Check if user's on running his local db
			// or if he's on a server. If, on server,
			// disable possibility to do backups etc; 081028 p�d
			if (bIsDBOnServer)
			{
				m_bTBBackupDatabaseStatus = FALSE;
				m_bTBRestoreDatabaseStatus = FALSE;
				m_bTBCopyDatabaseStatus = FALSE;
			}
			else
			{
				m_bTBBackupDatabaseStatus = TRUE;
				m_bTBRestoreDatabaseStatus = TRUE;
				m_bTBCopyDatabaseStatus = TRUE;
			}
		}
		if ((_tcscmp(szDBLocation,_T("")) == 0 &&
				 _tcscmp(szDBServer,_T("")) == 0 &&
				 _tcscmp(szDBUser,_T("")) == 0))
		{
			sMsgPane1.Format(_T("<<%s>>"),getResStr(IDS_STRING40112));
			sMsgPane2 = _T("");
		}
		else
		{
			// Chack type of authentication; 080131 p�d
			if (nIsWindowsAuthentication == 0)		// Windows authentication
			{
				sMsgPane1.Format(_T("%s\\\\%s  (%s)"),szDBLocation,szDBServer,getResStr(IDS_STRING40115));
			}
			else if (nIsWindowsAuthentication == 1)	// Server authentication
			{
				sMsgPane1.Format(_T("%s\\\\%s@%s  (%s)"),szDBLocation,szDBServer,szDBUser,getResStr(IDS_STRING40116));
			}
			// Try to find information on selected database. I.e. DBname,DBUser,Notes; 080415 p�d
			if (m_vecHmsUserDBs.size() > 0 && _tcscmp(szDataBase,_T("")) != 0)
			{
				for (UINT i = 0;i < m_vecHmsUserDBs.size();i++)
				{
					rec = m_vecHmsUserDBs[i];

					if (rec.getDBName().CompareNoCase(szDataBase) == 0)
						break;
				}	// for (UINT i = 0;i < m_vecHmsUserDBs.size();i++)
			}	// if (m_vecHmsUserDBs.size() > 0)
			// Only show notes, if notes are avaliable; 080415 p�d
			if (m_bConnected && !rec.getNotes().IsEmpty() && rec.getDBName().CompareNoCase(szDataBase) == 0 && _tcscmp(szDataBase,_T("")) != 0)
				sMsgPane2.Format(_T("%s : %s  (%s)"),getResStr(IDS_STRING156),szDataBase,rec.getNotes());
			else if (m_bConnected && rec.getNotes().IsEmpty() && rec.getDBName().CompareNoCase(szDataBase) == 0 && _tcscmp(szDataBase,_T("")) != 0)
			{
				sMsgPane2.Format(_T("%s : %s"),getResStr(IDS_STRING156),szDataBase);
			}
			else
				sMsgPane2.Format(_T("<<%s>>"),getResStr(IDS_STRING172));
		}
//	}
//	else	// OFFLINE
//	{
/*
		m_bConnected = FALSE;
		m_bAdminConnected = FALSE;

		if (m_saConnection.isConnected())
			m_saConnection.Disconnect();

		if (m_saAdminConnection.isConnected())
				m_saAdminConnection.Disconnect();

		sMsgPane1.Format(_T("<<%s>>"),getResStr(IDS_STRING40112));
		sMsgPane2 = _T("");
*/
//	}

	// Set, in registry, if we're connected to database; 080116 p�d
	setIsDBConSet((m_bConnected ? 1 : 0));

	m_wndStatusBar.SetPaneText(1,sMsgPane1);
	m_wndStatusBar.SetPaneText(2,sMsgPane2);
}
// Handles only if we're connected to the LAN, not what kind
// of connection the database is on (Local,Network); 071221 p�d
void CMainFrame::setOnlineOfflineOnStatusBar(void)
{
	int	nNetworkReturn = isNetwork();	
	CString sMsgPane;
	// Check if we're conencted to LAN; 071221 p�d
	if (nNetworkReturn == 1)
	{
		m_enumCONN_STATUS = CONN_NETWORK;
		sMsgPane = (getResStr(IDS_STRING165));
	}
	else if (nNetworkReturn == 0)
	{
		m_enumCONN_STATUS = CONN_NO_NETWORK;
		sMsgPane = (getResStr(IDS_STRING166));
	}
	else
	{
		m_enumCONN_STATUS = CONN_NOTHING;
		sMsgPane = _T("");
	}

//	m_wndStatusBar.SetPaneText(3,sMsgPane);
}

///////////////////////////////////////////////////////////////////////////////////
// Setup ALL language specific text in HMSShell program; 051114 p�d
void CMainFrame::setLanguage(void)
{
	UINT nBarID;
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;

	// Set caption for Navigation bar; 051114 p�d
	CXTPDockingPane* pPane = GetDockingPaneManager()->FindPane(ID_PANE_NAVIGATION);
	ASSERT(pPane);
	if (pPane) 
	{
		pPane->SetTitle((getResStr(IDS_STRING132)));
	}

	m_bInRecalcLayout = TRUE;
	// Setup commandbars and manues; 051114 p�d
	CXTPCommandBars* pCommandBars = GetCommandBars();
	for (int i = 0; i < pCommandBars->GetCount(); i++)
	{
		CXTPToolBar* pToolBar = pCommandBars->GetAt(i);
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() == xtpBarTypeNormal)
			{

				nBarID = pToolBar->GetBarID();
				pToolBar->SetTitle((getResStr(nBarID)));
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_MAINFRAME)
				{		
					// OBS! Use LoadLibraryEx instead of just LoadLibrary. Otherwise it won't work
					// on Windows 2000; 060731 p�d
					// changed how to load the icons from the resource. (091012 ag)
					pCtrl = p->GetAt(0);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 3);	// RSTR_TB_NEW
					if (hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(1);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 12);	// RSTR_TB_FOLDER
					if (hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(2);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 35);	// RSTR_TB_SAVE
					if (hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(3);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 22);	// RSTR_TB_WASTEBIN
					if (hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(4);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 31);	// RSTR_TB_PREVIEW
					if (hIcon) pCtrl->SetCustomIcon(hIcon);

					/*pCtrl = p->GetAt(5); // #4493 20151013 J�
					hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 32);	// RSTR_TB_HELP
					if (hIcon) pCtrl->SetCustomIcon(hIcon);*/
				}	// if (nBarID == IDR_MAINFRAME)
				else if (nBarID == IDR_TOOLBAR_DBNAVIG)
				{
					// Setup imagelists, based on ResourceDLL; 051207 p�d
					// changed how to load the icons from the resource. (091012 ag)
					pCtrl = p->GetAt(0);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 11);	// RSTR_TB_FIRST
					if (hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(1);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 25);	// RSTR_TB_PREV
					if (hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(2);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 34);	// RSTR_TB_NEXT
					if (hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(3);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 24);	// RSTR_TB_LAST
					if (hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(4);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 26);	// RSTR_TB_LIST
					if (hIcon) pCtrl->SetCustomIcon(hIcon);
				}	// else if (nBarID == IDR_TOOLBAR_DBNAVIG)
			}	// if (pToolBar->GetType() == xtpBarTypeNormal)
			else if (pToolBar->GetType() == xtpBarTypeMenuBar)
			{ 
				CMenu menu;
				int nDBSet = getDBQualified();
				// Set if user's qualified to change settings in database; 061207 p�d
				m_bIsUMDatabase = (nDBSet == 1 ? TRUE : FALSE);
				XTPResourceManager()->LoadMenu(&menu, IDR_MAINFRAME);
				pToolBar->LoadMenu(&menu);
				pToolBar->GetControls()->CreateOriginalControls();


				((CXTPMenuBar*)pToolBar)->RefreshMenu();

			}
		}	// if (pToolBar->IsBuiltIn())
	}	// for (int i = 0; i < pCommandBars->GetCount(); i++)


	pCommandBars->UpdateCommandBars();

	if (!bIsFirstTime)
	{
		if (m_pDockingFrame != 0)
		{
			m_pDockingFrame->setTreeListVec( m_vecShellData );
			// Added 081205 p�d
			getPanesHiddenOrVisible();
			m_pDockingFrame->addPanes(-1);
			m_pDockingFrame->setVisiblePanes(m_vecVisibleItemsOnNavPane);
		}
	}

	// Setup Language filenames, by adding language abbrevation
	// and extension; 060811 p�d
	m_sLangSet = getLangSet();
	CString sLangFN;
	CString sLangFN1,S;
	for (UINT i = 0;i < m_vecIndexTable.size();i++)
	{
		if (bIsFirstTime)
		{
			sLangFN.Format(_T("%s%s%s"),m_vecIndexTable[i].szLanguageFN,m_sLangSet,LANGUAGE_FN_EXT);
			// Check if Language filename exists. I.e. there's a translation
			// to this language; 080328 p�d
			if (!fileExists(sLangFN))
			{
				sLangFN.Format(_T("%s%s%s"), m_vecIndexTable[i].szLanguageFN, _T("ENU"), LANGUAGE_FN_EXT);
			}
		}
		else
		{
			sLangFN1 = extractFileNameNoExt(m_vecIndexTable[i].szLanguageFN);
			sLangFN.Format(_T("%s%s%s%s"),

			extractFilePath(m_vecIndexTable[i].szLanguageFN),
											sLangFN1.Left(sLangFN1.GetLength() - m_sLangSet.GetLength()),
											m_sLangSet,
											LANGUAGE_FN_EXT);
			// Check if Language filename exists. I.e. there's a translation
			// to this language; 080328 p�d
			if (!fileExists(sLangFN))
			{
				sLangFN.Format(_T("%s%s%s%s"),
					extractFilePath(m_vecIndexTable[i].szLanguageFN),
					sLangFN1.Left(sLangFN1.GetLength() - m_sLangSet.GetLength()),
					_T("ENU"),	// Default language 
					LANGUAGE_FN_EXT);
			}
		}
		_tcscpy(m_vecIndexTable[i].szLanguageFN,sLangFN);
	}

	bIsFirstTime = FALSE;

	m_bInRecalcLayout = FALSE;
	RecalcLayout();
	AfxGetApp()->OnIdle(0);
	UpdateWindow();
}

BOOL CMainFrame::isPermanentCategory(LPCTSTR category)
{
	int nItems,nLen;
	splitInfo(m_szPermanentCategories,&nItems,&nLen,';');

	for (int i = 0;i < nItems;i++)
	{
		if (_tcscmp(category,split(m_szPermanentCategories,i,';')) == 0)
			return TRUE;
	}	// for (int i = 0;i < nSize;i++)
	return FALSE;
}

BOOL CMainFrame::setupDBConnection(CSplashDialog& dlg)
{
	CString sMsg;
	TCHAR szDBLocation[127];
	CString sDBLocation;
	TCHAR szDBServer[127];
	TCHAR szDBUser[127];
	TCHAR szDBPsw[127];
	TCHAR szDSNName[127];
	TCHAR szDBClient[127];
	int nIsWindowsAuthentication;
	// Get info. on which database server's used; 060310 p�d
	GetAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,&nIsWindowsAuthentication);
	sDBLocation = szDBLocation;
	sDBLocation = sDBLocation.MakeLower();

		// Check for type of authentication; 080131 p�d
		if (nIsWindowsAuthentication == 0)
		{
			sMsg.Format(_T("%s: %s\\\\%s (%s)"),getResStr(IDS_STRING40113),sDBLocation,szDBServer,getResStr(IDS_STRING40115));
		}
		else if (nIsWindowsAuthentication == 1)
		{
			sMsg.Format(_T("%s: %s\\\\%s@%s (%s)"),getResStr(IDS_STRING40113),sDBLocation,szDBServer,szDBUser,getResStr(IDS_STRING40116));
		}

	dlg.setSplashInfoText(sMsg);

	setServerInfoOnStatusBar(FALSE);

	dlg.setSplashInfoText(_T(""));
	return TRUE;
}

void CMainFrame::doAdminSetup(void)
{
	executeShellTreeItem(UMDATABASE_MODULE_ID1,_T("OpenSuite"),_T("Administration.dll"));
}


void CMainFrame::doDatabaseBackup(void)
{
	// Read Databases (Companies), in hms_administrator database; 081002 p�d
	getHmsUserDB();

	CDatabaseBackupDlg *pDlg = new CDatabaseBackupDlg(BACKUP_DB);
	if (pDlg != NULL)
	{
		pDlg->setHMSDBs(m_vecHmsUserDBs);
		pDlg->DoModal();

		delete pDlg;
	}	// if (pDlg != NULL)
}

void CMainFrame::doDatabaseRestore(void)
{
	// Read Databases (Companies), in hms_administrator database; 081002 p�d
	getHmsUserDB();

	DB_CONNECTION_DATA data;
	data.conn = &m_saConnection;
	data.admin_conn = &m_saAdminConnection;
	CDBAdmin *pDBAdmin = new CDBAdmin(data);

	CDatabaseBackupDlg *pDlg = new CDatabaseBackupDlg(RESTORE_DB);
	if (pDlg != NULL)
	{
		pDlg->setHMSDBs(m_vecHmsUserDBs);
		if (pDBAdmin != NULL)
		{
			pDlg->setHMSDBAdmin(pDBAdmin);
		}	// if (pDB != NULL)
		pDlg->DoModal();

		delete pDlg;
	}	// if (pDlg != NULL)
	if (pDBAdmin)
		delete pDBAdmin;
}

void CMainFrame::doDatabaseCopy(void)
{
	// Read Databases (Companies), in hms_administrator database; 081006 p�d
	getHmsUserDB();

	CDatabaseCopyDlg *pDlg = new CDatabaseCopyDlg();
	if (pDlg != NULL)
	{
		pDlg->setHMSDBs(m_vecHmsUserDBs);
		pDlg->DoModal();

		delete pDlg;
	}	// if (pDlg != NULL)

}


void CMainFrame::checkIfThereIsADatabaseAdmin(void)
{
	if (m_bConnected)
	{
		if (setupAdminDatabase() == -1)	// Had to create admin database; 080507 p�d
		{
			WriteUserDBToRegistry(_T(""));

			setServerInfoOnStatusBar(m_bConnected);
		}
		getHmsUserDB();
	}

}

void CMainFrame::getHmsUserDB(void)
{
	// Collect data from hms_administrator table; 080414 p�d
	if (m_bConnected)
	{
		DB_CONNECTION_DATA data;
		data.conn = &m_saConnection;
		data.admin_conn = &m_saAdminConnection;
		CDBAdmin *pDBAdmin = new CDBAdmin(data);
		if (pDBAdmin != NULL)
		{
			m_vecHmsUserDBs.clear();
			pDBAdmin->userdb_Get(m_vecHmsUserDBs);
			delete pDBAdmin;
		}	// if (pDB != NULL)
	}	// if (m_bConnected)
}

void CMainFrame::runDoDatabaseTablesInSuitesModules(void)
{
	CStringArray arrSuitesAndModules;
	getSuites(arrSuitesAndModules,FALSE);
	getModules(arrSuitesAndModules,FALSE);

	if (arrSuitesAndModules.GetCount() == 0)
		return;

	HINSTANCE hModule = NULL;
	typedef CRuntimeClass *(*Func)(LPCTSTR);
  Func proc;
	// Check if there's any ShellData files; 080421 p�d
	if (arrSuitesAndModules.GetCount() > 0)
	{
		for (int i = 0;i < arrSuitesAndModules.GetCount();i++)
		{
			hModule = AfxLoadLibrary(arrSuitesAndModules[i]);
			if (hModule != NULL)
			{
				proc = (Func)GetProcAddress((HMODULE)hModule, EXPORTED_DO_DATABASE_TABLES );
				if (proc != NULL)
				{
					// call the function
					proc(_T("")); // Empty arg = use deafult database; 081001 p�d
				}	// if (proc != NULL)
				AfxFreeLibrary(hModule);
			}	// if (hModule != NULL)
		}	// for (int i = 0;i < arr.GetCount();i++)
	}	// if (arr.GetCount() > 0)
}

void CMainFrame::getPanesHiddenOrVisible(void)
{
	int nItems = m_pDockingFrame->m_wndNavigationBar.GetItemCount();
	CXTPShortcutBarItem *pItem = NULL;
	m_vecVisibleItemsOnNavPane.clear();

	for (int i = 0;i < nItems;i++)
	{
		pItem = m_pDockingFrame->m_wndNavigationBar.GetItem(i);

		if (pItem != NULL)
		{
			if (pItem->GetID() >= 0)
			{
				m_vecVisibleItemsOnNavPane.push_back(CVisibleItemsOnNavPane(pItem->GetCaption(),pItem->IsVisible()));
			}	// if (pItem->GetID() >= 0)
		}	// if (pItem != NULL)
	}	// for (int i = 0;i < nItems;i++)
}
// PROTECTED METHOD; 060802 p�d
void CMainFrame::newsBulletines(void)
{
	BOOL bFound,bIsDirty;

	// Vector holding News bulletins; 060802 p�d
	vecNewsCategoriesSetup vecNewsCategories;
	vecNewsCategoriesSetup vecNewsCategories_tmp;

	UINT i,j;
	long lSize;
	CStringArray strarrBuffer;
	TCHAR szThisPubDate[128];
	TCHAR szLastPubDate[128];
	CString sCatOnWEB,sCatOnDisk;
	RSSXMLFilePars fpars;

	memset(szThisPubDate, 0, _tcslen(szThisPubDate) * sizeof(TCHAR));
	memset(szLastPubDate, 0, _tcslen(szLastPubDate) * sizeof(TCHAR));

	// Handle NewsCategories setup in binary file; 060809 p�d
	// getNewsCatergoriesSetupInfo in HMSFuncLib; 060809 p�d
	getNewsCatergoriesSetupInfo(vecNewsCategories);

	if (getRSSXMLFile(strarrBuffer,&lSize))
	{
		if (fpars.LoadFromBuffer(*strarrBuffer.GetData()))
		{
			getRSSFileLastPubDateInReg(szLastPubDate);
			fpars.getThisPubDate(szThisPubDate);

			BOOL bNewNews = FALSE;

			// Check last pub date; 090608 Peter
			try
			{
				System::DateTime ^lastPubDate, ^thisPubDate;
				System::String ^strLastPubDate = gcnew System::String(szLastPubDate);
				if(System::String::IsNullOrEmpty(strLastPubDate) != true)
				{
					System::String ^strThisPubDate = gcnew System::String(szThisPubDate);
					lastPubDate = System::Convert::ToDateTime(strLastPubDate);
					thisPubDate = System::Convert::ToDateTime(strThisPubDate);
					if( System::DateTime::Compare(*thisPubDate, *lastPubDate) > 0 )
					{
						// This is a new record
						bNewNews = TRUE;
					}
				}
				else
				{
					bNewNews = TRUE;
				}
			}
			catch( System::Exception ^e )
			{
				AfxMessageBox( CString(e->Message) );
				bNewNews = TRUE; // Probably caused by empty registry value
			}

			m_vecNewsBulletines.clear();
			fpars.getNewsBulletines(m_vecNewsBulletines, vecNewsCategories);


			// Get permanent categories, set in registry; 060809 p�d
			getPermanentCategoriesInReg(m_szPermanentCategories);

			// Compare categories in vecNewsBulletine (i.e. file on Web), to
			// vecNewsCategories setup; 060809 p�d
			for (i = 0;i < m_vecNewsBulletines.size();i++)
			{
				if (!isPermanentCategory(m_vecNewsBulletines[i].getCategory()))
				{
					vecNewsCategories_tmp.push_back(CNewsCategoriesSetupItem(1,m_vecNewsBulletines[i].getCategory()));
				}	// if (!isPermanentCategory(vecNewsBulletines[i].getCategory()))
			}

			// Compare vecNewsCategories_tmp = categories on WEB xml-file to
			// vecNewsCategories, saved on disk; 060810 p�d
			bIsDirty = FALSE;
			for (i = 0;i < vecNewsCategories_tmp.size();i++)
			{
				bFound = FALSE;
				sCatOnWEB = vecNewsCategories_tmp[i].getCategory();
				for (j = 0;j < vecNewsCategories.size();j++)
				{
					sCatOnDisk = vecNewsCategories[j].getCategory();
					if (_tcscmp(sCatOnWEB,sCatOnDisk) == 0)
					{
						bFound = TRUE;
						break;
					}	// if (sCatOnWEB.Compare(sCatOnDisk) == 0)
				}	// for (j = 0;j < vecNewsCategories.size();j++)

				if (!bFound)
				{
					vecNewsCategories.push_back(CNewsCategoriesSetupItem(1,sCatOnWEB));
					bIsDirty = TRUE;
				}	// if (!bFound)

			}	// for (i = 0;i < vecNewsCategories_tmp.size();i++)

			// Save News category setup file; 060809 p�d
			// Only if there's something to save; 060810 p�d
			if (bIsDirty)
			{
				saveNewsCatergoriesSetupInfo(vecNewsCategories);
			}


			// show the popup if there are new news, or we have to
			if(m_vecNewsBulletines.size() > 0 && (bNewNews == TRUE || m_bShowNews == TRUE)) setupNewsPopup();

			setRSSFileLastPubDateInReg(szThisPubDate);
		}
	}
}

void CMainFrame::setupNewsPopup(void)
{
	m_pActivePopup = new CXTPPopupControl();

	UpdateData();

	m_pActivePopup->SetTheme(xtpPopupThemeMSN);

	m_pActivePopup->SetPopupAnimation((XTPPopupAnimation)m_nAnimation);
	m_pActivePopup->SetShowDelay(m_uShowDelay);
	m_pActivePopup->SetAnimateDelay(m_uAnimDelay);
	m_pActivePopup->SetTransparency(255);

	m_sizeNewsPopup.cy = 40 + (m_vecNewsBulletines.size() * 20);
	m_pActivePopup->SetPopupSize(m_sizeNewsPopup);
	
	m_pActivePopup->AllowMove(m_bAllowMove);

	m_pActivePopup->Show(this);

	m_lstPopupControl.AddTail(m_pActivePopup);
	enableItems(FALSE);

	showNewItemInPopup();
}

void CMainFrame::showNewItemInPopup()
{
	CXTPPopupItem* pItemText = NULL;
	CXTPPopupItem* pItemIcon = NULL;

	CSize szPopup = m_sizeNewsPopup;
	m_pActivePopup->RemoveAllItems();

	// close icon.
	pItemIcon = (CXTPPopupItem*)m_pActivePopup->AddItem( new CXTPPopupItem(CRect(szPopup.cx - 26, 5, szPopup.cx - 10, 21)) );
	pItemIcon->SetIcon((HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON5), IMAGE_ICON, 16, 16, 0));
	pItemIcon->SetButton(TRUE);
	pItemIcon->SetID(ID_POPUP_CLOSE);

	// Title
	CString csBuf;
	csBuf.Format(_T("%s - %s"), getResStr(100), getResStr(11011));
	pItemText = (CXTPPopupItem*)m_pActivePopup->AddItem( new CXTPPopupItem(CRect(10, 5, szPopup.cx - 50, 30), csBuf) );
	pItemText->SetBold(TRUE);
	pItemText->SetHyperLink(FALSE);

	// Description
	for (int i = 0; i < m_vecNewsBulletines.size(); i++)
	{
		pItemText = (CXTPPopupItem*)m_pActivePopup->AddItem( new CXTPPopupItem(CRect(10, 35 + (20 * i), szPopup.cx - 10, 60 + (20 * i) ),  m_vecNewsBulletines[i].getTitle()) );
		pItemText->SetHyperLink(TRUE);
		pItemText->SetID(ID_GOTO_SITE + i);
		pItemText->SetTextColor(RGB(0, 61, 178));
		pItemText->SetTextAlignment(DT_LEFT|DT_WORDBREAK);
	}

	m_pActivePopup->RedrawControl();
}


void CMainFrame::closeChildWindows(BOOL close_all)
{
	// Check if there's any child-windows open. If so, close them; 090316 p�d
	BOOL bCloseWindow = TRUE;
	CString sText;
	CMDIChildWnd *pWnd = NULL;
	CDocument *pDoc = NULL;
	CDocTemplate *pDocTmpl = NULL;
	// Go to Next Child-window, we assume that the first window
	// is the UMDatabase change database window; 090316 p�d
	this->MDINext();
	do
	{
		pWnd = this->MDIGetActive();
		if (pWnd != NULL)
		{
			pDoc = pWnd->GetActiveDocument();
			if (pDoc != NULL)
			{
				pDocTmpl = pDoc->GetDocTemplate();
				if (pDocTmpl != NULL)
				{
					pDocTmpl->GetDocString(sText,CDocTemplate::docName);
					if (sText.CompareNoCase(_T("Module601")) == 0 && !close_all)
					{
						bCloseWindow = FALSE;
						break;
					}	// if (sText.CompareNoCase(_T("Module601")) && !close_all)
					else
						bCloseWindow = TRUE;
				}	// if (pDocTmpl != NULL)
			}	// if (pDoc != NULL)
			if (bCloseWindow) pWnd->DestroyWindow();
		}
	} while (pWnd != NULL);
}

BOOL CMainFrame::areThereChildWindows(void)
{
	CMDIChildWnd *pWnd = this->MDIGetActive();
	return (pWnd != NULL);
}

void CMainFrame::enableItems(BOOL bEnable)
{
	CWnd* pWnd = GetWindow(GW_CHILD);
	while (pWnd)
	{
		pWnd->EnableWindow(bEnable);
		pWnd = pWnd->GetWindow(GW_HWNDNEXT);
	}

}

// PUBLIC METHOD
void CMainFrame::setHInstVec(HINSTANCE_VEC &vec)
{
	m_vecHInst = vec;
}

// PUBLIC METHOD
void CMainFrame::setTreeListVec(vecTreeList &v)
{
	m_vecShellData = v;
}

// PUBLIC METHOD
void CMainFrame::setIndexTableVec(vecINDEX_TABLE &v)
{
	m_vecIndexTable = v;
}

void CMainFrame::setInfoTableVec(vecINFO_TABLE &v)
{
	m_vecInfoTable = v;
}

//#pragma managed(push, on)
