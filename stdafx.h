	// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0600		// Target Windows Vista
#define _WIN32_WINNT 0x0600

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxhtml.h>						// Per tal de poder utilitzar HTML.

#include <vector>
#include <map>

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#include <XTToolkitPro.h>   // Codejock Software Components

#include <SQLAPI.h>

#include <shfolder.h>
#include <shlobj.h>

#include "Misc.h"
#include "XMLHandler.h"

#include "pad_hms_miscfunc.h"
#include "pad_xmlshelldatahandler.h"

#include <Sensapi.h>
#pragma comment(lib, "Sensapi.lib")

#include "w3c.h"

class CSuiteModuleInfoParser
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;
protected:
	MSXML2::IXMLDOMNodePtr addItem(MSXML2::IXMLDOMNodePtr,LPCTSTR);
public:
	CSuiteModuleInfoParser(void);

	virtual ~CSuiteModuleInfoParser();

	BOOL LoadFromFile(LPCTSTR);
	BOOL LoadFromBuffer(LPCTSTR);
	BOOL SaveToFile(LPCTSTR);

	BOOL CreateDoc(void);

	BOOL setSuiteModuleInfo(vecINFO_TABLE &);

	BOOL getXML(CString &xml);

};

///////////////////////////////////////////////////////////////////////////////
// DEFINES AND CONSTANTS

#define WM_USER_MSG_TREELIST				(WM_USER + 1)
#define WM_USER_MSG_SUITE					(WM_USER + 2)		// Used for messages sent from a Suite/usermodule

#define USER_MSG_EXECUTE_SHELL				(WM_USER + 4)		// This is a identifer used when communication from a

#define CMDID_CHANGE_LANGUAGE				0x8011				// OnCommand, change language
#define CMDID_CHANGE_SERVER					0x8012				// OnCommand, change server
#define CMDID_SPLASH_INFO					0x8013				// OnCommand, change server

#define ID_TOGGLE_PANE						0x8014
#define ID_TOGGLE_ONLINE_OFFLINE			0x8015

#define ID_TIMER							0x8016
#define ID_TIMER2							0x8017

// Add stringsvalues matching languagefile HMSShell....xml
#define IDS_STRING131                   131
#define IDS_STRING132                   132
#define IDS_STRING133                   133
#define IDS_STRING134                   134
#define IDS_STRING136                   136

#define IDS_STRING144                   144

#define IDS_STRING145                   145
#define IDS_STRING146                   146
#define IDS_STRING147                   147

#define IDS_STRING148                   148
#define IDS_STRING149                   149
#define IDS_STRING150                   150

#define IDS_STRING151                   151
#define IDS_STRING152                   152
#define IDS_STRING153                   153
#define IDS_STRING154                   154
#define IDS_STRING155                   155

#define IDS_STRING156                   156

#define IDS_STRING157                   157

#define IDS_STRING158                   158
#define IDS_STRING159                   159
#define IDS_STRING160                   160
#define IDS_STRING161                   161
#define IDS_STRING162                   162
#define IDS_STRING163                   163

#define IDS_STRING164                   164

#define IDS_STRING165                   165
#define IDS_STRING166                   166

#define IDS_STRING167                   167
#define IDS_STRING168                   168

#define IDS_STRING169                   169
#define IDS_STRING170                   170
#define IDS_STRING171                   171

#define IDS_STRING172                   172

#define IDS_STRING173                   173
#define IDS_STRING174                   174
#define IDS_STRING175                   175
#define IDS_STRING176                   176
#define IDS_STRING177                   177
#define IDS_STRING178                   178

#define IDS_STRING183                   183
#define IDS_STRING184                   184
#define IDS_STRING185                   185
#define IDS_STRING186                   186
#define IDS_STRING187                   187
#define IDS_STRING188                   188

#define IDS_STRING189                   189
#define IDS_STRING190                   190

#define IDS_STRING191					191

#define IDS_STRING192					192
#define IDS_STRING193					193
#define IDS_STRING194					194
#define IDS_STRING195					195

// webnews
#define IDS_STRING196					196
#define IDS_STRING197					197

#define IDS_STRING198					198


#define IDS_STRING900                   900
#define IDS_STRING901                   901
#define IDS_STRING902                   902
#define IDS_STRING903                   903
#define IDS_STRING904                   904
#define IDS_STRING911					911


#define ID_CHANGE_LANGUAGE							40000		// Change language, also string value
#define IDS_STRING40001								40001		// String in language dialog
#define IDS_STRING40002								40002		// String in language dialog
#define IDS_STRING40003								40003		// String in language dialog

#define ID_ADMINISTRATION							40100		// Setup administration information
#define IDS_STRING40101								40101		// String in adminsetup dialog
#define IDS_STRING40102								40102		// String in adminsetup dialog
#define IDS_STRING40103								40103		// String in adminsetup dialog
#define IDS_STRING40104								40104		// String in adminsetup dialog
#define IDS_STRING40105								40105		// String in adminsetup dialog
#define IDS_STRING40106								40106		// String in adminsetup dialog
#define IDS_STRING40107								40107		// String in adminsetup dialog
#define IDS_STRING40108								40108		// String in adminsetup dialog
#define IDS_STRING40109								40109		// String in adminsetup dialog
#define IDS_STRING40110								40110		// String in adminsetup dialog
#define IDS_STRING40111								40111		// String in adminsetup dialog
#define IDS_STRING40112								40112		// String in adminsetup dialog
#define IDS_STRING40113								40113		// String in adminsetup dialog
#define IDS_STRING40114								40114		// String in adminsetup dialog
#define IDS_STRING40115								40115		// String in adminsetup dialog
#define IDS_STRING40116								40116		// String in adminsetup dialog
#define IDS_STRING40117								40117		// String in adminsetup dialog
#define IDS_STRING40118								40118		// String in adminsetup dialog
#define IDS_STRING40119								40119		// String in adminsetup dialog
#define IDS_STRING40120								40120		// String in adminsetup dialog
#define IDS_STRING40121								40121		// String in adminsetup dialog
#define IDS_STRING40122								40122		// String in adminsetup dialog

#define IDS_STRING40150								40150		// String in dbbackup dialog
#define IDS_STRING40151								40151		// String in dbbackup dialog
#define IDS_STRING40152								40152		// String in dbbackup dialog
#define IDS_STRING40153								40153		// String in dbbackup dialog
#define IDS_STRING40154								40154		// String in dbbackup dialog
#define IDS_STRING40155								40155		// String in dbbackup dialog
#define IDS_STRING40156								40156		// String in dbbackup dialog
#define IDS_STRING40157								40157		// String in dbbackup dialog
#define IDS_STRING40158								40158		// String in dbbackup dialog
#define IDS_STRING40159								40159		// String in dbbackup dialog
#define IDS_STRING40160								40160		// String in dbbackup dialog
#define IDS_STRING40161								40161		// String in dbbackup dialog
#define IDS_STRING40162								40162		// String in dbbackup dialog
#define IDS_STRING40163								40163		// String in dbbackup dialog
#define IDS_STRING40164								40164		// String in dbbackup dialog
#define IDS_STRING40165								40165		// String in dbbackup dialog
#define IDS_STRING40166								40166		// String in dbbackup dialog
#define IDS_STRING40167								40167		// String in dbbackup dialog
#define IDS_STRING40168								40168		// String in dbbackup dialog
#define IDS_STRING40169								40169		// String in dbbackup dialog
#define IDS_STRING40170								40170		// String in dbbackup dialog
#define IDS_STRING40171								40171		// String in dbbackup dialog
#define IDS_STRING40172								40172		// String in dbbackup dialog
#define IDS_STRING40173								40173		// String in dbbackup dialog
#define IDS_STRING40174								40174		// String in dbbackup dialog
#define IDS_STRING40175								40175		// String in dbbackup dialog
#define IDS_STRING40176								40176		// String in dbbackup dialog
#define IDS_STRING40177								40177		// String in dbbackup dialog
#define IDS_STRING40178								40178		// String in dbbackup dialog
#define IDS_STRING40179								40179		// String in dbbackup dialog
#define IDS_STRING40180								40180		// String in dbbackup dialog
#define IDS_STRING40181								40181		// String in dbbackup dialog
#define IDS_STRING40182								40182		// String in dbbackup dialog
#define IDS_STRING40184								40184		// String in dbbackup dialog
#define IDS_STRING40185								40185		// String in dbbackup dialog
#define IDS_STRING40186								40186		// String in dbbackup dialog

#define ID_BACKUP_DATABASE							40200		// Do backups; 081001 p�d
#define ID_RESTORE_DATABASE							40201		// Restore backup for database; 081002 p�d
#define ID_COPY_DATABASE							40202		// Copy a database to another; 081002 p�d

#define ID_RESTORE_LAYOUT							40203		// Restore window layout, also string value

#define ID_REPORT_CTRL								0x8013

#define ID_POPUP_HIDE_CMD							0x8014

#define ID_TREELIST_REPORT							0x8015

#define ID_STSTUSBAR_IDICATOR						0x8001

// Defines for News popup control items; 061030 p�d
#define ID_POPUP_CLOSE								0x8100
#define ID_POPUP_PREV								0x8101
#define ID_POPUP_NEXT								0x8102
#define ID_GOTO_SITE								0x8103

#define ID_BACKUP_REPORT							0x8200

enum 
{
	COLUMN_0,
	COLUMN_1,
	COLUMN_2,
	COLUMN_3,
	COLUMN_4
};


///////////////////////////////////////////////////////////////////////////////
//	Resource names for icons in resource dll (HMSToolBarIcones32.dll); 051209 p�d

const LPCTSTR TOOLBAR_RES_DLL					= _T("HMSIcons.icl");		// Resource dll, holds icons for e.g. toolbars; (0901012 ag)

const LPCTSTR RSTR_TB_NEW						= _T("Blanktdock");
const LPCTSTR RSTR_TB_FOLDER					= _T("Folder");
const LPCTSTR RSTR_TB_SAVE						= _T("Save");
const LPCTSTR RSTR_TB_WASTEBIN					= _T("Kasta");
const LPCTSTR RSTR_TB_PREVIEW					= _T("Preview");
const LPCTSTR RSTR_TB_HELP						= _T("Quest");

const LPCTSTR RSTR_TB_FIRST						= _T("First");
const LPCTSTR RSTR_TB_NEXT						= _T("Left");
const LPCTSTR RSTR_TB_PREV						= _T("Right");
const LPCTSTR RSTR_TB_LAST						= _T("Last");
const LPCTSTR RSTR_TB_LIST						= _T("Lista");

const LPCTSTR RESOURCE_DLL						= _T("HMSIcons.icl");		// Resource dll, holds icons for e.g. toolbars; (091012 ag)

const LPCTSTR RSTR_TREE_FOLDER_CLOSED			= _T("Treefolderclosed_16x16");
const LPCTSTR RSTR_TREE_FOLDER_OPEN				= _T("Treefolderopen_16x16");
const LPCTSTR RSTR_TREE_FORM					= _T("Treeform_16x16");
const LPCTSTR RSTR_TREE_REPORT					= _T("Treereport_16x16");
const LPCTSTR RSTR_TREE_HELP					= _T("Treehelp_16x16");

// ID number for Icons in HMSIcons32.dll

#define ID_TREECTRL_FOLDER_ITEM		0
#define ID_TREECTRL_MEXEC_ITEM		2
#define ID_TREECTRL_EXEC_ITEM		2
#define ID_TREECTRL_REPORT_ITEM		3
#define ID_TREECTRL_HELP_ITEM		4
#define ID_TREECTRL_MHELP_ITEM		4

///////////////////////////////////////////////////////////////////////////////
//	Misc. constant values; 051114 p�d

const LPCTSTR HMS_SHELL_REGISTRY_KEY			= _T("HaglofManagmentSystem");

const LPCTSTR COMMANDBAR_SETUP					= _T("HMS_CommandBars");
const LPCTSTR NAVIGAITONBARS_SETUP				= _T("HMS_NavigationBars");
const LPCTSTR RUNLAYOUT_SETUP					= _T("HMS_RunLayout");

const LPCTSTR PROGRAM_NAME						= _T("HMSShell");					// Used for Languagefile, registry entries etc.; 051114 p�d


const LPCTSTR SETUP_SHELLDATA_DIR				= _T("Shelldata");

///////////////////////////////////////////////////////////////////////////////
// Database ststus on Network enums; 071221 p�d
// Set if database is local or on a network.
// Criteria:
// If we have a network connection and a valid connection to db
// we can assume that we're on a network (LAN,WAN)
// If we don't have a connection to a network, but we still have
// a valid db connection, we can assume we're running a local DB; 071221 p�d
typedef enum  { DCS_ON_NETWORK,
							  DCS_ON_LOCAL,
								DCS_NOTHING } DATABASE_CONNECTION_STATUS;

// Enumerated type for if we're connected to the network or not; 071221 p�d
typedef enum  { CONN_NETWORK,
							  CONN_NO_NETWORK,
								CONN_NOTHING } CONN_STATUS;

///////////////////////////////////////////////////////////////////////////////
// Colors used in e.g. CMyExtEdit
#define BLACK      RGB(  0,  0,  0)
#define WHITE      RGB(255,255,255)
#define INFOBK	   ::GetSysColor(COLOR_INFOBK)

///////////////////////////////////////////////////////////////////////////////
// Suite handling constants

const LPCSTR INIT_SUITE_FUNCTION		= "InitSuite";
const LPCSTR OPEN_SUITE_FUNCTION		= "OpenSuite";
const LPCSTR OPEN_SUITE_FUNCTION1		= "OpenSuiteArg";


///////////////////////////////////////////////////////////////////////////////
// UserModules and/or Suites with Exported function, used in AfxLoadLibrary; 060303 p�d

const LPCTSTR UMDataBase				= _T("UMDataBase.dll");	// Databasae handling functions, classes etc.; 060303 p�d

///////////////////////////////////////////////////////////////////////////////
// define index for databasemodule, change database etc; 090319 p�d
#define		UMDATABASE_MODULE_ID1		601		// ID for change database window; 090319 p�d


///////////////////////////////////////////////////////////////////////////////
// Functions

int isNetwork(void);

int InsertRow(CListCtrl &ctrl,int nPos,int nNoOfCols, LPCTSTR pText, ...);
int GetSelectedItem(CListCtrl &ctrl);
void DeleteItems(CListCtrl &ctrl);

CView *showFormView(int idd, LPCTSTR lang_fn, LPARAM lp = -1);
