
#ifndef _MISC_H_
#define _MISC_H_

#include "XMLHandler.h"
#include "InputDlg.h"
#include <vector>


// Buffer
#define BUFFER_SIZE		1024*500		// 500 kb buffer

const LPCTSTR REG_ROOT_HMSSHELL				= _T("Software\\HaglofManagmentSystem\\HMSShell");

// Use this root key in my own registry settings; 060113 p�d
const LPCTSTR REG_TOOLBAR_KEY				= _T("HMS_TOOLBAR");
const LPCTSTR REG_TOOLBAR_HEIGHT			= _T("TBHeight");	// Height of toolbar; 060113 p�d

// Use this root key in my own registry settings; 060113 p�d
const LPCTSTR REG_PANESELECTED_KEY			= _T("HMS_PANE_SELECTED");
const LPCTSTR REG_PANESELECTED				= _T("PaneSelected");		// Last selected Suite in navbar
const LPCTSTR REG_PANESELECTED_ID			= _T("PaneSelectedID");	// Last selected Suite in navbar

const LPCTSTR LANGUAGE_FN_WC				= _T("HMSShell*.xml");			// Wildcard for HMSShellxxx.xml language file()s; 051115 p�d
const LPCTSTR SUITES_FN_WC					= _T("*.dll");			// Wildcard for Suites files; 051115 p�d
const LPCTSTR MODULES_FN_WC					= _T("*.dll");			// Wildcard for Module files; 051128 p�d
const LPCTSTR SUITES_FN_EXT					= _T(".dll");				// Extansion for suites (also for usermodules etc.)
const LPCTSTR HELPFILE_EXT					= _T(".chm");

const LPCTSTR SUBDIR_HELP					= _T("Help");		// All helpfiles are in a subdir of the Suites directory; 060804 p�d
const LPCTSTR SETUP_DIR						= _T("Setup");	// All original files etc, in this dir; 090616 p�d

const LPCTSTR SHELLDATA_FN_EXT				= _T(".xml");				// Uses ordinary xm-file
//const LPCTSTR SHELLDATA_FN_WC				= "*.xml";			// Wildcard for xml files; 051115 p�d

// Exported function in Suite/Module, to create Tables in Active database; 080421 p�d
const LPCSTR EXPORTED_DO_DATABASE_TABLES	= "DoDatabaseTables";		

const LPCTSTR HMS_ADMIN_TABLE				= _T("Create_admin_db_table.sql");
//const LPCTSTR HMS_USER_DB_TABLE_NAME		= _T("HMS_USER_DB");	// Name of table in database; 060228 p�d
const LPCTSTR HMS_SQL_SERVER_DIR			= _T("Microsoft SQL Server");

const LPCTSTR KEGFILE_EXT			= _T(".keg");	//KORUS Egenuppf�ljning file type, will call the UMKORUS module. 120508 Mathias

// Structrue for Sending/Reciving SQLApi++ object connmection data
// from HMSShell to Suit/User module, using WM_COPYDATA; 010719 p�d
// This struct is also in DBTransaction_lib
/*
typedef struct _db_connection_data
{
	SAConnection* conn;
	SAConnection* admin_conn;
} DB_CONNECTION_DATA;
*/
struct LANGDEF
{
	UINT nID;
	WORD wLangID;
};

const LANGDEF languages[] =
{
	{ XTP_IDC_LANG_CROATIAN,       MAKELANGID(LANG_CROATIAN, SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_CZECH,          MAKELANGID(LANG_CZECH, SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_DANISH,         MAKELANGID(LANG_DANISH, SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_DUTCH,          MAKELANGID(LANG_DUTCH, SUBLANG_DUTCH) },
	{ XTP_IDC_LANG_ENGLISH,        MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US) },
	{ XTP_IDC_LANG_ESTONIAN,       MAKELANGID(LANG_ESTONIAN, SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_FINNISH,        MAKELANGID(LANG_FINNISH, SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_FRENCH,         MAKELANGID(LANG_FRENCH, SUBLANG_FRENCH) },
	{ XTP_IDC_LANG_GERMAN,         MAKELANGID(LANG_GERMAN, SUBLANG_GERMAN) },
	{ XTP_IDC_LANG_HUNGARIAN,      MAKELANGID(LANG_HUNGARIAN, SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_ITALIAN,        MAKELANGID(LANG_ITALIAN, SUBLANG_ITALIAN) },
	{ XTP_IDC_LANG_LATVIAN,        MAKELANGID(LANG_LATVIAN, SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_LITHUANIAN,     MAKELANGID(LANG_LITHUANIAN, SUBLANG_LITHUANIAN) },
	{ XTP_IDC_LANG_NORWEGIAN,      MAKELANGID(LANG_NORWEGIAN, SUBLANG_NORWEGIAN_BOKMAL) },
	{ XTP_IDC_LANG_POLISH,         MAKELANGID(LANG_POLISH,  SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_PORTUGUESE_BR,  MAKELANGID(LANG_PORTUGUESE, SUBLANG_PORTUGUESE_BRAZILIAN) },
	{ XTP_IDC_LANG_PORTUGUESE,     MAKELANGID(LANG_PORTUGUESE, SUBLANG_PORTUGUESE) },
	{ XTP_IDC_LANG_ROMANIAN,       MAKELANGID(LANG_ROMANIAN, SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_RUSSIAN,        MAKELANGID(LANG_RUSSIAN, SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_SLOVAK,         MAKELANGID(LANG_SLOVAK, SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_SLOVENIAN,      MAKELANGID(LANG_SLOVENIAN, SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_SPANISH,        MAKELANGID(LANG_SPANISH, SUBLANG_SPANISH_MODERN) },
	{ XTP_IDC_LANG_SWEDISH,        MAKELANGID(LANG_SWEDISH, SUBLANG_DEFAULT) },
	{ XTP_IDC_LANG_UKRAINIAN,      MAKELANGID(LANG_UKRAINIAN, SUBLANG_DEFAULT) },
};

class CLanguageInfo
{
	LCID	nLCID;
	TCHAR szLangName[64];
	TCHAR szLangAbrev[64];
public:
	CLanguageInfo(void)
	{
		nLCID	= 0;
		_tcscpy(szLangName,_T(""));
		_tcscpy(szLangAbrev,_T(""));
	}

	CLanguageInfo(LCID lcid,LPCTSTR name,LPCTSTR abrev)
	{
		nLCID = lcid;
		_tcscpy(szLangName,name);
		_tcscpy(szLangAbrev,abrev);
	}

	CLanguageInfo(const CLanguageInfo &c)
	{
		*this = c;
	}

	LCID getLCID(void)					{ return nLCID;			}
	LPCTSTR getLangName(void)		{ return szLangName; }
	LPCTSTR getLangAbrev(void)		{ return szLangAbrev;	}
};

typedef std::vector<CLanguageInfo> vecLANGUAGE_INFO;


///////////////////////////////////////////////////////////////////////////////
// CModuleInstance; this class holds data on which modules are initiated and
// the searchpath to the module (used as an identifer); 051124 p�d
class CModuleInstance
{
	HINSTANCE hInst;
	TCHAR szModulePath[MAX_PATH];
public:
	CModuleInstance(void)
	{
		hInst = NULL;
		_tcscpy(szModulePath,_T(""));
	}
	CModuleInstance(HINSTANCE inst,LPCTSTR path)
	{
		hInst = inst;
		_tcscpy(szModulePath,path);
	}
	CModuleInstance(const CModuleInstance &c)
	{
		*this = c;
	}

	HINSTANCE getHInst(void)		{ return hInst; }
	LPCTSTR getModulePath(void)	{ return szModulePath; }
};

typedef std::vector<CModuleInstance> HINSTANCE_VEC;

///////////////////////////////////////////////////////////////////////////////
// CPADTreeCtrl

#define CTreeCtrlBase CXTPCommandBarsSiteBase<CXTTreeCtrl>

class CPADTreeCtrl : public CTreeCtrlBase
{
// Construction
public:
	DECLARE_DYNAMIC( CPADTreeCtrl )
	CPADTreeCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPADTreeCtrl)
	virtual BOOL PreTranslateMessage(MSG *pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CPADTreeCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CPADTreeCtrl)
	afx_msg BOOL OnEraseBkgnd(CDC *pDC);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnPopupMenuItem(UINT nMenuID);
	afx_msg void OnPopupMenuItemExpandCollapse(UINT nMenuID);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
}; // class CObjectHierarchyTreeCtrl

///////////////////////////////////////////////////////////////////////////////
// CPADExtTreeCtrl; 060427 p�d

class CPADExtTreeCtrl : public CTreeCtrlBase
{
	//private:
	CEdit *m_pTreeEdit;

	BOOL m_bIsTopLevelItem;
	BOOL m_bIsDropTarget;
	CString m_sSuitePathAndFN;

	UINT m_menuItemSelected;
	int m_nSelectedLevel;

	CInputDlg *pInputDlg;
protected:
	CImageList*	m_pDragImage;
	BOOL		m_bLDragging;
	HTREEITEM	m_hitemDrag,m_hitemDrop;// Construction

	HTREEITEM CopyBranch( HTREEITEM htiBranch, HTREEITEM htiNewParent, HTREEITEM htiAfter = TVI_LAST );
	HTREEITEM CopyItem( HTREEITEM hItem, HTREEITEM htiNewParent,HTREEITEM htiAfter = TVI_LAST );


	virtual void OnItemCopied( HTREEITEM hItem, HTREEITEM hNewItem );

	void ExpandTree(HTREEITEM);
	void CollapseTree(HTREEITEM);

	void input(HTREEITEM item)
	{
		pInputDlg = new CInputDlg();
		if (pInputDlg != NULL)
		{
			pInputDlg->setEditText(GetItemText(item));
			if (pInputDlg->DoModal() == IDOK)
			{
				SetItemText(item,pInputDlg->getEditText());
			}
			delete pInputDlg;
		}
	}
public:
	DECLARE_DYNAMIC( CPADExtTreeCtrl )
	CPADExtTreeCtrl();

	// My mthods
	void setSuitePathAndFN(LPCTSTR);
	
	UINT getSelectedMenuID(void)
	{
		return m_menuItemSelected;
	}
	CEdit *getEditCtrl(void)
	{
		return m_pTreeEdit;
	}
	void resetEditCtrl(void)
	{
		m_pTreeEdit = NULL;
	}
	int getSelectedLevel(void)
	{
		return m_nSelectedLevel;
	}
	
	int getLevel(HTREEITEM hItem)
	{
		int nLevel = 0;

		while ((hItem = GetParentItem( hItem )) != NULL)
		{
			nLevel++;
		}
		return nLevel;
	}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPADExtTreeCtrl)
	//}}AFX_VIRTUAL

	virtual ~CPADExtTreeCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CPADTreeCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnPopupMenuItem(UINT nMenuID);

	afx_msg void OnBeginlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult);

	// Drag and drop handling
	afx_msg void OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);


	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
}; // class CObjectHierarchyTreeCtrl

// Simple vector to hold items to show/hide, set in mehod void getItemsToShowHide(...)

//typedef std::vector<int> vecInt;


///////////////////////////////////////////////////////////////////////////////
// CDockingPaneManager 

class CNavigationPane : public CXTPShortcutBarPane
{
	DECLARE_DYNAMIC( CNavigationPane )
protected:
	CPADTreeCtrl m_wndTreeCtrl;
	CImageList m_ilTreeIcons;
	CString m_sToDoCaption;
	// Vector to hold items to show/hide, set in mehod void getItemsToShowHide(...)
	vecInt m_vecShowHideItems;
public:
	CNavigationPane();
	virtual ~CNavigationPane();

	BOOL Create(LPCTSTR lpszCaption,LPCTSTR lpszTodoCaption, CXTPShortcutBar* pParent);

	void setupShellTreeData(vecTreeList &,LPCTSTR suite_fn,BOOL do_add);

	void clearShellTreeData(void)
	{
		m_wndTreeCtrl.DeleteAllItems();
	}

	void setToDoCaption(LPCTSTR s)
	{
		m_sToDoCaption = s;
	}

	void getItemsToShowHide(vecInt&);

	void setTreeDataFocus(void)
	{
		m_wndTreeCtrl.SetFocus();
	}

	// Generated message map functions
protected:
	//{{AFX_MSG(CNavigationPane)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};


/////////////////////////////////////////////////////////////////////////////
// Colors

const COLORREF colorAliceBlue            = RGB(240,248,255);
const COLORREF colorAntiqueWhite         = RGB(250,235,215);
const COLORREF colorAqua                 = RGB(  0,255,255);
const COLORREF colorAquamarine           = RGB(127,255,212);
const COLORREF colorAzure                = RGB(240,255,255);
const COLORREF colorBeige                = RGB(245,245,220);
const COLORREF colorBisque               = RGB(255,228,196);
const COLORREF colorBlack                = RGB(  0,  0,  0);
const COLORREF colorBlanchedAlmond       = RGB(255,255,205);
const COLORREF colorBlue                 = RGB(  0,  0,255);
const COLORREF colorBlueViolet           = RGB(138, 43,226);
const COLORREF colorBrown                = RGB(165, 42, 42);
const COLORREF colorBurlywood            = RGB(222,184,135);
const COLORREF colorCadetBlue            = RGB( 95,158,160);
const COLORREF colorChartreuse           = RGB(127,255,  0);
const COLORREF colorChocolate            = RGB(210,105, 30);
const COLORREF colorCoral                = RGB(255,127, 80);
const COLORREF colorCornflowerBlue       = RGB(100,149,237);
const COLORREF colorCornsilk             = RGB(255,248,220);
const COLORREF colorCrimson              = RGB(220, 20, 60);
const COLORREF colorCyan                 = RGB(  0,255,255);
const COLORREF colorDarkBlue             = RGB(  0,  0,139);
const COLORREF colorDarkCyan             = RGB(  0,139,139);
const COLORREF colorDarkGoldenRod        = RGB(184,134, 11);
const COLORREF colorDarkGray             = RGB(169,169,169);
const COLORREF colorDarkGreen            = RGB(  0,100,  0);
const COLORREF colorDarkKhaki            = RGB(189,183,107);
const COLORREF colorDarkMagenta          = RGB(139,  0,139);
const COLORREF colorDarkOliveGreen       = RGB( 85,107, 47);
const COLORREF colorDarkOrange           = RGB(255,140,  0);
const COLORREF colorDarkOrchid           = RGB(153, 50,204);
const COLORREF colorDarkRed              = RGB(139,  0,  0);
const COLORREF colorDarkSalmon           = RGB(233,150,122);
const COLORREF colorDarkSeaGreen         = RGB(143,188,143);
const COLORREF colorDarkSlateBlue        = RGB( 72, 61,139);
const COLORREF colorDarkSlateGray        = RGB( 47, 79, 79);
const COLORREF colorDarkTurquoise        = RGB(  0,206,209);
const COLORREF colorDarkViolet           = RGB(148,  0,211);
const COLORREF colorDeepPink             = RGB(255, 20,147);
const COLORREF colorDeepSkyBlue          = RGB(  0,191,255);
const COLORREF colorDimGray              = RGB(105,105,105);
const COLORREF colorDodgerBlue           = RGB( 30,144,255);
const COLORREF colorFireBrick            = RGB(178, 34, 34);
const COLORREF colorFloralWhite          = RGB(255,250,240);
const COLORREF colorForestGreen          = RGB( 34,139, 34);
const COLORREF colorFuchsia              = RGB(255,  0,255);
const COLORREF colorGainsboro            = RGB(220,220,220);
const COLORREF colorGhostWhite           = RGB(248,248,255);
const COLORREF colorGold                 = RGB(255,215,  0);
const COLORREF colorGoldenRod            = RGB(218,165, 32);
const COLORREF colorGray                 = RGB(127,127,127);
const COLORREF colorGreen                = RGB(  0,128,  0);
const COLORREF colorGreenYellow          = RGB(173,255, 47);
const COLORREF colorHoneyDew             = RGB(240,255,240);
const COLORREF colorHotPink              = RGB(255,105,180);
const COLORREF colorIndianRed            = RGB(205, 92, 92);
const COLORREF colorIndigo               = RGB( 75,  0,130);
const COLORREF colorIvory                = RGB(255,255,240);
const COLORREF colorKhaki                = RGB(240,230,140);
const COLORREF colorLavender             = RGB(230,230,250);
const COLORREF colorLavenderBlush        = RGB(255,240,245);
const COLORREF colorLawngreen            = RGB(124,252,  0);
const COLORREF colorLemonChiffon         = RGB(255,250,205);
const COLORREF colorLightBlue            = RGB(173,216,230);
const COLORREF colorLightCoral           = RGB(240,128,128);
const COLORREF colorLightCyan            = RGB(224,255,255);
const COLORREF colorLightGoldenRodYellow = RGB(250,250,210);
const COLORREF colorLightGreen           = RGB(144,238,144);
const COLORREF colorLightGrey            = RGB(211,211,211);
const COLORREF colorLightPink            = RGB(255,182,193);
const COLORREF colorLightSalmon          = RGB(255,160,122);
const COLORREF colorLightSeaGreen        = RGB( 32,178,170);
const COLORREF colorLightSkyBlue         = RGB(135,206,250);
const COLORREF colorLightSlateGray       = RGB(119,136,153);
const COLORREF colorLightSteelBlue       = RGB(176,196,222);
const COLORREF colorLightYellow          = RGB(255,255,224);
const COLORREF colorLime                 = RGB(  0,255,  0);
const COLORREF colorLimeGreen            = RGB( 50,205, 50);
const COLORREF colorLinen                = RGB(250,240,230);
const COLORREF colorMagenta              = RGB(255,  0,255);
const COLORREF colorMaroon               = RGB(128,  0,  0);
const COLORREF colorMediumAquamarine     = RGB(102,205,170);
const COLORREF colorMediumBlue           = RGB(  0,  0,205);
const COLORREF colorMediumOrchid         = RGB(186, 85,211);
const COLORREF colorMediumPurple         = RGB(147,112,219);
const COLORREF colorMediumSeaGreen       = RGB( 60,179,113);
const COLORREF colorMediumSlateBlue      = RGB(123,104,238);
const COLORREF colorMediumSpringGreen    = RGB(  0,250,154);
const COLORREF colorMediumTurquoise      = RGB( 72,209,204);
const COLORREF colorMediumVioletRed      = RGB(199, 21,133);
const COLORREF colorMidnightBlue         = RGB( 25, 25,112);
const COLORREF colorMintCream            = RGB(245,255,250);
const COLORREF colorMistyRose            = RGB(255,228,225);
const COLORREF colorMoccasin             = RGB(255,228,181);
const COLORREF colorNavajoWhite          = RGB(255,222,173);
const COLORREF colorNavy                 = RGB(  0,  0,128);
const COLORREF colorOldLace              = RGB(253,245,230);
const COLORREF colorOlive                = RGB(128,128,  0);
const COLORREF colorOliveDrab            = RGB(107,142, 35);
const COLORREF colorOrange               = RGB(255,165,  0);
const COLORREF colorOrangeRed            = RGB(255, 69,  0);
const COLORREF colorOrchid               = RGB(218,112,214);
const COLORREF colorPaleGoldenRod        = RGB(238,232,170);
const COLORREF colorPaleGreen            = RGB(152,251,152);
const COLORREF colorPaleTurquoise        = RGB(175,238,238);
const COLORREF colorPaleVioletRed        = RGB(219,112,147);
const COLORREF colorPapayaWhip           = RGB(255,239,213);
const COLORREF colorPeachPuff            = RGB(255,218,185);
const COLORREF colorPeru                 = RGB(205,133, 63);
const COLORREF colorPink                 = RGB(255,192,203);
const COLORREF colorPlum                 = RGB(221,160,221);
const COLORREF colorPowderBlue           = RGB(176,224,230);
const COLORREF colorPurple               = RGB(128,  0,128);
const COLORREF colorRed                  = RGB(255,  0,  0);
const COLORREF colorRosyBrown            = RGB(188,143,143);
const COLORREF colorRoyalBlue            = RGB( 65,105,225);
const COLORREF colorSaddleBrown          = RGB(139, 69, 19);
const COLORREF colorSalmon               = RGB(250,128,114);
const COLORREF colorSandyBrown           = RGB(244,164, 96);
const COLORREF colorSeaGreen             = RGB( 46,139, 87);
const COLORREF colorSeaShell             = RGB(255,245,238);
const COLORREF colorSienna               = RGB(160, 82, 45);
const COLORREF colorSilver               = RGB(192,192,192);
const COLORREF colorSkyBlue              = RGB(135,206,235);
const COLORREF colorSlateBlue            = RGB(106, 90,205);
const COLORREF colorSlateGray            = RGB(112,128,144);
const COLORREF colorSnow                 = RGB(255,250,250);
const COLORREF colorSpringGreen          = RGB(  0,255,127);
const COLORREF colorSteelBlue            = RGB( 70,130,180);
const COLORREF colorTan                  = RGB(210,180,140);
const COLORREF colorTeal                 = RGB(  0,128,128);
const COLORREF colorThistle              = RGB(216,191,216);
const COLORREF colorTomato               = RGB(255, 99, 71);
const COLORREF colorTurquoise            = RGB( 64,224,208);
const COLORREF colorViolet               = RGB(238,130,238);
const COLORREF colorWheat                = RGB(245,222,179);
const COLORREF colorWhite                = RGB(255,255,255);
const COLORREF colorWhiteSmoke           = RGB(245,245,245);
const COLORREF colorYellow               = RGB(255,255,  0);
const COLORREF colorYellowGreen          = RGB(139,205, 50);

/////////////////////////////////////////////////////////////////////////////
// Misc. functions

BOOL getGlobLangsInfo(vecLANGUAGE_INFO &);	// Create a vector for languages[]
BOOL getGlobLangInfo(LPCTSTR lng_abrev,CLanguageInfo &);	// Get info for a specific language

void setLangSet(LPCTSTR lang_name);
CString getLanguageFileName(void);
void getLanguageFiles(CStringArray &);	// Get LanguageFiles in Language directory;

CString getResStr(UINT id);

CString getShellDataDir(void);
BOOL getShellDataFiles(CStringArray &);
BOOL getShellDataFilesOnDisk(CStringArray &);
BOOL checkShellDataFiles(CStringArray &,CStringArray &);

BOOL getShellTreeDataItems(CStringArray &,vecTreeList &);

BOOL getSuites(CStringArray &,BOOL clear = TRUE);

BOOL getModules(CStringArray &,BOOL clear = TRUE);

CString getHelpDir(void);

CString getSetupShellData(void);

HTREEITEM GetNextTreeItem(const CTreeCtrl&, HTREEITEM);

int getLevel(const CTreeCtrl&,HTREEITEM);

BOOL getCheckStatus(const CTreeCtrl&,HTREEITEM);

void setTreeParents(CTreeCtrl&,HTREEITEM,BOOL);

BOOL getRSSXMLFile(CStringArray &str_array,long *size);

void setDBQualified(int);
int getDBQualified(void);

BOOL makeDBConnection(SAConnection &con, bool silent = false);
BOOL makeAdminDBConnection(SAConnection &con, bool silent = false);

int SplitString(const CString& input,const CString& delimiter, CStringArray& results);

int setupAdminDatabase(void);

int runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table);
// Create database table from string; 080627 p�d
int runSQLScriptFileEx(LPCTSTR table_name,CString script);


#endif // _GLOB_LANGUAGE_H_
