// DatabaseBackupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DatabaseBackupDlg.h"

// CDatabaseBackupDlg dialog

IMPLEMENT_DYNAMIC(CDatabaseBackupDlg, CDialog)

BEGIN_MESSAGE_MAP(CDatabaseBackupDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &CDatabaseBackupDlg::OnBnClickedSearchPath)
	ON_BN_CLICKED(IDC_BUTTON2, &CDatabaseBackupDlg::OnBnClickedDoBackup)
	ON_NOTIFY(NM_CLICK, ID_BACKUP_REPORT, OnReportItemClick)
END_MESSAGE_MAP()

CDatabaseBackupDlg::CDatabaseBackupDlg(enumAction action /*=NOTHING*/,CWnd* pParent /*=NULL*/)
	: CDialog(CDatabaseBackupDlg::IDD, pParent)
{
	m_bInitialized = FALSE;
	m_enumAction = action;
}

CDatabaseBackupDlg::~CDatabaseBackupDlg()
{
	m_ilIcons.DeleteImageList();
}

void CDatabaseBackupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_BCK_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_BCK_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_BCK_LBL3, m_wndLbl3);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);

	DDX_Control(pDX, IDC_BUTTON1, m_wndBtnSearchPath);
	DDX_Control(pDX, IDC_BUTTON2, m_wndBtnDoBackup);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);

	//}}AFX_DATA_MAP

}

// CLanguageDlg message handlers
BOOL CDatabaseBackupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if (!m_bInitialized)
	{
		m_sRestoreMsg.Format(_T("%s\n%s\n\n%s\n"),
			(getResStr(IDS_STRING40163)),
			(getResStr(IDS_STRING40164)),
			(getResStr(IDS_STRING40165)));

		m_sBackupDoneMsg = (getResStr(IDS_STRING40166));
		m_sRestoreDoneMsg = (getResStr(IDS_STRING40167));
		m_sBackupDeviceMsg = (getResStr(IDS_STRING40182));

		m_wndLbl1.SetWindowText((getResStr(IDS_STRING40151)));
		m_wndLbl2.SetWindowText((getResStr(IDS_STRING40152)));

		m_wndLbl3.SetBkColor(INFOBK);
		m_wndLbl3.SetTextColor(BLUE);

		m_wndEdit1.SetEnabledColor(BLACK,WHITE );
		m_wndEdit1.SetDisabledColor(BLACK,INFOBK );
		m_wndEdit1.SetReadOnly();

		m_wndBtnSearchPath.SetWindowText(getResStr(IDS_STRING40186));

		m_wndCancelBtn.SetWindowText(getResStr(IDS_STRING40157));

		m_sDBBackupDir = getServerDBBackupDir();
		if(m_sDBBackupDir.Right(1) != '\\') m_sDBBackupDir += _T("\\");
		m_wndEdit1.SetWindowText(m_sDBBackupDir);

		GetUserDBInRegistry(m_szUserDB);

		// Action specifics; 081003 p�d
		if (m_enumAction == BACKUP_DB)
		{
			SetWindowText((getResStr(IDS_STRING40150)));
			m_wndLbl3.SetWindowText((getResStr(IDS_STRING40161)));
			m_wndBtnDoBackup.SetWindowText((getResStr(IDS_STRING40156)));
			m_wndBtnDoBackup.EnableWindow(TRUE);
		}
		else if (m_enumAction == RESTORE_DB)
		{
			SetWindowText((getResStr(IDS_STRING40159)));
			m_wndLbl3.SetWindowText((getResStr(IDS_STRING40162)+_T("\n")+getResStr(IDS_STRING40168)));
			m_wndBtnDoBackup.SetWindowText((getResStr(IDS_STRING40160)));

			m_wndBtnDoBackup.EnableWindow(FALSE);
		}

		setupReport();
		populateReport();

		m_bInitialized = TRUE;
	}

	

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

// CDatabaseBackupDlg message handlers

void CDatabaseBackupDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_BACKUP_REPORT, FALSE, FALSE))
		{
			return;
		}
	}

	VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
	CBitmap bmp;
	VERIFY(bmp.LoadBitmap(IDB_BITMAP7));
	m_ilIcons.Add(&bmp, RGB(255, 0, 255));

	m_wndReport.SetImageList(&m_ilIcons);

	m_wndReport.ShowWindow(SW_NORMAL);

	// Checkbox
	pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""), 20,FALSE,12));
	pCol->GetEditOptions()->m_bAllowEdit = FALSE;
	pCol->SetHeaderAlignment(DT_WORDBREAK);
	// "Filnamn p�"
	pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (getResStr(IDS_STRING40153)), 150));
	pCol->GetEditOptions()->m_bAllowEdit = FALSE;
	pCol->SetHeaderAlignment(DT_WORDBREAK);
	// "Namn p� databas"
	pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (getResStr(IDS_STRING40154)), 150));
	pCol->GetEditOptions()->m_bAllowEdit = FALSE;
	pCol->SetHeaderAlignment(DT_WORDBREAK);
	// "Backup datum"
	pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (getResStr(IDS_STRING40155)), 150));
	pCol->GetEditOptions()->m_bAllowEdit = FALSE;
	pCol->SetHeaderAlignment(DT_WORDBREAK);
	// "Storlek p� backup"
	pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (getResStr(IDS_STRING40158)), 100));
	pCol->GetEditOptions()->m_bAllowEdit = FALSE;
	pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
	pCol->SetAlignment(DT_RIGHT);

	m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
	m_wndReport.SetMultipleSelection( FALSE );
	m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
	m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
	m_wndReport.AllowEdit(TRUE);
	m_wndReport.FocusSubItems(TRUE);
	m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
	m_wndReport.GetRecords()->SetCaseSensitive(FALSE);

	// Need to set size of Report control; 051219 p�d
	RECT rect;
	GetClientRect(&rect);
	setResize(GetDlgItem(ID_BACKUP_REPORT),2,100,rect.right - 4,rect.bottom - 135);
}

void CDatabaseBackupDlg::populateReport(void)
{
	BY_HANDLE_FILE_INFORMATION fileInfo;
	CString sFileDateTime;
	CString sBackupFileName;
	CString sBackupFileNameNoExt;
	CString sBackupFilePath;
	CString sFileSize;;
	long lFileSize;
	BOOL bEditable = FALSE;
	m_wndReport.ResetContent();
	CString sDBName,sDBName_log,sSQL;

	CStringArray sarrFileList;
	getListOfFilesInDirectory(L"*.bak",m_sDBBackupDir,sarrFileList);

	if (m_enumAction == BACKUP_DB)
	{
		for (UINT i = 0;i < m_vecHmsUserDBs.size();i++)
		{
			m_recHmsDBs = m_vecHmsUserDBs[i];
			lFileSize = 0;
			sFileDateTime.Empty();
			sBackupFileName.Format(_T("%s.bak"),m_recHmsDBs.getDBName());
			sBackupFilePath.Format(_T("%s%s"),m_sDBBackupDir,sBackupFileName);
			if (fileExists(sBackupFilePath))
			{
				if(getFileInformation(sBackupFilePath,fileInfo))
				{
					convertFileDateTime(fileInfo.ftLastWriteTime, sFileDateTime);
				}
				lFileSize = getFileSize(sBackupFilePath);
			}
			// Calculate filesize in kB; 081203 p�d
			sFileSize = formatNumber(lFileSize/1024);

			m_wndReport.AddRecord(new CDBBackupRec(TRUE,	// Action
				sBackupFileName,
				m_recHmsDBs.getDBName(),
				sFileDateTime,
				sFileSize,
				TRUE));
		}
	}
	else if (m_enumAction == RESTORE_DB)
	{
		for(int nLoop=0; nLoop<sarrFileList.GetSize(); nLoop++)
		{
			lFileSize = 0;
			sFileDateTime.Empty();
			sBackupFileName = sarrFileList.GetAt(nLoop);
			sBackupFilePath.Format(_T("%s%s"), m_sDBBackupDir, sBackupFileName);
			if (fileExists(sBackupFilePath))
			{
				sSQL.Format(script_filelistOnlyDB, sBackupFilePath);
				getDBBackupFilelistOnly(sSQL, sDBName, sDBName_log);

				if(getFileInformation(sBackupFilePath, fileInfo) == TRUE)
				{
					convertFileDateTime(fileInfo.ftLastWriteTime, sFileDateTime);
				}

				lFileSize = getFileSize(sBackupFilePath);
				// Calculate filesize in kB; 081203 p�d
				sFileSize = formatNumber(lFileSize/1024);
			}

			if (sDBName.CompareNoCase(m_szUserDB) == 0)
				bEditable = FALSE;
			else
				bEditable = TRUE;

			m_wndReport.AddRecord(new CDBBackupRec(FALSE,	// not checked
				sarrFileList.GetAt(nLoop),
				sDBName,
				sFileDateTime,
				sFileSize,
				bEditable));	// editable
		}
	}

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}

void CDatabaseBackupDlg::doBackupOfDB(void)
{
	CString sDBName;
	CString sFileName;
	CString sBackupToPath;
	CString sSQL;
	BOOL bError = FALSE;
	CDBBackupRec *pRec = NULL;

	HCURSOR hPrevCursor = ::GetCursor();
	HCURSOR hWaitCursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);

	// Get selected databases to backup; 081002 p�d
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		::SetCursor(hWaitCursor);

		for (int i1 = 0;i1 < pRows->GetCount();i1++)
		{
			pRec = (CDBBackupRec*)pRows->GetAt(i1)->GetRecord();
			if (pRec != NULL)
			{
				if (pRec->getColumnCheck(COLUMN_0))
				{
					sFileName = pRec->getColumnText(COLUMN_1);
					sDBName = pRec->getColumnText(COLUMN_2);
					sBackupToPath.Format(_T("%s%s"),m_sDBBackupDir,sFileName);

					// Setup the SQL backupstring for Database; 081002 p�d
					sSQL.Format(script_backupDB, sDBName, sDBName, sBackupToPath, sDBName, sBackupToPath);
					if (runSQLScript(sSQL, sDBName))
					{
						pRec->setColumnText(COLUMN_3,getDateTime());
						pRec->setColumnTextColor(RGB(0,0,255),5,COLUMN_0,COLUMN_1,COLUMN_2,COLUMN_3,COLUMN_4);
						long lFileSize = getFileSize(sBackupToPath);
						CString sFileSize = formatNumber(lFileSize/1024);
						pRec->setColumnText(COLUMN_4, sFileSize);

						m_wndReport.Populate();
						m_wndReport.UpdateWindow();
					}	// if (runSQLScript(_T(sSQL),sDBName))
					else
					{
						bError = TRUE;
					}
				}	// if (pRec->getColumnCheck(COLUMN_0))
			}	// if (pRec != NULL)
		}	// for (int i1 = 0;i1 < pRows->GetCount();i1++)

		::SetCursor(hPrevCursor);

		// Open backup folder
		if(!bError)
		{
			// Tell user, the deed is done; 081003 p�d
			ShellExecute(NULL, _T("explore"), m_sDBBackupDir, NULL, NULL, SW_SHOWNORMAL);
			::MessageBox(this->GetSafeHwnd(),m_sBackupDoneMsg,getResStr(IDS_STRING900),MB_ICONINFORMATION | MB_OK);
		}
		else // something has gone wrong
		{
			::MessageBox(this->GetSafeHwnd(),getResStr(IDS_STRING40184),getResStr(IDS_STRING901),MB_ICONERROR | MB_OK);
		}
	}	// if (pRows != NULL)

	pRows = NULL;
	pRec = NULL;
}

void CDatabaseBackupDlg::doRestoreOfDB(void)
{
	CString sDBName;
	CString sDBName_log;
	CString sFileName;
	CString sPath;
	CString sPath_data;
	CString sPath_data_log;
	CString sSQL;
	BOOL bAllOK;

	HCURSOR hPrevCursor = ::GetCursor();
	HCURSOR hWaitCursor = AfxGetApp()->LoadStandardCursor(IDC_WAIT);

	CDBBackupRec *pRec = NULL;
	// Get selected databases to backup; 081002 p�d
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (::MessageBox(this->GetSafeHwnd(),m_sRestoreMsg,getResStr(IDS_STRING900),MB_ICONINFORMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		if (pRows != NULL)
		{
			::SetCursor(hWaitCursor);
			for (int i1 = 0;i1 < pRows->GetCount();i1++)
			{
				pRec = (CDBBackupRec*)pRows->GetAt(i1)->GetRecord();
				if (pRec != NULL)
				{
					if (pRec->getColumnCheck(COLUMN_0))
					{
						sFileName = pRec->getColumnText(COLUMN_1);
						sPath.Format(_T("%s%s"),m_sDBBackupDir,sFileName);

						// Get information, from backupfile, on datbase and log; 081003 p�d
						sSQL.Format(script_filelistOnlyDB,(sPath));
						if(getDBBackupFilelistOnly(sSQL,sDBName,sDBName_log) == FALSE) return;

						bAllOK = FALSE;

						// Setup the SQL restorestring and run script; 081003 p�d					
						CString csTmp1, csTmp2;
						getServerDBDataLogDir(&csTmp1, &csTmp2);	// get server data and log-directories
						sPath_data.Format(_T("%s\\%s.mdf"), csTmp1, sDBName);
						sPath_data_log.Format(_T("%s\\%s.ldf"), csTmp2, sDBName);
						sSQL.Format(script_restoreDB, sDBName, sPath, sDBName, sPath_data, sDBName_log, sPath_data_log);
						if (restoreDB(sSQL,sDBName))
						{
							if (m_pDBAdmin)
								m_pDBAdmin->userdb_New(CHmsUserDBRec(sDBName,L"",L""));

							pRec->setColumnTextColor(RGB(0,0,255),5,COLUMN_0,COLUMN_1,COLUMN_2,COLUMN_3,COLUMN_4);
							m_wndReport.Populate();
							m_wndReport.UpdateWindow();
							bAllOK = TRUE;
						}	// if (runSQLScript(_T(sSQL),sDBName))			
					}	// if (pRec->getColumnCheck(COLUMN_0))
				}	// if (pRec != NULL)
			}	// for (int i1 = 0;i1 < pRows->GetCount();i1++)

			::SetCursor(hPrevCursor);
			if (bAllOK)
			{
				// Tell user, the deed is done; 081003 p�d
				::MessageBox(this->GetSafeHwnd(),m_sRestoreDoneMsg,getResStr(IDS_STRING900),MB_ICONINFORMATION | MB_OK);
			}

		}	// if (pRows != NULL)
	}
	pRows = NULL;
	pRec = NULL;
}

void CDatabaseBackupDlg::OnBnClickedSearchPath()
{
	// Open backup folder
	ShellExecute(NULL, _T("explore"), m_sDBBackupDir, NULL, NULL, SW_SHOWNORMAL);
}

void CDatabaseBackupDlg::OnBnClickedDoBackup()
{
	CString sText;
	m_wndEdit1.GetWindowText(sText);

	// Action specifics; 081003 p�d
	switch (m_enumAction)
	{
	case BACKUP_DB:
		doBackupOfDB(); 
		break;
	case RESTORE_DB:
		doRestoreOfDB(); 
		break;
	};
}

void CDatabaseBackupDlg::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	BOOL bFound = FALSE;
	CXTPReportRows *pRows = NULL;
	CDBBackupRec *pRec = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		// Check if one or more items are checked.
		// If so, enable m_wndBtnDoBackup button; 081003 p�d
		pRows = m_wndReport.GetRows();
		if (pRows != NULL)
		{
			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRec = (CDBBackupRec *)pRows->GetAt(i)->GetRecord();
				if (pRec != NULL)
				{
					if (pRec->getColumnCheck(COLUMN_0))
					{
						bFound = TRUE;
						break;
					}	// if (pRec->getColumnCheck(COLUMN_0))
				}	// if (pRec != NULL)
			}	// for (int i = 0;i < pRows->GetCount();i++)
		}	// if (pRows != NULL)
	}	// if (pItemNotify != NULL)
	m_wndBtnDoBackup.EnableWindow(bFound);
}
