// CLanguageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "HMSShellXT.h"
#include "MainFrm.h"
#include "LanguageDlg.h"
#include ".\languagedlg.h"


// CLanguageDlg dialog

IMPLEMENT_DYNAMIC(CLanguageDlg, CDialog)
CLanguageDlg::CLanguageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLanguageDlg::IDD, pParent)
{
//	m_wndMDIFrame = (CMainFrame *)pParent;
}

CLanguageDlg::~CLanguageDlg()
{
}

void CLanguageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLanguageDlg)
	DDX_Control(pDX, IDC_BUTTON1, m_wndBtn1);
	DDX_Control(pDX, IDC_LIST1, m_wndLangList);
	DDX_Control(pDX, IDC_STATIC1, m_wndLabel1);
	//}}AFX_DATA_MAP

}


BEGIN_MESSAGE_MAP(CLanguageDlg, CDialog)
	//{{AFX_MSG_MAP(CDataBaseView)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_LBN_SELCHANGE(IDC_LIST1, OnLbnSelchangeList1)
END_MESSAGE_MAP()


// CLanguageDlg message handlers
BOOL CLanguageDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	setupLanguagesInListBox();

	m_sLangAbrevSet = _T("");

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

// EVENTS
void CLanguageDlg::OnBnClickedButton1()
{
/*
	SEND_MESSAGE_DATA_TRANS_STRUCT data;
	_tcscpy(data.szValue,m_sLangAbrevSet);
	data.nCommand = ID_CHANGE_LANGUAGE;
*/

	// Change language in registry; 051205 p�d
	setLangSet(m_sLangAbrevSet);

	// Send message to Main windows (CWinApp); 051205 p�d
//	AfxGetMainWnd()->SendMessage(WM_COMMAND,CMDID_CHANGE_LANGUAGE,0);
	CHMSShellXTApp *pApp = (CHMSShellXTApp*)AfxGetApp();
	if (pApp != NULL)
	{
		pApp->m_bShowChangeLanguageMsg = TRUE;
		pApp->cmdChangeLanguage();
	}

	this->SendMessage(WM_CLOSE);
}


void CLanguageDlg::OnLbnSelchangeList1()
{
  BOOL bFound = FALSE;
	int nItem = m_wndLangList.GetCurSel();

	if (nItem != LB_ERR)
	{
		UINT nSel = (DWORD)m_wndLangList.GetItemData(nItem);
		if (nSel >= 0 && nSel < m_vecLang.size())
		{
			CLanguageInfo linfo = m_vecLang[nSel];
			// Language abbervation; 051202 p�d
			m_sLangAbrevSet = linfo.getLangAbrev();
			m_wndBtn1.EnableWindow( TRUE );
		}
	}
}

// PROTECTED METHODS
void CLanguageDlg::setupLanguagesInListBox(void)
{
	XMLHandler *xml = new XMLHandler();
	DWORD nCnt = 0;

	getGlobLangsInfo(m_vecLang);
	getLanguageFiles(m_arrLangFiles);
	
	if (m_vecLang.size() > 0 && m_arrLangFiles.GetCount() > 0)
	{
		for (int j = 0;j < m_arrLangFiles.GetCount();j++)
		{
			if (xml->load(m_arrLangFiles[j]))
			{
				CString sLangAbrev = xml->str(LANGFILE_HEADER_LANGABREV);
				for (UINT i = 0;i < m_vecLang.size();i++)
				{	
					CLanguageInfo linfo = m_vecLang[i];
					if (sLangAbrev != "n/a")
					{
						if (sLangAbrev == linfo.getLangAbrev())
						{
							m_wndLangList.AddString(linfo.getLangName());
							m_wndLangList.SetItemData(nCnt,(DWORD)i);
							
							nCnt++;
							break;
						}	// if (sLangAbrev == linfo.getLangAbrev())
					}
				}	// for (UINT i = 0;i < m_vecLang.size();i++)
			}	// if (xml->Start(m_arrLangFiles[j]))
		}	// for (int j = 0;j < m_arrLangFiles.GetCount();j++)
	}	// if (m_vecLang.size() > 0 && m_arrLangFiles.GetCount() > 0)

	delete xml;
}

// PUBLIC
void CLanguageDlg::setLanguage(void)
{
	// Setup text for captions; 051201 p�d
	SetWindowText(getResStr(IDS_STRING40001));
	m_wndLabel1.SetWindowText(getResStr(IDS_STRING40002));
	m_wndBtn1.SetWindowText(getResStr(IDS_STRING40003));
	m_wndBtn1.EnableWindow( FALSE );
}

