// SplashDialog.cpp : implementation file
//

#include "stdafx.h"
#include "SplashDialog.h"


// CSplashDialog dialog

IMPLEMENT_DYNAMIC(CSplashDialog, CBitmapDialog)

BEGIN_MESSAGE_MAP(CSplashDialog, CBitmapDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

CSplashDialog::CSplashDialog(CWnd* pParent /*=NULL*/)
	: CBitmapDialog(CSplashDialog::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CSplashDialog::~CSplashDialog()
{
}

void CSplashDialog::DoDataExchange(CDataExchange* pDX)
{
	CBitmapDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	//}}AFX_DATA_MAP
}


/////////////////////////////////////////////////////////////////////////////
// CSplashDialog message handlers

BOOL CSplashDialog::OnInitDialog()
{
	CBitmapDialog::OnInitDialog();


	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
//	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
//	ASSERT(IDM_ABOUTBOX < 0xF000);


	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	// ** Load the bitmap **
	LoadBitmap (IDB_BITMAP3);
	SetTransparent (TRUE);
	SetTransColor (RGB(255,0,255));
	SetStaticTransparent (TRUE);
	SetClickAnywhereMove (TRUE);

	// Setup font(s) to use in OnPaint
	LOGFONT lfFont1;
	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 85;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_MEDIUM;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt1.CreatePointFontIndirect(&lfFont1);

	::ZeroMemory(&lfFont1,sizeof(LOGFONT));
	lfFont1.lfHeight	= 85;
	lfFont1.lfItalic	= FALSE;
	lfFont1.lfWeight	= FW_NORMAL;
	::lstrcpy(lfFont1.lfFaceName,_T("Arial"));
	m_fnt2.CreatePointFontIndirect(&lfFont1);

	const LPCTSTR VER_NUMBER						= _T("FileVersion");
	const LPCTSTR VER_COMPANY						= _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT					= _T("LegalCopyright");

	m_sVersion		= getVersionInfo(NULL,VER_NUMBER);
	m_sCopyright	= getVersionInfo(NULL,VER_COPYRIGHT);
	m_sCompany		= getVersionInfo(NULL,VER_COMPANY);
	m_sInfoText		= "";

	
	CenterWindow();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSplashDialog::setSplashInfoText(LPCTSTR txt)
{
	m_sInfoText = txt;
	Invalidate();
	UpdateWindow();
}


// CSplashDialog message handlers
void CSplashDialog::OnSysCommand(UINT nID, LPARAM lParam)
{
	CBitmapDialog::OnSysCommand(nID, lParam);
}

void CSplashDialog::OnLButtonDown(UINT nFlags,CPoint pnt)
{
	OnOK();
}


// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSplashDialog::OnPaint() 
{
	CString sRelease(_T("Release "));
	CString sVersionLbl(getResStr(IDS_STRING163));
	CPaintDC dc(this);
	CPen pen(PS_DOT,1,RGB(0,0,0));
	CRect rect(130,150,430,255);
	
	dc.SelectObject(&pen);
	dc.SelectObject(&m_fnt2);
	dc.SetTextColor(RGB(0,0,250));
	//dc.RoundRect(rect,CPoint(30,30));

	// Find release number; 090731 Peter
	HKEY hKey;
	TCHAR szBuf[40]; szBuf[0] = '\0';
	int len = sizeof(szBuf) / sizeof(TCHAR);
	if( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\HaglofManagmentSystem\\Settings"), 0, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS )
	{
		RegQueryValueEx(hKey, _T("Release"), NULL, NULL, (LPBYTE)szBuf, (unsigned long*)&len);
		sRelease += szBuf;

		RegCloseKey(hKey);
	}

	rect.InflateRect(-15,-10,-10,-10);
	dc.DrawText(m_sInfoText,rect,DT_WORDBREAK | DT_END_ELLIPSIS );

	dc.SetBkMode(TRANSPARENT);
	dc.SelectObject(&m_fnt1);
	dc.SetTextColor(RGB(255,255,255));
	dc.TextOut(16,125,sRelease,sRelease.GetLength());
	dc.SetTextColor(RGB(0,0,255));
	dc.TextOut(145,330,sVersionLbl + _T(" ") + m_sVersion,m_sVersion.GetAllocLength() + sVersionLbl.GetAllocLength() + 1);
	dc.TextOut(300,330,m_sCopyright,m_sCopyright.GetAllocLength());

	CBitmapDialog::OnPaint();
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSplashDialog::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}
