// HMSShellXT.h : main header file for the HMSShellXT application
//
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#ifndef __CGFILTYP_H__
#include "CGFILTYP.H"	//	CGCFileTypeAccess
#endif

#include "resource.h"       // main symbols

#include "XMLHandler.h"

#include "SplashDialog.h"

#include <fstream>
#include <vector>

using namespace std;

// CHMSShellXTApp:
// See HMSShellXT.cpp for the implementation of this class
//


class CHMSShellXTApp : public CWinApp
{
public:
	CHMSShellXTApp();

	void ReloadDefaultMenu();
	void SetRTLLayout(BOOL bRTLLayout);
	BOOL IsUnicodeAndLayoutSupport();
	void InitSuites(void);
	void InitSuiteAndUserModuleConfig(void);

	void GetUserModulesPerSuite(CString suite);

	LCID m_curLanguageLCID;
	BOOL m_bIsRTL;

	vecINDEX_TABLE m_vecIndexTable;
	vecINFO_TABLE m_vecInfoTable;

	BOOL m_bShowChangeLanguageMsg;
	BOOL setupLanguage(CString lang_abbrev);

	void cmdChangeLanguage(void);	// Added 2007-11-06 p�d

	void matchShellDataFiles(void);	// Added 2009-01-13 p�d

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
protected:
	BOOL m_bLogging;
	HMENU m_hMDIMenu;
	HACCEL m_hMDIAccel;
	HINSTANCE_VEC m_vecHInstances;
	CStringArray m_arrSuitesList;
	CStringArray m_arrShellDataFileList;
	CStringArray m_arrShellDataFileListOnDisk;
	vecTreeList m_vecShellData;

	ofstream m_logfile;

	CStringArray m_arrUserModulesPerSuite;

	CSplashDialog *m_pSplashDlg;

	void ApplyDockingPaneID(CString subkey, DWORD id);
	void FixDockingPaneIDs();
	void WriteToLog(CStringA message, bool error = false);
	CString HandleFileType(LPCTSTR  aFileType, LPCTSTR  aDocClassName, LPCTSTR aModuleName);
  BOOL ModuleExists(LPCTSTR aModuleName);
public:
	//{{AFX_MSG(CScribbleApp)
	afx_msg void OnAppAbout();
	afx_msg void OnChangeLanguageCmd();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CHMSShellXTApp theApp;