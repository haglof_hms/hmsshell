#pragma once

#include "Resource.h"

#include "DBHandler.h"
/////////////////////////////////////////////////////////////////////////////////
// CDBBackupRec

class CDBBackupRec : public CXTPReportRecord
{
	//private:
protected:

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue,BOOL editable = TRUE) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
			if (!editable)
				SetTextColor(RED);
			else
				SetTextColor(BLACK);
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}

		void setTextColor(COLORREF rgb_value)	
		{ 
			SetTextColor(rgb_value);
		}
	};
	//////////////////////////////////////////////////////////////////////////
	// Customized record item, used for displaying checkboxes.
	class CCheckItem : public CXTPReportRecordItem
	{
	public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck = TRUE,BOOL editable = TRUE)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
			SetEditable(editable);		
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);		
		}
	};
public:
	CDBBackupRec(void)
	{
		AddItem(new CCheckItem());
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CDBBackupRec(BOOL is_check,LPCTSTR filename,LPCTSTR dbname,LPCTSTR date,LPCTSTR size,BOOL editable)	 
	{
		AddItem(new CCheckItem(is_check,editable));
		AddItem(new CTextItem((filename),editable));	
		AddItem(new CTextItem((dbname),editable));	
		AddItem(new CTextItem((date),editable));	
		AddItem(new CTextItem((size),editable));	
	}

	inline CString getColumnText(int item)	{ return ((CTextItem*)GetItem(item))->getTextItem();	}

	inline void setColumnText(int item,LPCTSTR text)	{ ((CTextItem*)GetItem(item))->setTextItem(text);}

	inline BOOL getColumnCheck(int item)	{	return ((CCheckItem*)GetItem(item))->getChecked();	}

	inline void setColumnCheck(int item,BOOL bChecked)	{	((CCheckItem*)GetItem(item))->setChecked(bChecked);	}

	void setColumnTextColor(COLORREF rgb_value,int num,...)	
	{ 
		int i,col;
		va_list vl;
		va_start(vl,num);
		for (i = 0;i < num;i++)
		{
			col = va_arg(vl,int);
			((CTextItem*)GetItem(col))->setTextColor(rgb_value);
		}
		va_end(vl);
	}

};

// CDatabaseBackupDlg dialog

typedef enum { NOTHING,BACKUP_DB,RESTORE_DB } enumAction;

class CDatabaseBackupDlg : public CDialog
{
	DECLARE_DYNAMIC(CDatabaseBackupDlg)

	BOOL m_bInitialized;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;

	CMyExtEdit m_wndEdit1;

	CButton m_wndBtnSearchPath;
	CButton m_wndBtnDoBackup;
	CButton m_wndCancelBtn;

	CMyReportCtrl m_wndReport;

	TCHAR m_szUserDB[128];
	CString m_sDBBackupDir;

	CString m_sBackupDoneMsg;
	CString m_sRestoreDoneMsg;
	CString m_sRestoreMsg;
	CString m_sBackupDeviceMsg;

	enumAction m_enumAction;

	CImageList m_ilIcons;

	CDBAdmin *m_pDBAdmin;
protected:
	CHmsUserDBRec m_recHmsDBs;
	vecHmsUserDBs m_vecHmsUserDBs;

	void setupReport(void);

	void doBackupOfDB(void);
	void doRestoreOfDB(void);
public:
	CDatabaseBackupDlg(enumAction action = NOTHING,CWnd* pParent = NULL);   // standard constructor
	virtual ~CDatabaseBackupDlg();

	void setHMSDBs(vecHmsUserDBs &vec)	{	m_vecHmsUserDBs = vec;	}
	void setHMSDBAdmin(CDBAdmin *db)		{	m_pDBAdmin = db;	}
	void populateReport(void);

// Dialog Data
	enum { IDD = IDD_DIALOG3 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnBnClickedSearchPath();
	afx_msg void OnBnClickedDoBackup();
};
