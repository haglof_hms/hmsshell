// ShellTreeHiddenItems.cpp : implementation file
//

#include "stdafx.h"
#include "ShellTreeHiddenItems.h"
#include ".\shelltreehiddenitems.h"

// CShellTreeHiddenItems dialog

IMPLEMENT_DYNAMIC(CShellTreeHiddenItems, CDialog)

BEGIN_MESSAGE_MAP(CShellTreeHiddenItems, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_BTN_RESET_NAVTREE, &CShellTreeHiddenItems::OnBnClickedBtnResetNavtree)
END_MESSAGE_MAP()

CShellTreeHiddenItems::CShellTreeHiddenItems(vecTreeList &list,LPCTSTR suite_path,LPCTSTR caption,CWnd* pParent /*=NULL*/)
	: CDialog(CShellTreeHiddenItems::IDD, pParent)
{
	m_vecTreeList = list;
	m_sSuitePath = (suite_path);
	m_sCaption = (caption);

	m_ilTreeIcons.Create(16, 16, ILC_MASK|ILC_COLOR32, 1, 1);
	HICON hIcon = NULL;

	// changed how to load the icons from the resource. (091012 ag)
	hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 54);	// RSTR_TREE_FOLDER_CLOSED
	if (hIcon) m_ilTreeIcons.Add(hIcon);

	hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 55);	// RSTR_TREE_FOLDER_OPEN
	if (hIcon) m_ilTreeIcons.Add(hIcon);

	hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 56);	// RSTR_TREE_FORM
	if (hIcon) m_ilTreeIcons.Add(hIcon);

	hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 57);	// RSTR_TREE_REPORT
	if (hIcon) m_ilTreeIcons.Add(hIcon);

	hIcon = ExtractIcon(AfxGetInstanceHandle(), RESOURCE_DLL, 58);	// RSTR_TREE_HELP
	if (hIcon) m_ilTreeIcons.Add(hIcon);
}

CShellTreeHiddenItems::~CShellTreeHiddenItems()
{
}

void CShellTreeHiddenItems::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_TREE1, m_wndTreeCtrl);

//	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);

	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	DDX_Control(pDX, IDC_BTN_RESET_NAVTREE, m_wndBtnReset);
	//}}AFX_DATA_MAP
}

BOOL CShellTreeHiddenItems::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CShellTreeHiddenItems::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_vecReturnTreeList.clear();

	m_wndTreeCtrl.SetImageList(&m_ilTreeIcons, TVSIL_NORMAL);
	if (_tcscmp(extractFileName(m_sSuitePath),SHELLDATA_ShortcutsFN) == 0)
	{
		m_wndTreeCtrl.ModifyStyle(0,TVS_EDITLABELS);
	}
	m_wndTreeCtrl.setSuitePathAndFN(m_sSuitePath);

	setupData();

	return TRUE;
}

// PROTECTED
void CShellTreeHiddenItems::setupData(void)
{
	CString sCaption;

	sCaption.Format(_T("%s - %s"),(getResStr(IDS_STRING148)),m_sCaption);
	SetWindowText(sCaption);
//	m_wndLbl1.SetWindowText(_T(getResStr(IDS_STRING150)));

	m_wndBtnOK.SetWindowText((getResStr(IDS_STRING902)));
	m_wndBtnCancel.SetWindowText((getResStr(IDS_STRING903)));
	m_wndBtnReset.SetWindowText((getResStr(IDS_STRING904)));

	setupTreeData(m_vecTreeList);
}

// ID number for Icons in HMSIcons32.dll
/*
#define ID_TREECTRL_FOLDER_ITEM				0
#define ID_TREECTRL_EXEC_ITEM					2
#define ID_TREECTRL_REPORT_ITEM				3
#define ID_TREECTRL_HELP_ITEM					4
*/
void CShellTreeHiddenItems::setupTreeData(vecTreeList &list)
{
	HTREEITEM rootNode	  = NULL;
	HTREEITEM mexecNode	  = NULL;
	HTREEITEM childNode1  = NULL;
	HTREEITEM childNode2  = NULL;
	HTREEITEM childNode3  = NULL;
	HTREEITEM childNode4  = NULL;
	HTREEITEM execNode	  = NULL;
	int nItemIndex,nIndex,nLevel;
	TCHAR sCaption[50];
	TCHAR sNodeName[50];
	TCHAR sText[50];
	TCHAR sAttr_Func[50];
	TCHAR sAttr_Suite[64];
	HINSTANCE hModule = NULL;
	CString S;

	//m_wndTreeCtrl.ModifyStyle(TVS_CHECKBOXES,0);
	m_wndTreeCtrl.ModifyStyle(0,TVS_CHECKBOXES);

	for (unsigned int j = 0;j < list.size();j++)
	{
		CTreeList *tree = new CTreeList(list[j]);

//	S.Format(_T("CShellTreeHiddenItems::setupTreeData %d\nm_sSuitePath %s\ntree->getSuitePath() %s"),list.size(),m_sSuitePath,tree->getSuitePath());
//	AfxMessageBox(S);

		if (_tcscmp(m_sSuitePath,tree->getSuitePath()) == 0)
		{

			_tcscpy(sCaption,tree->getCaption());	
			_tcscpy(sNodeName,tree->getNodeName());	
			// Check if ItemText holds text, if so use ItemText instead of
			// Resourcefile string id value; 060503 p�d
			if (_tcscmp(tree->getItemText(),_T("")) != 0)
			{
				_tcscpy(sText,tree->getItemText());
			}
			else
			{
				_tcscpy(sText,getResStr(tree->getStrID()));// tree->getElement());	
			}
			_tcscpy(sAttr_Func,tree->getFunc());	
			_tcscpy(sAttr_Suite,tree->getSuite());	
			nIndex		= tree->getIndex();
			nLevel = tree->getLevel();

			if (_tcscmp(sNodeName,MAIN_ITEM) == 0)
			{
				nItemIndex = 0;
				rootNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_FOLDER_ITEM,ID_TREECTRL_FOLDER_ITEM,TVI_ROOT);
				m_wndTreeCtrl.SetItemState(rootNode, TVIS_BOLD, TVIS_BOLD);
				m_wndTreeCtrl.SetItemData(rootNode,DWORD_PTR(tree));
				m_wndTreeCtrl.SetCheck(rootNode,tree->getVisible() == 1);
			} // if (_tcscmp(sNodeName,MAIN_ITEM) == 0)
			else if (_tcscmp(sNodeName,MEXEC_ITEM) == 0)
			{
				nItemIndex = 0;
				rootNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_MEXEC_ITEM,ID_TREECTRL_MEXEC_ITEM,TVI_ROOT);
				m_wndTreeCtrl.SetItemData(rootNode,DWORD_PTR(tree));
				m_wndTreeCtrl.SetCheck(rootNode,tree->getVisible() == 1);
			} // if (_tcscmp(sNodeName,MEXEC_ITEM) == 0)
			else if (_tcscmp(sNodeName,MHELP_ITEM) == 0)
			{
				nItemIndex = 0;
				rootNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_MHELP_ITEM,ID_TREECTRL_MHELP_ITEM,TVI_ROOT);
				m_wndTreeCtrl.SetItemData(rootNode,DWORD_PTR(tree));
				m_wndTreeCtrl.SetCheck(rootNode,tree->getVisible() == 1);
			} // if (_tcscmp(sNodeName,MEXEC_ITEM) == 0)
			else if (_tcscmp(sNodeName,SUB_ITEM1) == 0) 
			{
				nItemIndex = 1;
				childNode1 = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_FOLDER_ITEM,ID_TREECTRL_FOLDER_ITEM,rootNode);
				m_wndTreeCtrl.SetItemData(childNode1,DWORD_PTR(tree));
				m_wndTreeCtrl.SetCheck(childNode1,tree->getVisible() == 1);
			} // if (_tcscmp(sNodeName,SUB_ITEM1) == 0)
			else if (_tcscmp(sNodeName,SUB_ITEM2) == 0)
			{
				nItemIndex = 2;
				childNode2 = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_FOLDER_ITEM,ID_TREECTRL_FOLDER_ITEM,childNode1);
				m_wndTreeCtrl.SetItemData(childNode2,DWORD_PTR(tree));
				m_wndTreeCtrl.SetCheck(childNode2,tree->getVisible() == 1);
			} // if (_tcscmp(sNodeName,SUB_ITEM2) == 0)
			else if (_tcscmp(sNodeName,SUB_ITEM3) == 0)
			{
				nItemIndex = 3;
				childNode3 = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_FOLDER_ITEM,ID_TREECTRL_FOLDER_ITEM,childNode2);
				m_wndTreeCtrl.SetItemData(childNode3,DWORD_PTR(tree));
				m_wndTreeCtrl.SetCheck(childNode3,tree->getVisible() == 1);
			} // if (_tcscmp(sNodeName,SUB_ITEM3) == 0)
			else if (_tcscmp(sNodeName,SUB_ITEM4) == 0)
			{
				nItemIndex = 4;
				childNode4 = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_FOLDER_ITEM,ID_TREECTRL_FOLDER_ITEM,childNode3);
				m_wndTreeCtrl.SetItemData(childNode4,DWORD_PTR(tree));
				m_wndTreeCtrl.SetCheck(childNode4,tree->getVisible() == 1);
			} // if (_tcscmp(sNodeName,SUB_ITEM4) == 0)
			else if (_tcscmp(sNodeName,EXEC_ITEM) == 0)
			{
				nItemIndex = nLevel - 2;
				if (nItemIndex == 0)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,rootNode);
				}
				else if (nItemIndex == 1)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode1);
				}
				else if (nItemIndex == 2)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode2);
				}
				else if (nItemIndex == 3)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode3);
				}
				else if (nItemIndex == 4)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode4);
				}
				// Add tree data to Data item in tree view; 050315 p�d
				m_wndTreeCtrl.SetItemData(execNode,DWORD_PTR(tree));
				m_wndTreeCtrl.SetCheck(execNode,tree->getVisible() == 1);
			}
			else if (_tcscmp(sNodeName,REPORT_ITEM) == 0)
			{
				nItemIndex = nLevel - 2;
				if (nItemIndex == 0)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_REPORT_ITEM,ID_TREECTRL_REPORT_ITEM,rootNode);
				}
				else if (nItemIndex == 1)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_REPORT_ITEM,ID_TREECTRL_REPORT_ITEM,childNode1);
				}
				else if (nItemIndex == 2)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_REPORT_ITEM,ID_TREECTRL_REPORT_ITEM,childNode2);
				}
				else if (nItemIndex == 3)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_REPORT_ITEM,ID_TREECTRL_REPORT_ITEM,childNode3);
				}
				else if (nItemIndex == 4)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_REPORT_ITEM,ID_TREECTRL_REPORT_ITEM,childNode4);
				}
				// Add tree data to Data item in tree view; 050315 p�d
				m_wndTreeCtrl.SetItemData(execNode,DWORD_PTR(tree));
				m_wndTreeCtrl.SetCheck(execNode,tree->getVisible() == 1);
			} // if (_tcscmp(sNodeName,REPORT_ITEM) == 0)
			else if (_tcscmp(sNodeName,HELP_ITEM) == 0)
			{
				nItemIndex = nLevel - 2;
				if (nItemIndex == 0)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_HELP_ITEM,ID_TREECTRL_HELP_ITEM,rootNode);
				}
				else if (nItemIndex == 1)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_HELP_ITEM,ID_TREECTRL_HELP_ITEM,childNode1);
				}
				else if (nItemIndex == 2)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_HELP_ITEM,ID_TREECTRL_HELP_ITEM,childNode2);
				}
				else if (nItemIndex == 3)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_HELP_ITEM,ID_TREECTRL_HELP_ITEM,childNode3);
				}
				else if (nItemIndex == 4)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_HELP_ITEM,ID_TREECTRL_HELP_ITEM,childNode4);
				}
				// Add tree data to Data item in tree view; 050315 p�d
				m_wndTreeCtrl.SetItemData(execNode,DWORD_PTR(tree));
				m_wndTreeCtrl.SetCheck(execNode,tree->getVisible() == 1);
			} // if (_tcscmp(sNodeName,REPORT_ITEM) == 0)
			else if (_tcscmp(sNodeName,EXTERN_ITEM) == 0)
			{
				nItemIndex = nLevel - 2;
				if (nItemIndex == 0)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,rootNode);
				}
				else if (nItemIndex == 1)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode1);
				}
				else if (nItemIndex == 2)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode2);
				}
				else if (nItemIndex == 3)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode3);
				}
				else if (nItemIndex == 4)
				{           
					execNode = m_wndTreeCtrl.InsertItem(sText,ID_TREECTRL_EXEC_ITEM,ID_TREECTRL_EXEC_ITEM,childNode4);
				}
				// Add tree data to Data item in tree view; 050315 p�d
				m_wndTreeCtrl.SetItemData(execNode,DWORD_PTR(tree));
				m_wndTreeCtrl.SetCheck(execNode,tree->getVisible() == 1);
			} // if (_tcscmp(sNodeName,REPORT_ITEM) == 0)
		}	// if (_tcscmp(m_sSuitePath,tree->getSuitePath()) == 0)
	}	// for (unsigned int j = 0;j < list.size();j++)

//	AddItem(_T(m_sToDoCaption), &m_wndTreeCtrl, 0);
}

void CShellTreeHiddenItems::OnBnClickedOk()
{
	CPADExtTreeCtrl* pCtrl = (CPADExtTreeCtrl*) GetDlgItem(IDC_TREE1);
	CTreeList *pRec;
	CString sEditText;
	int nLevel;
	int nCounter = 0;
	if (pCtrl != NULL)
	{
		HTREEITEM hTreeItem = pCtrl->GetRootItem();
		while (hTreeItem != NULL)
		{
			TVITEM item;
			TCHAR szText[1024];
			item.hItem = hTreeItem;
			item.mask = TVIF_TEXT | TVIF_HANDLE;
			item.pszText = szText;
			item.cchTextMax = 1024;
			
			BOOL bWorked = pCtrl->GetItem(&item);
			if (bWorked)
			{
				pRec = (CTreeList *)pCtrl->GetItemData(hTreeItem);
				pRec->setVisible((pCtrl->GetCheck(hTreeItem) ? 1 : 0));
				nLevel = pCtrl->getLevel(hTreeItem);
				pRec->setLevel(nLevel+1);
				if (pRec->getStrID() <= 0)
				{
					// Check if there's item text already, if so add new text
					// If not, set itemtext empty; 060509 p�d
					pRec->setItemtext(item.pszText);
				}
				else
				{
					pRec->setItemtext(_T(""));
				}

				m_vecReturnTreeList.push_back(*pRec);
				nCounter++;
			}	// if (bWorked)

			hTreeItem = GetNextTreeItem(*pCtrl,hTreeItem);
		}	// while (hTreeItem != NULL)
	}	// if (pCtrl != NULL)
	OnOK();
}

void CShellTreeHiddenItems::getReturnTreeListVec(vecTreeList &v)
{
	v = m_vecReturnTreeList;
}

void CShellTreeHiddenItems::OnBnClickedBtnResetNavtree()
{
	XMLHandler xml;
	CString sOriginalShellDataFile;
	sOriginalShellDataFile.Format(_T("%s\\%s"),getSetupShellData(),extractFileName(m_sSuitePath));
	if (fileExists(sOriginalShellDataFile))
	{
		if (copyFile(sOriginalShellDataFile,getShellDataDir()))
		{
			if (fileExists(m_sSuitePath))
			{
				if (xml.load(m_sSuitePath))
				{
					xml.getShellTreeData(m_vecTreeList,TRUE);			
					m_wndTreeCtrl.DeleteAllItems();
					setupTreeData(m_vecTreeList);
				}	// if (xml.load(m_sSuitePath))
			}	// if (fileExists(m_sSuitePath))
		}	// if (copyFile(sOriginalShellDataFile,getShellDataDir()))
	}	// if (fileExists(sOriginalShellDataFile))
}
