#ifndef _RSSXMLFILEPARSER_H_
#define _RSSXMLFILEPARSER_H_


//////////////////////////////////////////////////////////////////////////////////
//	CNewsBulletineItem
class CNewsBulletineItem
{
//private:
	CString m_sTitle;
	CString m_sLink;
	CString m_sDescription;
	CString m_sCategory;
	CString m_sPubDate;
public:
	// Default contructor
	CNewsBulletineItem(void)
	{
		m_sTitle				= _T("");
		m_sLink					= _T("");	
		m_sDescription	= _T("");
		m_sCategory			= _T("");
		m_sPubDate			= _T("");
	}
	CNewsBulletineItem(LPCTSTR title,LPCTSTR link,LPCTSTR desc,LPCTSTR category,LPCTSTR pub_date)
	{
		m_sTitle				= (title);
		m_sLink					= (link);	
		m_sDescription	= (desc);
		m_sCategory			= (category);
		m_sPubDate			= (pub_date);
	}
	// Copy contructor
	CNewsBulletineItem(const CNewsBulletineItem &c)
	{
		*this = c;
	}

	CString getTitle(void)			{ return m_sTitle; }
	CString getLink(void)				{ return m_sLink; }
	CString getDesc(void)				{ return m_sDescription; }
	CString getCategory(void)		{ return m_sCategory; }
	CString getPubDate(void)		{ return m_sPubDate; }
};

typedef std::vector<CNewsBulletineItem> vecNewsBulletineItems;

//////////////////////////////////////////////////////////////////////////////////
//	RSSXMLFilePars
class RSSXMLFilePars
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;
protected:
	CString getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	double getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	BOOL getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
public:
	RSSXMLFilePars(void);

	virtual ~RSSXMLFilePars();

	BOOL LoadFromFile(LPCTSTR);
	BOOL LoadFromBuffer(LPCTSTR);
	BOOL SaveToFile(LPCTSTR);

	BOOL getThisPubDate(LPTSTR);

	BOOL getNewsBulletines(vecNewsBulletineItems &, vecNewsCategoriesSetup &);

	BOOL getXML(CString &xml);
};



#endif