#pragma once

#include "BitmapDialog.h"

#include "Resource.h"
// CSplashDialog dialog

class CSplashDialog : public CBitmapDialog
{
	DECLARE_DYNAMIC(CSplashDialog)

public:
	CSplashDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSplashDialog();
// Dialog Data
	enum { IDD = IDD_SPLASH };

	void setSplashInfoText(LPCTSTR);
protected:
	HICON m_hIcon;

	CString m_sVersion;
	CString m_sCompany;
	CString m_sCopyright;

	CString m_sInfoText;

	CFont m_fnt1;
	CFont m_fnt2;
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBmpDlgDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CBmpDlgDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnLButtonDown(UINT,CPoint);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
