#pragma once

#include "StdAfx.h"

#include "Resource.h"

// CInputDlg dialog

class CInputDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputDlg)

	CString	m_sLangAbrev;
	CString	m_sLangFN;
	
	CString m_sEditText;

	CMyExtEdit m_wndEdit;
	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;
	
	BOOL m_bInitialized;
	
	void setupLanguage(void);

public:
	void setEditText(LPCTSTR str)
	{
		m_sEditText = str;
	}

	CString getEditText(void)
	{
		return m_sEditText;
	}

	CInputDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	// message handlers
	//{{AFX_MSG(CMyView)
	afx_msg void OnShowWindow(BOOL bShow,UINT nStatus);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
