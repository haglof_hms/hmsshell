// MainFrm.h : interface of the CMainFrame class
//

#include "StdAfx.h"
#include "DBHandler.h"
#include "LanguageDlg.h"
#include "SplashDialog.h"
#include "RSSXMLFileParser.h"
#include "XMLHandler.h"

/////////////////////////////////////////////////////////////////////////////
// CNavPaneInfo; helper class for on creating navigation panes; 051202 p�d
class CNavPaneInfo
{
	CNavigationPane *m_pane;
	TCHAR szCaption[128];
	TCHAR szNodeName[128];
	TCHAR szSuitePath[MAX_PATH];
	CTreeList m_recTreeList;
public:
	CNavPaneInfo()
	{
		m_pane	= NULL;
		_tcscpy(szCaption,_T(""));
		_tcscpy(szNodeName,_T(""));
		_tcscpy(szSuitePath,_T(""));
	}
	CNavPaneInfo(CNavigationPane *p,LPCTSTR cap,LPCTSTR node_name,LPCTSTR suite,CTreeList &tl)
	{
		m_pane	= p;
		_tcscpy(szCaption,cap);
		_tcscpy(szNodeName,node_name);
		_tcscpy(szSuitePath,suite);
		m_recTreeList = tl;
	}

	CNavPaneInfo(const CNavPaneInfo &c)
	{
		*this = c;
	}

	CNavigationPane *getNavPane(void)		{ return m_pane; }
	LPCTSTR getCaption(void)							{ return szCaption; }
	void setCaption(LPCTSTR s)						{ _tcscpy(szCaption,s); }

	LPCTSTR getNodeName(void)						{ return szNodeName; }

	LPCTSTR getSuitePath(void)						{ return szSuitePath; }

	CTreeList &getTreeList(void)				{ return m_recTreeList; }
};

typedef std::vector<CNavPaneInfo> VEC_NAVIGATION_PANES;

typedef std::map<LPCTSTR,BOOL> mapVisisblePanes;

class CVisibleItemsOnNavPane
{
	CString m_sCaption;
	BOOL m_bIsVisible;
public:
	CVisibleItemsOnNavPane(void)
	{
		m_sCaption = _T("");
		m_bIsVisible = TRUE;	// Defualt visible
	}
	CVisibleItemsOnNavPane(LPCTSTR cap,BOOL visible)
	{
		m_sCaption = cap;
		m_bIsVisible = visible;	// Defualt visible
	}

	CString getCaption(void)
	{
		return m_sCaption;
	}

	BOOL getIsVisible(void)
	{
		return m_bIsVisible;
	}

};

typedef std::vector<CVisibleItemsOnNavPane> vecVisibleItemsOnNavPane;

/////////////////////////////////////////////////////////////////////////////
// CDockingFrame frame
class CDockingFrame : public CXTPFrameWnd
{
	DECLARE_DYNCREATE(CDockingFrame)
protected:
	CDockingFrame();           // protected constructor used by dynamic creation

	VEC_NAVIGATION_PANES m_vecPanes;
	
	CImageList m_ilPaneIconsSmall;
	CImageList m_ilPaneIconsLarge;

	vecTreeList m_vecTreeList;

	void clearImageLists(void);
// Attributes
public:

	BOOL Create(CWnd *parent);

	void setTreeListVec(vecTreeList &);
	void setVisiblePanes(vecVisibleItemsOnNavPane &) const;

	vecTreeList &getTreeListVec(void)	{ return m_vecTreeList; }

	CXTPShortcutBar m_wndNavigationBar;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDockingFrame)
	//}}AFX_VIRTUAL

	void addPanes(int id);

	void updPane(UINT index);

	void getItemsToShowHide(UINT index,vecInt&);

	BOOL getPaneIDForSuite(LPCTSTR suite,int *pane_id);

	CNavPaneInfo& getSelectedNavPane(int index)
	{
		return m_vecPanes[index];
	}
// Implementation
protected:
	virtual ~CDockingFrame();

	// Generated message map functions
	//{{AFX_MSG(CDockingFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////
// CMainFrame

class CMainFrame : public CXTPMDIFrameWnd // CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

	CXTPDockingPaneManager m_paneManager;
	CXTPTabClientWnd m_MTIClientWnd;
	
	CDockingFrame* m_pDockingFrame;

	vecTreeList m_vecShellData;

	HINSTANCE_VEC m_vecHInst;

	BOOL bIsFirstTime;

	vecINDEX_TABLE m_vecIndexTable;
	vecINFO_TABLE m_vecInfoTable;

	CString m_sCaption;
	CString m_sNodeName;
	CString m_sSuitePath;
	short m_nVisible;
	long m_nCounter;
	UINT m_nNumOf;
	BOOL m_bContinue;
	int m_nPaneID;

// Attributes
public:

// Operations
public:
	CXTPDockingPaneManager* GetDockingPaneManager() {	return &m_paneManager; }
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	void setLanguage(void);

	void setHInstVec(HINSTANCE_VEC &vec);

	void setTreeListVec(vecTreeList &);
	void setIndexTableVec(vecINDEX_TABLE &);
	void setInfoTableVec(vecINFO_TABLE &);
	void executeShellTreeItem(int index,LPCTSTR func,LPCTSTR suite);
	void executeShellTreeItem(CTreeList &);
	void executeShellTreeItemEx(_user_msg *msg);

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreTranslateMessage(MSG *pMsg);
	//}}AFX_VIRTUAL

	// This method also can reconnect to db usin' makeDBConnection
	//	and makeAdminDBConnection; 071221 p�d
	void setServerInfoOnStatusBar(BOOL on_line);

	void setOnlineOfflineOnStatusBar(void);

	RECT m_rectToolbar;

	// Overrode CWnd implementation to restore saved window position.
	CXTWindowPos m_wndPosition;
	BOOL ShowWindowEx(int nCmdShow);

	// Added 060809 p�d
	BOOL isPermanentCategory(LPCTSTR);

	BOOL setupDBConnection(CSplashDialog& dlg);

	void doAdminSetup(void);

	void doDatabaseBackup(void);
	void doDatabaseRestore(void);
	void doDatabaseCopy(void);

	void checkIfThereIsADatabaseAdmin(void);
// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members

	BOOL bFirstOpened;

	BOOL m_bIsUMDatabase;

	BOOL m_bIsFocusOnNavTree;

	CString m_sLangSet;

	CString m_sLastSelectedPane;
	DWORD m_nLastSelectedPaneID;
	
	CXTPStatusBar m_wndStatusBar;

//	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	
	CXTPDockingPane* m_wndNavBarPane;
	CXTPCommandBars* pCommandBars;
	CXTPCommandBar* pMenuBar;

	HINSTANCE getModuleInstance(LPCTSTR path);

	CLanguageDlg m_wndLangDlg;
	
	vecHmsUserDBs m_vecHmsUserDBs;
	void getHmsUserDB(void);
	void runDoDatabaseTablesInSuitesModules(void);

	mapVisisblePanes m_mapVisisblePanes;
	vecVisibleItemsOnNavPane m_vecVisibleItemsOnNavPane;
	void getPanesHiddenOrVisible(void);

	// Enable/Disable DBNavigation buttons
	BOOL m_bTBNewBtnStatus;
	BOOL m_bTBOpenBtnStatus;
	BOOL m_bTBSaveBtnStatus;
	BOOL m_bTBDeleteBtnStatus;
	BOOL m_bTBPreviewBtnStatus;

	BOOL m_bTBBackupDatabaseStatus;
	BOOL m_bTBRestoreDatabaseStatus;
	BOOL m_bTBCopyDatabaseStatus;

	BOOL m_bTBMenuNews;

	BOOL m_bDBNavigationBtnStart;
	BOOL m_bDBNavigationBtnPrev;
	BOOL m_bDBNavigationBtnNext;
	BOOL m_bDBNavigationBtnEnd;
	BOOL m_bDBNavigationBtnList;

	void sendMessageToChild(int id,LPARAM param = 0);
	void sendMessageToAChild(int id,_doc_identifer_msg *msg);
	
	BOOL treeItemHide(LPARAM);
	BOOL treeItemDialog(LPARAM);
	BOOL treeItemAddToShortcuts(LPARAM);
	// Added 060809 p�d
	TCHAR m_szPermanentCategories[128];

	int	m_nTheme;
	int	m_nAnimation;
	UINT	m_uAnimDelay;
	UINT	m_uShowDelay;
	BOOL	m_bAllowMove;
	CSize m_sizeNewsPopup;
	UINT m_nNewsBulletinItemIndex;
	BOOL m_bShowNews;
	BOOL m_bShowPopup;
	
	//popup window object
	CList<CXTPPopupControl*, CXTPPopupControl*> m_lstPopupControl;
	CXTPPopupControl* m_pActivePopup;
	
	SAConnection m_saConnection;
	SAConnection m_saAdminConnection;
	BOOL m_bConnected;
	BOOL m_bAdminConnected;

	DB_CONNECTION_DATA m_DBConnection;

	DATABASE_CONNECTION_STATUS m_enumDATABASE_CONNECTION_STATUS;
	CONN_STATUS m_enumCONN_STATUS;

	CTreeList *m_pActiveTreeListItem;
// My methods
protected:	
	vecNewsBulletineItems m_vecNewsBulletines;
	void newsBulletines(void);

	void enableItems(BOOL bEnable);
	void setupNewsPopup(void);
	void showNewItemInPopup();

	void closeChildWindows(BOOL close_all = TRUE);
	BOOL areThereChildWindows(void);

	int DeleteLayoutKeys(HKEY hKey, CString path);

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnShowWindow(BOOL bShow,UINT nStatus);
	afx_msg LRESULT OnDockingPaneNotify(WPARAM wParam, LPARAM lParam);
	afx_msg void OnShowPane(void);
	afx_msg void OnUpdateMenuView(CCmdUI* pCmdUI);
	afx_msg void OnCustomize();

	afx_msg void OnTimer(UINT nIDEvent);

	//	My commands
	//afx_msg void OnHelp(void); //Tagit bort, finns redan i enskilda moduler ist�llet #4493 20151013
	afx_msg void OnNews(void);
	afx_msg void OnWebSite(void);
	afx_msg void OnAbout(void);
	//afx_msg BOOL OnHelpInfo(HELPINFO* lpHelpInfo);

	afx_msg void OnChangeLanguage(void);
	afx_msg void OnAdministration(void);
	afx_msg void OnBackup(void);
	afx_msg void OnUpdateBackupBtn(CCmdUI* pCmdUI);
	afx_msg void OnRestoreBackup(void);
	afx_msg void OnUpdateRestoreDatabaseBtn(CCmdUI* pCmdUI);
	afx_msg void OnCopyBackup(void);
	afx_msg void OnUpdateCopyDatabaseBtn(CCmdUI* pCmdUI);
	afx_msg void OnRestoreLayout(void);

	afx_msg void OnNewItemBtnClick();
	afx_msg void OnUpdateNewItemBtn(CCmdUI* pCmdUI);

	afx_msg void OnOpenItemBtnClick();
	afx_msg void OnUpdateOpenItemBtn(CCmdUI* pCmdUI);

	afx_msg void OnSaveItemBtnClick();
	afx_msg void OnUpdateSaveItemBtn(CCmdUI* pCmdUI);

	afx_msg void OnDeleteItemBtnClick();
	afx_msg void OnUpdateDeleteItemBtn(CCmdUI* pCmdUI);

	afx_msg void OnPreviewItemBtnClick();
	afx_msg void OnUpdatePreviewItemBtn(CCmdUI* pCmdUI);

	afx_msg void OnDBNavigStartBtnClick();
	afx_msg void OnDBNavigPrevBtnClick();
	afx_msg void OnDBNavigNextBtnClick();
	afx_msg void OnDBNavigEndBtnClick();
	afx_msg void OnDBNavigListBtnClick();

	afx_msg LRESULT OnMessageFromSuite( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnTreeListMessage( WPARAM wParam, LPARAM lParam );

	afx_msg LRESULT OnMDIDestroy(WPARAM wParam, LPARAM lParam);

	afx_msg void OnNewItemToolbarBtnStart(CCmdUI* pCmdUI);

	afx_msg void OnUpdateMenuToolsDatabase(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMenuNews(CCmdUI* pCmdUI);

	afx_msg void OnUpdateDBToolbarBtnStart(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDBToolbarBtnPrev(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDBToolbarBtnNext(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDBToolbarBtnEnd(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDBToolbarBtnList(CCmdUI* pCmdUI);

	afx_msg LRESULT OnIdleUpdateCmdUI(WPARAM,LPARAM);

	afx_msg void OnChangeServerCmd();

	afx_msg LRESULT OnPopUpNotify(WPARAM wParam, LPARAM lParam);

	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);

	afx_msg void OnOnlineOfflineToggleBtn(void);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

// Struct that carries the data of the filetype that should activate something in HMS.
//2012-05-08 Mathias Dickl�n
struct STRUCT_FILETYPE
{
	int Id;
	wchar_t Type[16];
	wchar_t Path[512];
};


