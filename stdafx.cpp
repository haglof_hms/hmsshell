// stdafx.cpp : source file that includes just the standard includes
// HMSShellXT.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "ResLangFileReader.h"

#define ID_SHOWVIEW_MSG							0x8017
#define MSG_IN_SUITE				 			(WM_USER + 10)		// This identifer's used to send messages internally

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}

/////////////////////////////////////////////////////////////////////////////////////
// CSuiteModuleInfoParser

// Handle Suite and User module info into XML file; 060803 p�d

CSuiteModuleInfoParser::CSuiteModuleInfoParser(void)
{
	CHECK(CoInitialize(NULL));

	pDomDoc = NULL;

	// Create MSXML2 DOM Document
	pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
	
	// Set parser in NON async mode
	pDomDoc->async	= VARIANT_FALSE;
	
	// Validate during parsing
	pDomDoc->validateOnParse = VARIANT_TRUE;
}

CSuiteModuleInfoParser::~CSuiteModuleInfoParser()
{
  CoUninitialize();
}

BOOL CSuiteModuleInfoParser::CreateDoc(void)
{
	_variant_t vNullVal;
	MSXML2::IXMLDOMProcessingInstructionPtr pPI = NULL;
	//---------------------------------------------------------------------------
	// Create an empty DOM; 030120 p�d
	//---------------------------------------------------------------------------
	pDomDoc->loadXML(_T(""));
	//---------------------------------------------------------------------------
	// Add the XML processing instruction ("<?xml version="1.0"?>"); 030120 p�d
	//---------------------------------------------------------------------------

	pPI = pDomDoc->createProcessingInstruction(PI_XML_NAME, PI_VER_ENCODE);

	vNullVal.vt = VT_NULL;
	pDomDoc->insertBefore(pPI, vNullVal);

	return true;
}

// Methods for Create,Load and Saving xml file(s); 060407 p�d
BOOL CSuiteModuleInfoParser::LoadFromFile(LPCTSTR file)
{
	return pDomDoc->load(file);
}

BOOL CSuiteModuleInfoParser::LoadFromBuffer(LPCTSTR buffer)
{
	return pDomDoc->loadXML(buffer);
}

BOOL CSuiteModuleInfoParser::SaveToFile(LPCTSTR file)
{
	return pDomDoc->save(file);
}

// Protected
MSXML2::IXMLDOMNodePtr CSuiteModuleInfoParser::addItem(MSXML2::IXMLDOMNodePtr node,LPCTSTR S)
{
//	node->text = _T(_bstr_t(S));
	node->text = (_bstr_t(S));
	return node;
}

// Public methods

BOOL CSuiteModuleInfoParser::setSuiteModuleInfo(vecINFO_TABLE &vec)
{
	CString sStrID;
	MSXML2::IXMLDOMNodePtr ptrNodeSuite = NULL;
	MSXML2::IXMLDOMNodePtr ptrNodeUM = NULL;
	MSXML2::IXMLDOMNodePtr ptrItem = NULL;
	// Setup ROOT
	MSXML2::IXMLDOMElementPtr ptrRootElem = pDomDoc->createElement(TAG_ROOT);
	pDomDoc->documentElement = ptrRootElem;
	// Get Root node
	MSXML2::IXMLDOMNodePtr ptrRoot = pDomDoc->documentElement;
	
	// Add element to Suite node
	for (UINT i = 0;i < vec.size();i++)
	{
		_info_table rec = vec[i];
		sStrID.Format(_T("%d"),rec.nStrIndex);

		// Add Suite(s); 060803 p�d
		if (rec.nType == 1)
		{
			MSXML2::IXMLDOMElementPtr ptrElem = pDomDoc->createElement(TAG_SUITE);
			ptrNodeSuite = ptrRoot->appendChild( ptrElem );
			ptrNodeSuite->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_STRID ),sStrID) );
			ptrNodeSuite->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_LANG_FILE ),rec.szLanguageFN) );
			ptrNodeSuite->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_VERSION ),rec.szVersion) );
			ptrNodeSuite->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_COPYRIGHT ),rec.szCopyright) );
			ptrNodeSuite->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_COMPANY ),rec.szCompany) );
		}	// if (rec.nType == 1)

		// Add User module(s) in Suite; 060804 p�d
		if (rec.nType == 2)
		{
			MSXML2::IXMLDOMElementPtr ptrElem = pDomDoc->createElement(TAG_USER_MODULE);
			ptrNodeUM = ptrNodeSuite->appendChild( ptrElem );
			ptrNodeUM->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_STRID ),sStrID) );
			ptrNodeUM->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_LANG_FILE ),rec.szLanguageFN) );
			ptrNodeUM->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_VERSION ),rec.szVersion) );
			ptrNodeUM->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_COPYRIGHT ),rec.szCopyright) );
			ptrNodeUM->appendChild( addItem(pDomDoc->createElement( TAG_ELEM_COMPANY ),rec.szCompany) );
		}	// if (rec.nType == 2)
	} // for (UINT i = 0;i < vec.size();i++)


	return TRUE;
}

BOOL CSuiteModuleInfoParser::getXML(CString &xml)
{
	CComBSTR bstrBuffer;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	if (pRoot)
	{
		pRoot->get_xml( &bstrBuffer );
		CW2CT szBuffer( bstrBuffer );
		xml = szBuffer;
		return TRUE;
	}

	return FALSE;
}

// Check for Network connection ststus; 071221 p�d
int isNetwork(void)
{
	DWORD dw;
  if( !IsNetworkAlive(&dw))
  {
		// Let's assume all errors will result
		// in a NO CONNECTION; 071221 p�d
		return 0;
	}
	else
	{
		if (dw == NETWORK_ALIVE_LAN || dw == NETWORK_ALIVE_WAN)
			return 1;
	}

	return 0;
}

int InsertRow(CListCtrl &ctrl,int nPos,int nNoOfCols, LPCTSTR pText, ...)
{
    va_list argList;
    va_start(argList, pText);

    ASSERT(nNoOfCols >= 1); // use the 'normal' Insert function with 1 argument
    int nIndex = ctrl.InsertItem(LVIF_TEXT|LVIF_STATE, nPos, pText,0,LVIS_SELECTED,0,0);
    ASSERT(nIndex != -1);
    if (nIndex < 0) return(nIndex);
    for (int i = 1; i < nNoOfCols; i++) 
		{
        LPCTSTR p = va_arg(argList,LPCTSTR);
        if (p != NULL) 
				{
					ctrl.SetItemText(nIndex, i, p);    
        }
    }
    va_end(argList);
    return(nIndex);
}

int GetSelectedItem(CListCtrl &ctrl)
{
	int nItem = -1;
  POSITION nPos = ctrl.GetFirstSelectedItemPosition();
  if (nPos)
  {
		nItem = ctrl.GetNextSelectedItem(nPos);
  }
  return nItem;
}

CView *showFormView(int idd, LPCTSTR lang_fn, LPARAM lp)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CView *pActiveView;

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
					pActiveView = pView;
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					CString sDocTitle;
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
						break;
					}	// if(posView != NULL)
				}

				break;
			}
		}
	}

	return pActiveView;
}
