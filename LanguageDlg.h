#if !defined(AFX_LANGUAGEDLG_H)
#define AFX_LANGUAGEDLG_H

//#pragma once

#include "StdAfx.h"
#include "Misc.h"
#include "resource.h"

// CLanguageDlg dialog


class CLanguageDlg : public CDialog
{
	DECLARE_DYNAMIC(CLanguageDlg)

public:
	CLanguageDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLanguageDlg();

// Dialog Data
	enum { IDD = IDD_FORMVIEW };

// My public methods goes here
	CString getLangAbrev(void)		{ return m_sLangAbrevSet; }

	void setLanguage(void);
// Data members
protected:
//	CMainFrame *m_wndMDIFrame;

	CButton m_wndBtn1;
	CListBox m_wndLangList;
	CStatic m_wndLabel1;

	CString m_sLangAbrevSet;

	vecLANGUAGE_INFO m_vecLang;
	CStringArray m_arrLangFiles;

	void setupLanguagesInListBox(void);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	// message handlers
	//{{AFX_MSG(CLanguageDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnLbnSelchangeList1();
};

#endif