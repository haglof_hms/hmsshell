//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by HMSShellXT.rc
//
#define IDS_STRING1                     1
#define IDS_STRING2                     2
#define IDS_STRING3                     3
#define IDS_STRING4                     4
#define IDD_FORMVIEW                    101
#define IDR_MAINFRAME                   128
#define IDR_TOOLBAR_DBNAVIG             143
#define IDR_NAVBAR_POPUP                147
#define IDB_BITMAP2                     149
#define IDD_DIALOG1                     152
#define IDR_SHOW_HIDE_TREE              153
#define IDD_SPLASH                      155
#define IDB_BITMAP3                     157
#define IDI_ICON5                       159
#define IDI_ICON6                       164
#define IDI_ICON7                       165
#define IDD_DIALOG2                     166
#define IDD_DIALOG3                     169
#define IDB_BITMAP7                     171
#define IDD_DIALOG4                     172
#define IDC_BUTTON1                     1000
#define IDC_LIST1                       1001
#define IDC_STATIC1                     1002
#define IDC_EDIT1                       1003
#define IDC_EDIT2                       1004
#define IDC_EDIT3                       1005
#define IDC_LBL1                        1006
#define IDC_LBL2                        1007
#define IDC_LBL3                        1008
#define IDC_LBL5                        1011
#define IDC_COMBO1                      1012
#define IDC_BTN_TEST                    1013
#define IDC_TREE1                       1014
#define IDC_LBL6                        1015
#define IDC_LBL7                        1016
#define IDC_COMBO2                      1020
#define IDC_COMBO3                      1021
#define IDC_COMBO4                      1022
#define IDC_SEARCH_SERVERS              1023
#define IDC_BCK_LBL1                    1024
#define IDC_BCK_LBL2                    1025
#define IDC_BUTTON2                     1026
#define IDC_BCK_LBL3                    1028
#define IDC_LIST2                       1029
#define IDC_FROM_LBL                    1030
#define IDC_TO_LBL                      1031
#define IDC_INFO_LBL                    1032
#define IDC_BTN_RESET_NAVTREE           1033
#define IDC_BUTTON3                     1034
#define IDC_ACTIVE_DB_LBL               1035
#define ID_VIEW_NAVIGATIONBAR           32776
#define ID_DBNAVIG_START                32778
#define ID_DBNAVIG_NEXT                 32779
#define ID_DBNAVIG_PREV                 32780
#define ID_DBNAVIG_END                  32781
#define ID_NEW_ITEM                     32786
#define ID_OPEN_ITEM                    32787
#define ID_SAVE_ITEM                    32788
#define ID_DELETE_ITEM                  32789
#define ID_PREVIEW_ITEM                 32790
#define ID_DBNAVIG_LIST                 32791
#define ID_POPUP_HIDE                   32792
#define ID_POPUP_SHOW                   32793
#define ID_POPUP_ADD_TO_SHORTCUTS       32795
#define ID_POPUP_EXPAND_TREE            32796
#define ID_POPUP_COLLAPSE_TREE          32797
#define ID_POPUP_ADD_MAINITEM           32798
#define ID_POPUP_DEL_MAINITEM           32799
#define ID_POPUP_CHANGE_ITEMNAME        32800
#define ID_POPUP_CHANGE_ITEM_TYPE       32801
#define ID_TOOLS_LANGUAGE               32802
#define ID_TOOLS_DATABASE               32803
#define ID_REPORT_OPEN                  32804
#define ID_MENUITEM_NEWS                32807
#define ID_MENUITEM_WEBSITE             32808
#define ID_TOOLS_BACKUP                 32809
#define ID_DATABASE_BACKUP              32811
#define ID_DATABASE_RESTORE             32814
#define ID_DATABASE_COPY                32815
#define ID_DATABASE_CHANGE_DB           32816
#define ID_WINDOWS_CACADE               32819
#define ID_WINDOWS_TILE                 32820
#define ID_WINDOWS_TILE_HORZ            32821
#define ID_PANE_NAVIGATION              32822

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        175
#define _APS_NEXT_COMMAND_VALUE         32823
#define _APS_NEXT_CONTROL_VALUE         1036
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
