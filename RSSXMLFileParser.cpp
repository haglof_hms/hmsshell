#include "StdAfx.h"
#include "RSSXMLFileParser.h"

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}


RSSXMLFilePars::RSSXMLFilePars(void)
{
	CHECK(CoInitialize(NULL));

	pDomDoc = NULL;

	// Create MSXML2 DOM Document
	pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
	
	// Set parser in NON async mode
	pDomDoc->async	= VARIANT_FALSE;
	
	// Validate during parsing
	pDomDoc->validateOnParse = VARIANT_TRUE;
}

RSSXMLFilePars::~RSSXMLFilePars()
{
  CoUninitialize();
}

// Methods for Loagins and Saving xml file(s); 060407 p�d

BOOL RSSXMLFilePars::LoadFromFile(LPCTSTR file)
{
	return pDomDoc->load(file);
}

BOOL RSSXMLFilePars::LoadFromBuffer(LPCTSTR buffer)
{
	BOOL bRet;

	if(pDomDoc->loadXML(buffer) == VARIANT_FALSE)
	{
		bRet = FALSE;
	}
	else
	{
		bRet = TRUE;
	}

	return bRet;
}

BOOL RSSXMLFilePars::SaveToFile(LPCTSTR file)
{
	return pDomDoc->save(file);
}

// Protected
CString RSSXMLFilePars::getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
	}	// if (pAttr)

	return szData;
}

double RSSXMLFilePars::getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	double fValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		fValue = _tstof(szData);
	}	// if (pAttr)

	return fValue;
}

BOOL RSSXMLFilePars::getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	BOOL bValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		bValue = (_tcscmp(szData,_T("0")) == 0 ? FALSE : TRUE);
	}	// if (pAttr)

	return bValue;
}


// Public

// Define nodes in RSS XML-file; 060801 p�d
#define NODE_ROOT										_T("item")
#define NODE_LASTBUILD_TITLE				_T("title")
#define NODE_LASTBUILD_LINK					_T("link")
#define NODE_LASTBUILD_DESC					_T("description")
#define NODE_LASTBUILD_CATEGORY			_T("category")
#define NODE_LASTBUILD_PUB_DATE			_T("pubDate")
#define NODE_LASTBUILD_PUB_DATE0		_T("//channel/item/pubDate")

// Methods for reading Header information
BOOL RSSXMLFilePars::getThisPubDate(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_LASTBUILD_PUB_DATE0);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		
		return TRUE;
	}

	return FALSE;
}

BOOL RSSXMLFilePars::getNewsBulletines(vecNewsBulletineItems &vec, vecNewsCategoriesSetup &vecNewsCategories)
{
	CComBSTR bstrTitle;
	CComBSTR bstrLink;
	CComBSTR bstrDesc;
	CString csDesc;
	CComBSTR bstrCategory;
	CComBSTR bstrPubDate;
	long lNumOf;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodeListPtr pNodeList = pRoot->getElementsByTagName(_bstr_t(NODE_ROOT));
	MSXML2::IXMLDOMNodePtr pNode = NULL;
	BOOL bAdd;

	if (pNodeList)
	{	
		vec.clear();
		pNodeList->get_length( &lNumOf );
		for (long l = 0;l < lNumOf;l++)
		{
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[l];

			if (pChild)
			{
				pNode = pChild->selectSingleNode(_bstr_t(NODE_LASTBUILD_TITLE));
				if (pNode)
				{
					pNode->get_text(&bstrTitle);
				}
				pNode = pChild->selectSingleNode(_bstr_t(NODE_LASTBUILD_LINK));
				if (pNode)
				{
					pNode->get_text(&bstrLink);
				}
				pNode = pChild->selectSingleNode(_bstr_t(NODE_LASTBUILD_DESC));
				if (pNode)
				{
					pNode->get_text(&bstrDesc);
				}
				pNode = pChild->selectSingleNode(_bstr_t(NODE_LASTBUILD_CATEGORY));
				if (pNode)
				{
					pNode->get_text(&bstrCategory);
				}
				pNode = pChild->selectSingleNode(_bstr_t(NODE_LASTBUILD_PUB_DATE));
				if (pNode)
				{
					pNode->get_text(&bstrPubDate);
				}

				// ta bort nyheter som tillh�r kategorier vi inte vill visa
				bAdd = TRUE;
				for(int j = 0; j < vecNewsCategories.size(); j++)
				{
					if( vecNewsCategories[j].getDisplay() == FALSE )
					{
						if(	vecNewsCategories[j].getCategory() == CString(bstrCategory) )
						{
							bAdd = FALSE;
							break;
						}
					}
				}

				if(bAdd == TRUE)
				{
					vec.push_back(CNewsBulletineItem(CString(bstrTitle),
													 CString(bstrLink),
													 CString(bstrDesc),
													 CString(bstrCategory),
													 CString(bstrPubDate)));
				}
			}	// if (pChild)
		}	// for (long l = 0;l < lNumOf;l++)

		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}


BOOL RSSXMLFilePars::getXML(CString &xml)
{
	CComBSTR bstrBuffer;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	if (pRoot)
	{
		pRoot->get_xml( &bstrBuffer );
		CW2CT szBuffer( bstrBuffer );
		xml = szBuffer;
		return TRUE;
	}

	return FALSE;
}


